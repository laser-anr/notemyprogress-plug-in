<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/student_sessions.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_student', $context);
require_capability('local/notemyprogress:student_sessions', $context);

if(is_siteadmin()){
    print_error(get_string("only_student","local_notemyprogress"));
}

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "STUDENT_STUDY_SESSIONS", "student_study_sessions", $actualLink, "Section where you can consult various indicators on the study sessions carried out by the student");

$reports = new \local_notemyprogress\student($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => [
        "section_help_title" => get_string("ss_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("ss_section_help_description", "local_notemyprogress"),
        "inverted_time_help_title" => get_string("ss_inverted_time_help_title", "local_notemyprogress"),
        "inverted_time_help_description_p1" => get_string("ss_inverted_time_help_description_p1", "local_notemyprogress"),
        "inverted_time_help_description_p2" => get_string("ss_inverted_time_help_description_p2", "local_notemyprogress"),
        "hours_session_help_title" => get_string("ss_hours_session_help_title", "local_notemyprogress"),
        "hours_session_help_description_p1" => get_string("ss_hours_session_help_description_p1", "local_notemyprogress"),
        "hours_session_help_description_p2" => get_string("ss_hours_session_help_description_p2", "local_notemyprogress"),
        "resources_access_help_title" => get_string("ss_resources_access_help_title", "local_notemyprogress"),
        "resources_access_help_description_p1" => get_string("ss_resources_access_help_description_p1", "local_notemyprogress"),
        "resources_access_help_description_p2" => get_string("ss_resources_access_help_description_p2", "local_notemyprogress"),
        "resources_access_help_description_p3" => get_string("ss_resources_access_help_description_p3", "local_notemyprogress"),

        "title" => get_string("nmp_title", "local_notemyprogress"),
        "chart" => $reports->get_chart_langs(),
        "days" => array(
            get_string("nmp_mon_short", "local_notemyprogress"),
            get_string("nmp_tue_short", "local_notemyprogress"),
            get_string("nmp_wed_short", "local_notemyprogress"),
            get_string("nmp_thu_short", "local_notemyprogress"),
            get_string("nmp_fri_short", "local_notemyprogress"),
            get_string("nmp_sat_short", "local_notemyprogress"),
            get_string("nmp_sun_short", "local_notemyprogress"),
        ),
        "hours" => array(
            get_string("nmp_00", "local_notemyprogress"),
            get_string("nmp_01", "local_notemyprogress"),
            get_string("nmp_02", "local_notemyprogress"),
            get_string("nmp_03", "local_notemyprogress"),
            get_string("nmp_04", "local_notemyprogress"),
            get_string("nmp_05", "local_notemyprogress"),
            get_string("nmp_06", "local_notemyprogress"),
            get_string("nmp_07", "local_notemyprogress"),
            get_string("nmp_08", "local_notemyprogress"),
            get_string("nmp_09", "local_notemyprogress"),
            get_string("nmp_10", "local_notemyprogress"),
            get_string("nmp_11", "local_notemyprogress"),
            get_string("nmp_12", "local_notemyprogress"),
            get_string("nmp_13", "local_notemyprogress"),
            get_string("nmp_14", "local_notemyprogress"),
            get_string("nmp_15", "local_notemyprogress"),
            get_string("nmp_16", "local_notemyprogress"),
            get_string("nmp_17", "local_notemyprogress"),
            get_string("nmp_18", "local_notemyprogress"),
            get_string("nmp_19", "local_notemyprogress"),
            get_string("nmp_20", "local_notemyprogress"),
            get_string("nmp_21", "local_notemyprogress"),
            get_string("nmp_22", "local_notemyprogress"),
            get_string("nmp_23", "local_notemyprogress"),
        ),
        "modules_names" => array(
            "assign" => get_string("nmp_assign", "local_notemyprogress"),
            "assignment" => get_string("nmp_assignment", "local_notemyprogress"),
            "attendance" => get_string("nmp_attendance", "local_notemyprogress"),
            "book" => get_string("nmp_book", "local_notemyprogress"),
            "chat" => get_string("nmp_chat", "local_notemyprogress"),
            "choice" => get_string("nmp_choice", "local_notemyprogress"),
            "data" => get_string("nmp_data", "local_notemyprogress"),
            "feedback" => get_string("nmp_feedback", "local_notemyprogress"),
            "folder" => get_string("nmp_folder", "local_notemyprogress"),
            "forum" => get_string("nmp_forum", "local_notemyprogress"),
            "glossary" => get_string("nmp_glossary", "local_notemyprogress"),
            "h5pactivity" => get_string("nmp_h5pactivity", "local_notemyprogress"),
            "imscp" => get_string("nmp_imscp", "local_notemyprogress"),
            "label" => get_string("nmp_label", "local_notemyprogress"),
            "lesson" => get_string("nmp_lesson", "local_notemyprogress"),
            "lti" => get_string("nmp_lti", "local_notemyprogress"),
            "page" => get_string("nmp_page", "local_notemyprogress"),
            "quiz" => get_string("nmp_quiz", "local_notemyprogress"),
            "resource" => get_string("nmp_resource", "local_notemyprogress"),
            "scorm" => get_string("nmp_scorm", "local_notemyprogress"),
            "survey" => get_string("nmp_survey", "local_notemyprogress"),
            "url" => get_string("nmp_url", "local_notemyprogress"),
            "wiki" => get_string("nmp_wiki", "local_notemyprogress"),
            "workshop" => get_string("nmp_workshop", "local_notemyprogress"),
        ),
        "modules_strings" => array(
            "title" => get_string("nmp_modules_access_chart_title","local_notemyprogress"),
            "modules_no_viewed" => get_string("nmp_modules_no_viewed","local_notemyprogress"),
            "modules_viewed" => get_string("nmp_modules_viewed","local_notemyprogress"),
            "modules_complete" => get_string("nmp_modules_complete","local_notemyprogress"),
            "close_button" => get_string("nmp_close_button","local_notemyprogress"),
            "modules_interaction" => get_string("nmp_modules_interaction","local_notemyprogress"),
            "modules_interactions" => get_string("nmp_modules_interactions","local_notemyprogress"),
        ),
        "no_data" => get_string("no_data", "local_notemyprogress"),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name","local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to","local_notemyprogress"),
        "pagination_title" => get_string("pagination_title","local_notemyprogress"),
        "helplabel" => get_string("helplabel","local_notemyprogress"),
        "exitbutton" => get_string("exitbutton","local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),

        "inverted_time_chart_title" => get_string("nmp_student_time_inverted_title","local_notemyprogress"),
        "inverted_time_chart_x_axis" => get_string("nmp_student_time_inverted_x_axis","local_notemyprogress"),
        "inverted_time" => get_string("nmp_student_inverted_time","local_notemyprogress"),
        "expected_time" => get_string("nmp_student_expected_time","local_notemyprogress"),

        "resource_access_title" => get_string("nmp_resource_access_title", "local_notemyprogress"),
        "resource_access_x_axis" => get_string("nmp_resource_access_x_axis", "local_notemyprogress"),
        "resource_access_y_axis" => get_string("nmp_resource_access_y_axis", "local_notemyprogress"),
        "resource_access_legend1" => get_string("nmp_resource_access_legend1", "local_notemyprogress"),
        "resource_access_legend2" => get_string("nmp_resource_access_legend2", "local_notemyprogress"),

        "hours_sessions_title" => get_string("nmp_hours_sessions_title", "local_notemyprogress"),
        "week_progress_title" => get_string("nmp_week_progress_title", "local_notemyprogress"),

        "session_text" => get_string("nmp_session_text","local_notemyprogress"),
        "sessions_text" => get_string("nmp_sessions_text","local_notemyprogress"),
        "modules_details" => get_string("nmp_modules_details", "local_notemyprogress"),

        "hours_short" => get_string("nmp_hours_short", "local_notemyprogress"),
        "minutes_short" => get_string("nmp_minutes_short", "local_notemyprogress"),
        "seconds_short" => get_string("nmp_seconds_short", "local_notemyprogress"),

        "modules_access_chart_title" => get_string("nmp_modules_access_chart_title", "local_notemyprogress"),
        "modules_viewed" => get_string("nmp_modules_viewed", "local_notemyprogress"),
        "modules_no_viewed" => get_string("nmp_modules_no_viewed", "local_notemyprogress"),
        "modules_complete" => get_string("nmp_modules_complete", "local_notemyprogress"),
        "modules_interaction" => get_string("nmp_modules_interaction", "local_notemyprogress"),
        "modules_interactions" => get_string("nmp_modules_interactions", "local_notemyprogress"),
        "close_button" => get_string("nmp_close_button", "local_notemyprogress"),
    ],
    'resources_access_colors' => array('#06D6A0', '#FFD166', '#EF476F'),
    'inverted_time_colors' => array('#118AB2', '#06D6A0'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'indicators' => $reports->get_sessions(),
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/student_sessions', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/student_sessions', ['content' => $content]);
echo $OUTPUT->footer();