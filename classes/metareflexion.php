<?php

namespace local_notemyprogress;

require_once("lib_trait.php");


use stdClass;

class metareflexion
{
    use \lib_trait;

    public $course;
    public $user;
    public $weekcode;
    public $hours;
    public $metareflexionid;

    public $lastweekid;
    public $classroom_hours;
    public $hours_off_course;
    public $previous_class_learning;
    public $benefit_going_class;

    public $days;
    public $goals_committed;
    public $goals_completed;
    public $questions;

    function __construct($course, $userid, $idmetareflexion = null)
    {
        global $DB;

        $this->course = self::get_course($course);
        $this->user = self::get_user($userid);

        $configweeks = new \local_notemyprogress\configweeks($this->course->id, $this->user->id);
        $this->weeks = $configweeks->get_weeks_with_sections();
        $this->current_week = $configweeks->get_current_week();
        $this->past_week = $configweeks->get_past_week();

        $this->weekcode = null;
        $this->days = [];
        $this->hours = null;
        $this->goals_committed = [];
        $this->metareflexionid = $idmetareflexion;
    }

    /**
     * Get the days committed for a specific module
     *
     * @param string $id_module id of the module
     * @return object object with days committed
     */
    public function get_days_committed($id_module)
    {
        global $DB;
        $sql = "select dm.description from {notemyprogress_days_modules} dm where dm.id_course_modules = ?";
        $days = $DB->get_records_sql($sql, array($id_module));
        return $days;
    }
    
    /**
     * Get the week before the current week with its questions and answers assigned to their text
     *
     * @return object the last week
     */
    public function find_last_week()
    {
        $set_last_week = self::set_last_week();
        if (empty($set_last_week)) {
            $set_last_week = new stdClass();
        }
        $fq = self::find_questions($set_last_week);
        $set_last_week->questions = $fq;
        return $set_last_week;
    }
    
    /**
     * Get the last week with the answers
     *
     * @return object the last week with the answers
     */
    public function set_last_week()
    {
        global $DB;
        $last_week = new stdClass();
        $last_week->userid = $this->user->id;
        $last_week->weekcode = null;
        $last_week->classroom_hours = 0;
        $last_week->hours_off_course = 0;
        $last_week->objectives_reached = null;
        $last_week->previous_class_learning = null;
        $last_week->benefit_going_class = null;
        $last_week->feeling = null;
        if (empty($this->past_week)) {
            return null;
        } else {
            $sql =  "select * from {notemyprogress_last_weeks} where weekcode = ? and userid = ? order by id desc limit 1";
            $last_week_query = $DB->get_record_sql($sql, array($this->past_week->weekcode, $this->user->id));
            if (!empty($last_week_query)) {
                $last_week = $last_week_query;
                $last_week->weekcode = $this->past_week->weekcode;
            }
        }
        return $last_week;
    }

     /**
     * Get the questions and answers with their text
     * @param object last week without the answer selected
     * @return object the last week with the answers selecteds and the text associated
     */
    private function find_questions($last_week)
    {
        global $DB;
        $sql =  "select * from {notemyprogress_questions}";
        $questions = $DB->get_records_sql($sql);
        foreach ($questions as $key => $question) {
            $answers = self::find_answers($question->id);
            if ($question->id == 1) {
                $objectives_reached = isset($last_week->objectives_reached) ? $last_week->objectives_reached : null;
                $question->answer_selected = $objectives_reached;
            } elseif ($question->id == 2) {
                $previous_class_learning = isset($last_week->previous_class_learning) ? $last_week->previous_class_learning : null;
                $question->answer_selected = $previous_class_learning;
            } elseif ($question->id == 3) {
                $benefit_going_class = isset($last_week->benefit_going_class) ? $last_week->benefit_going_class : null;
                $question->answer_selected = $benefit_going_class;
            } elseif ($question->id == 4) {
                $feeling = isset($last_week->feeling) ? $last_week->feeling : null;
                $question->answer_selected = $feeling;
            }

            $question->answers = $answers;
        }
        $questions = self::update_texts_from_strings($questions);
        return $questions;
    }

     /**
     * Associates text to questions and answers
     *
     * @param object $questions questions without their text associated
     * @return object questions and answers with their text
     */
    private function update_texts_from_strings($questions)
    {
        foreach ($questions as $question) {
            $question->enunciated = get_string($question->enunciated, 'local_notemyprogress');
            foreach ($question->answers as $answer) {
                $answer->enunciated = get_string($answer->enunciated, 'local_notemyprogress');
            }
        }
        return $questions;
    }

     /**
     * Associates text to questions and answers
     *
     * @param string $id_questions id of the question
     * @return object questions and answers with their text
     */
    public function find_answers($id_questions)
    {
        global $DB;
        $sql =  "select * from {notemyprogress_answers} where questionid = ?";
        $answers = $DB->get_records_sql($sql, array($id_questions));
        return $answers;
    }


     /**
     * Get the current week schedule
     *
     * @param object reports object containing useful fonctions and corresponding to the current week
     * @return object the current week
     */
    public function find_current_week_new($reports)
    {
        $current_week = new stdClass();
        $current_week->weekly_cm = [];
        $array_current_week = array();
        $current_wk_schedule = self::get_schedules($this->current_week->weekcode);
        foreach ($this->current_week->sections as $key => $section) {
            $course_modules = self::get_sequence_section($section->sectionid);
            if ($course_modules["0"] === false){
            }
            else{
                foreach ($course_modules as $key => $cm) {
                    //$cm->modname = self::get_resource_type($cm->id);
                    $cm->modname = self::get_course_module_type($cm->module);
                    //$cm->modname = self::get_resource_type($cm->id);
                    if ($cm->modname != "label") {
                        array_push($current_week->weekly_cm, $cm);
                    }

                    // if ($current_wk_schedule) {
                    //     $days_committed_for_module = $this->get_days_committed($cm->id);
                    //     foreach ($days_committed_for_module as $key => $day) {
                    //         array_push($days_committed[$day->description], $cm->id);
                    //     }
                    // }
                }
            }
        }

        $current_week->weekly_schedules_id = null;
        $current_week->weekcode = $this->current_week->weekcode;
        
        $current_week->weekly_schedules_goals = [];
        $days_planned = array(
            "monday" => array(),
            "tuesday" => array(),
            "wednesday" => array(),
            "thursday" => array(),
            "friday" => array(),
            "saturday" => array(),
            "sunday" => array(),
        );
        $days_worked = array(
            "monday" => array(),
            "tuesday" => array(),
            "wednesday" => array(),
            "thursday" => array(),
            "friday" => array(),
            "saturday" => array(),
            "sunday" => array(),
        );
        $weekly_schedules_days = new stdClass();
        $weekly_schedules_days->days_planned = $days_planned;
        $weekly_schedules_days->days_worked = $days_worked;
        $current_week->weekly_schedules_days = $weekly_schedules_days;
       
        $current_week->weekly_schedules_hours = 0;
        $goals_cat = $this->get_goals_cat();
        //$goals_committed = array();
        if ($current_wk_schedule) {
             $current_week->weekly_schedules_id = $current_wk_schedule->id;
             $current_week->weekly_schedules_goals = $reports->goals_report_metereflexion($current_week->weekcode);
             $current_week->weekly_schedules_days = $reports->days_report_metereflexion($current_week->weekcode);
         }
         $current_week->weekly_schedules_hours = $reports->hours_report_metereflexion($current_week->weekcode);
        $current_week->goals_categories = $goals_cat;
        //$current_week->goals_committed = [];

        array_push($array_current_week, $current_week);
        return $array_current_week;
    }

    /**
     * Get the current week schedule's entry in the db for a specific weekcode
     *
     * @param string $weekcode code assigned to the week
     * @return object the current week entry
     */
    public function get_schedules($weekcode)
    {
        global $DB;
        $sql =  "select * from {notemyprogress_wk} where userid = ? and weekcode = ?";
        $cm_week_schedules = $DB->get_record_sql($sql, array($this->user->id, $weekcode));
        return $cm_week_schedules;
    }

    /**
     * Save the schedule the user has just created for the current week.
     *
     * @return object informations about the schedule entry
     */
    public function save_metareflexion()
    {
        if (!isset($this->weekcode) || !isset($this->days) || !isset($this->goals_committed) || !isset($this->hours)) {
            return false;
        }

        global $DB;
        //Add the weekly schedule to the notemyprogress_wk table
        $metareflexion = new stdClass();
        $metareflexion->userid = $this->user->id;
        $metareflexion->weekcode = $this->weekcode;
        $metareflexion->days = $this->days;
        $metareflexion->hours = $this->hours;
        $metareflexion->timecreated = self::now_timestamp();
        $metareflexion->timemodified = self::now_timestamp();
        $meta = $DB->insert_record("notemyprogress_wk", $metareflexion, true);

        //Add goals committed to the notemyprogress_goals table
        foreach ($this->goals_committed as $key => $id_goal_cat) {
            $goal = new stdClass();
            //$goal->userid = $this->user->id;
            $goal->id_goals_categories = $id_goal_cat;
            $goal->id_weekly_schedules = (self::get_schedules($this->weekcode))->id;
            //$goal->completed = 0;
            $DB->insert_record("notemyprogress_goals", $goal, true);

        }
        //Create days in notemyprogress_days table
        $day_descriptions = array("monday", "tuesday", "wednesday", "thursday", "friday", 'saturday', 'sunday');
        $d_array = json_decode($this->days, true);
        foreach ($day_descriptions as $day_desc) {
            $day = new stdClass();
            $day->userid = $this->user->id;
            $day->id_weekly_schedules = $meta;
            $day->description = $day_desc;
            //$id_day = $DB->insert_record("notemyprogress_days", $day, true);
            //Create an entry in notemyprogress_days_modules table for each day and module associated
            foreach ($d_array[$day_desc] as $key => $id_module) {
                $days_modules = new stdClass();
                $days_modules->id_course_modules = $id_module;
                //$days_modules->id_days = $id_day;
                $days_modules->id_weekly_schedules = $meta;
                $days_modules->day_description = $day_desc;
                
                $DB->insert_record("notemyprogress_days_modules", $days_modules, true);
            }
        }
        //return metareflexion created
        $this->metareflexionid = $meta;
        $metareflexion->id = $meta;
        return $metareflexion;
    }


    /**
     * Update the current week schedule in the db
     *
     * @return object informations about the schedule entry
     */
    public function update_metareflexion()
    {
        if (!isset($this->metareflexionid) || !isset($this->days) || !isset($this->goals_committed) || !isset($this->hours)) {
            return false;
        }
        global $DB;
        $metareflexion = new stdClass();
        $metareflexion->id = $this->metareflexionid;
        $metareflexion->hours = $this->hours;
        $metareflexion->timemodified = self::now_timestamp();
        $sql = "update {notemyprogress_wk} set hours = ?, timemodified = ? where id = ?";
        $result = $DB->execute($sql, array($this->hours, self::now_timestamp(), $this->metareflexionid));

        // //delete or add goals committed to the notemyprogress_goals table
        // $sql = "select id from {notemyprogress_goals}";
        // $query_goals = $DB->get_records_sql($sql);
        // $goals_stored = array();
        // foreach ($query_goals as $key => $goal) {
        //     array_push($goal->id);
        // }
        // $goals_to_add = array_diff($this->goals_committed, $goals_stored);
        // $goals_to_remove = array_diff($goals_stored, $this->goals_committed);
        // //Add the goals in $this->goals_committed and absent in the notemyprogress_goals table
        // foreach ($goals_to_add as $key => $id_goal) {
        //     $goal = new stdClass();
        //     $goal->userid = $this->user->id;
        //     $goal->id_goals_categories = $id_goal;
        //     $goal->weekcode = $this->weekcode;
        //     $goal->completed = 0;
        //     $DB->insert_record("notemyprogress_goals", $goal, true);
        // }
        // //Delete the goals absent in $this->goals_committed and currently stored in the notemyprogress_goals table
        // foreach ($goals_to_remove as $key => $id_goal) {
        //     $sql = "delete from {notemyprogress_goals} where id = ?";
        //     $result = $DB->execute($sql, array($id_goal));
        // }

        //Delete previous notemyprogress_goals entries
        $sql = "delete from {notemyprogress_goals} where id_weekly_schedules = ? ";
        $result = $DB->execute($sql,array($metareflexion->id));
        //Add entries
        foreach ($this->goals_committed as $key => $id_goal) {
            $goal = new stdClass();
            //$goal->userid = $this->user->id;
            $goal->id_goals_categories = $id_goal;
            $goal->id_weekly_schedules = (self::get_schedules($this->weekcode))->id;
            //$goal->completed = 0;
            $DB->insert_record("notemyprogress_goals", $goal, true);
        }

        //Delete previous notemyprogress_days_modules entries
        $sql = "delete from {notemyprogress_days_modules} where id_weekly_schedules = ?";
        $result = $DB->execute($sql,array($metareflexion->id));
        //Add entries
        $d_array = json_decode($this->days, true);
        foreach ($d_array as $key_day => $day) {
            //$sql = "select id from {notemyprogress_days} where id_weekly_schedules = ? and description = ? ";
            //$id_day = $DB->get_record_sql($sql, array($this->metareflexionid, $key));
            //Create an entry in notemyprogress_days_modules table for each day and module associated
            foreach ($day as $key => $id_module) {
                $days_modules = new stdClass();
                $days_modules->id_course_modules = $id_module;
                $days_modules->id_weekly_schedules = $this->metareflexionid;
                $days_modules->day_description = $key_day;

                //$days_modules->id_days = $id_day->id;
                $DB->insert_record("notemyprogress_days_modules", $days_modules, true);
            }
        }
        return $metareflexion;
    }

    
    /**
     * Save the answers to the reflexion questionnaire
     *
     * @return object informations about the answers to the reflexion questionnaire
     */
    public function save_lastweek()
    {
        global $DB;
        $lastweek = new stdClass();
        $lastweek->userid = $this->user->id;
        $lastweek->weekcode = $this->weekcode;
        $lastweek->classroom_hours = $this->classroom_hours;
        $lastweek->hours_off_course = $this->hours_off_course;
        $lastweek->objectives_reached =  $this->objectives_reached;
        $lastweek->previous_class_learning = $this->previous_class_learning;
        $lastweek->benefit_going_class = $this->benefit_going_class;
        $lastweek->feeling = $this->feeling;
        $id = $DB->insert_record("notemyprogress_last_weeks", $lastweek, true);
        $lastweek->id = $id;
        return $lastweek;
    }

    /**
     * Updates the answers to the reflexion questionnaire
     *
     * @return object informations about the answers to the reflexion questionnaire
     */
    public function update_lastweek()
    {
         if (!isset($this->lastweekid) || !isset($this->classroom_hours) || !isset($this->hours_off_course) || !isset($this->previous_class_learning) || !isset($this->benefit_going_class)) {
             return false;
         }
        global $DB;
        $lastweek = new stdClass();
        $lastweek->id = $this->lastweekid;
        $lastweek->classroom_hours = $this->classroom_hours;
        $lastweek->hours_off_course = $this->hours_off_course;
        $lastweek->objectives_reached =  $this->objectives_reached;
        $lastweek->previous_class_learning = $this->previous_class_learning;
        $lastweek->benefit_going_class = $this->benefit_going_class;
        $lastweek->feeling = $this->feeling;
        $sql = "update {notemyprogress_last_weeks} set classroom_hours = ?, hours_off_course = ?,objectives_reached = ?, previous_class_learning = ?, benefit_going_class = ? ,feeling = ?  where id = ?";
        $result = $DB->execute($sql, array($lastweek->classroom_hours,  $lastweek->hours_off_course,$lastweek->objectives_reached ,$lastweek->previous_class_learning, $lastweek->benefit_going_class,$lastweek->feeling, $lastweek->id));
        return $lastweek;
    }
    
    /**
     * Get all goals categories from the notemyprogress_goals_ct table
     *
     * @return object all goal categories
     */
    public function get_goals_cat()
    {
        global $DB;
        $sql =  "select * from {notemyprogress_goals_ct}";
        $goals_cat_res = $DB->get_records_sql($sql);
        return $goals_cat_res;
    }
}
