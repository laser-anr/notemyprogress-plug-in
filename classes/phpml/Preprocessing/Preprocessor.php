<?php

declare(strict_types=1);

namespace local_notemyprogress\phpml\Preprocessing;

use local_notemyprogress\phpml\Transformer;

interface Preprocessor extends Transformer
{
}
