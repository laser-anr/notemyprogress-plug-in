<?php

namespace local_notemyprogress\jwt;

class SignatureInvalidException extends \UnexpectedValueException
{
}
