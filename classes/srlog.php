<?php
require_once dirname(__FILE__) . '/../../../course/lib.php';
class local_student_reports_srlog
{

  public static function create($component, $action, $userid, $courseid)
  {
    global $DB;
    $user = self::get_user($userid);
    $course = self::get_course($courseid);
    $log = new stdClass();
    $log->userid = $user->id;
    $log->username = $user->username;
    $log->name = $user->firstname;
    $log->lastname = $user->lastname;
    $log->email = $user->email;
    $log->current_roles = self::get_user_roles($courseid, $userid);
    $log->courseid = $course->id;
    $log->coursename = $course->fullname;
    $log->courseshortname = $course->shortname;
    $log->component = $component;
    $log->action = $action;
    $log->timecreated = time();
    $id = $DB->insert_record("notemyprogress_logs", $log, true);
    $log->id = $id;
    return $log;
  }

  public static function get_user_roles($courseid, $userid)
  {
    $user_roles = array();
    $admins = array_values(get_admins());
    foreach ($admins as $admin) {
      if ($admin->id == $userid) {
        $user_roles[] = 'admin';
      }
    }
    $context = context_course::instance($courseid);
    $roles = get_user_roles($context, $userid);
    foreach ($roles as $role) {
      $user_roles[] = $role->shortname;
    }
    $user_roles = implode(', ', $user_roles);
    return $user_roles;
  }

  public static function get_user($userid)
  {
    global $DB;
    $sql = "select * from {user} where id = ?";
    $user = $DB->get_record_sql($sql, array($userid));
    return $user;
  }

  public static function get_course($courseid)
  {
    global $DB;
    $sql = "select * from {course} where id = ?";
    $user = $DB->get_record_sql($sql, array($courseid));
    return $user;
  }

  public static function create_file($courseid)
  {
    self::remove_old_logs_from_disk($courseid);
    set_time_limit(0);
    global $DB;

    $headers = array(
      get_string('logs_header_logid', 'local_notemyprogress'),
      get_string('logs_header_userid', 'local_notemyprogress'),
      get_string('logs_header_username', 'local_notemyprogress'),
      get_string('logs_header_names', 'local_notemyprogress'),
      get_string('logs_header_lastnames', 'local_notemyprogress'),
      get_string('logs_header_email', 'local_notemyprogress'),
      get_string('logs_header_roles', 'local_notemyprogress'),
      get_string('logs_header_courseid', 'local_notemyprogress'),
      get_string('logs_header_component', 'local_notemyprogress'),
      get_string('logs_header_action', 'local_notemyprogress'),
      get_string('logs_header_timecreated', 'local_notemyprogress')
    );

    $logs = $DB->get_recordset("notemyprogress_logs", array("courseid" => $courseid));
    $path = dirname(__FILE__) . "/../downloads/";
    $filename = "Registros" . "_curso__" . $courseid . "__.csv";

    $file = fopen($path . $filename, 'w');
    fprintf($file, chr(0xEF) . chr(0xBB) . chr(0xBF));
    fputcsv($file, $headers, ";");
    foreach ($logs as $log) {
      $date = DateTime::createFromFormat('U', $log->timecreated);
      $date->setTimeZone(new DateTimeZone('UTC'));
      $log->timecreated = $date->format('Y-m-d H:i:s e');
      fputcsv($file, (array) $log, ";");
    }
    fclose($file);
    $logs->close();

    return $filename;
  }

  public static function remove_old_logs_from_disk($courseid)
  {
    $path = dirname(__FILE__) . "/../downloads";
    $files = glob($path . '/*');
    foreach ($files as $file) {
      if (is_file($file)) {
        $route_parts = explode("__", $file);
        foreach ($route_parts as $route_part) {
          if ($route_part == $courseid) {
            unlink($file);
          }
        }
      }
    }
  }
}
