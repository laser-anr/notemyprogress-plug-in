<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax Script
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @author      Edisson Sigua, Bryan Aguilar, 2021 Éric Bart <bart.eric@hotmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define('AJAX_SCRIPT', true);

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/locallib.php');


global $USER, $COURSE, $DB;


$courseid = required_param('courseid', PARAM_INT);
$userid = required_param('userid', PARAM_INT);  

$COURSE = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$USER = $DB->get_record('user', array('id' => $userid), '*', MUST_EXIST);

require_login($COURSE, false);
$context = context_course::instance($courseid);
require_capability('local/notemyprogress:ajax', $context);
/*                    Start the déclaration of all the optional parrameters                               */
$beginDate = optional_param('beginDate', false, PARAM_TEXT); //optional_param('keyValue','DefaultValue','Type value');
$lastDate = optional_param('lastDate', false, PARAM_TEXT);

$sectionname = optional_param('sectionname', false, PARAM_TEXT);
$actiontype = optional_param('actiontype', false, PARAM_TEXT);
$objectType = optional_param('objectType', false, PARAM_TEXT);
$objectName = optional_param('objectName', false, PARAM_TEXT);
$objectDescription = optional_param('objectDescription', false, PARAM_TEXT);
$currentUrl = optional_param('currentUrl', false, PARAM_TEXT);

$scriptname = optional_param('scriptname', false, PARAM_ALPHA);

$action = optional_param('action', false, PARAM_ALPHA);
$weeks = optional_param('weeks', false, PARAM_RAW);
$profile = optional_param('profile', false, PARAM_RAW);
//debug
//$profile = "student";

$weekcode = optional_param('weekcode', false, PARAM_INT);
$dayscommitted = optional_param('days', false, PARAM_TEXT);
$goalscommitted =  optional_param('goals', false, PARAM_TEXT);
$hourscommitted = optional_param('hours', false, PARAM_INT);
$idmetareflexion = optional_param('metareflexionid', false, PARAM_INT);
$auto_eval_answers = optional_param('auto_eval_answers', false, PARAM_RAW);

$lastweekid = optional_param('lastweekid',  false,  PARAM_RAW);
$classroom_hours = optional_param('classroom_hours',  false,  PARAM_RAW);
$hours_off = optional_param('hours_off',  false,  PARAM_RAW);

$objectives_reached = optional_param('objectives_reached',  false,  PARAM_RAW);
$previous_class = optional_param('previous_class',  false,  PARAM_RAW);
$benefit = optional_param('benefit',  false,  PARAM_RAW);
$feeling = optional_param('feeling',  false,  PARAM_RAW);

$groupid = optional_param('groupid',  null,  PARAM_INT);

$subject = optional_param('subject', false, PARAM_TEXT);
$recipients = optional_param('recipients', false, PARAM_TEXT);
$text = optional_param('text', false, PARAM_TEXT);
$moduleid = optional_param('moduleid', false, PARAM_INT);
$modulename = optional_param('modulename', false, PARAM_TEXT);

$newinstance = optional_param('newinstance', false, PARAM_BOOL);

//gamification parameters
$rules = optional_param('rules', false ,PARAM_TEXT);
$levels = optional_param('levels', false ,PARAM_TEXT);
$settings= optional_param('settings', false ,PARAM_TEXT);
$url= optional_param('url', false ,PARAM_TEXT);
//$courseid = optional_param('modulename', false ,PARAM_TEXT);
$enable = optional_param('enable', false ,PARAM_BOOL);
$section = optional_param('section',false,PARAM_TEXT);

/*                                               */ 

$params = array(); // Array containing the data
$func = null; // fuction which will be call at line 193

if($action == 'saveconfigweek') {//Exemple: if the action passed is saveconfigweek then my array contain (weeks,courseid,userid,newinstance)
    array_push($params, $weeks);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $newinstance);

    if($weeks && $courseid && $userid){

        $func = "local_notemyprogress_save_weeks_config"; //this function must be contained in localib.php
    }
} elseif ($action == 'changegroup') {
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $groupid);
    if ($courseid && $userid) {
        $func = "local_notemyprogress_change_group";
    }
} elseif ($action == 'worksessions') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_notemyprogress_get_sessions";
    }
} elseif ($action == 'time') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_notemyprogress_get_inverted_time";
    }
} elseif ($action == 'assignments') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_notemyprogress_get_assignments_submissions";
    }
} elseif ($action == 'sendmail') {
    array_push($params, $COURSE);
    array_push($params, $USER);
    array_push($params, $subject);
    array_push($params, $recipients);
    array_push($params, $text);
    array_push($params, $moduleid);
    array_push($params, $modulename);
    if ($subject && $recipients && $text) {
        $func = "local_notemyprogress_send_email";
    }
} elseif ($action == 'quiz') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_notemyprogress_get_quiz_attempts";
    }
} elseif ($action == 'dropoutdata') {
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($courseid && $userid && $profile) {
        $func = "local_notemyprogress_generate_dropout_data";
    }
} elseif ($action == 'studentsessions') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);
    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_notemyprogress_get_student_sessions";
    }
} elseif ($action == 'savemetareflexion') {
    array_push($params, $userid);
    array_push($params, $weekcode);
    array_push($params, $dayscommitted);
    array_push($params, $goalscommitted);
    array_push($params, $hourscommitted);
    array_push($params, $courseid);
    array_push($params, $url);
    if (isset($userid , $weekcode , $dayscommitted , $goalscommitted , $hourscommitted , $courseid)) {
        $func = "local_sr_save_metareflexion";
    }
} elseif ($action == 'updatemetareflexion') {
    array_push($params, $userid);
    array_push($params, $weekcode);
    array_push($params, $idmetareflexion);
    array_push($params, $dayscommitted);
    array_push($params, $goalscommitted);
    array_push($params, $hourscommitted);
    array_push($params, $courseid);
    array_push($params, $url);
    if (isset($userid, $weekcode, $idmetareflexion, $dayscommitted, $goalscommitted, $hourscommitted, $courseid)) {
        $func = "local_sr_update_metareflexion";
    }
} elseif ($action == 'savelastweek') {
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $weekcode);
    array_push($params, $classroom_hours);
    array_push($params, $classroom_hours);
    array_push($params, $objectives_reached);
    array_push($params, $previous_class);
    array_push($params, $benefit);
    array_push($params, $feeling);
    if (isset($courseid , $userid ,$weekcode,$classroom_hours,$classroom_hours, $objectives_reached , $previous_class , $benefit , $feeling)) {
        $func = "local_sr_save_lastweek";
    }
} elseif ($action == 'updatelastweek') {
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $lastweekid);
    array_push($params, $classroom_hours);
    array_push($params, $hours_off);
    array_push($params, $objectives_reached);
    array_push($params, $previous_class);
    array_push($params, $benefit);
    array_push($params, $feeling);
  
    if (isset($courseid , $userid , $lastweekid , $objectives_reached  , $previous_class , $benefit , $feeling)) {
      $func = "local_sr_update_lastweek";
    }
  } elseif ($action == 'metareflexionrepotgetweek') {
    array_push($params, $weekcode);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $profile);

    if ($weekcode && $courseid && $userid && $profile) {
        $func = "local_sr_metareflexion_get_week";
    }
} elseif ($action == 'downloadMOODLElogs') {
    array_push($params, $beginDate);
    array_push($params, $lastDate);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $currentUrl);

    if ($lastDate && $beginDate && $courseid && $userid && $currentUrl) {
        $func = "local_notemyprogress_downloadMoodleLogs";
    }
} elseif ($action == 'downloadNMPlogs') {
    array_push($params, $beginDate);
    array_push($params, $lastDate);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $currentUrl);

    if ($lastDate && $beginDate && $courseid && $userid && $currentUrl) {
        $func = "local_notemyprogress_logsNMP";
    }
} elseif ($action == 'addLogs') {
    array_push($params, $sectionname);
    array_push($params, $actiontype);
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $objectType);
    array_push($params, $objectName);
    array_push($params, $currentUrl);
    array_push($params, $objectDescription);

    if ($courseid && $userid && $sectionname && $actiontype && $objectType && $objectName && $objectDescription && $currentUrl) {
        $func = "local_notemyprogress_addLogs";
    }
}elseif($action =='savegamification'){
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $rules);
    array_push($params, $levels);
    array_push($params, $settings);
    array_push($params, $url);
    array_push($params, $section);
    if($courseid && $userid && $rules && $levels && $settings && $url) {
        $func = "local_notemyprogress_save_gamification_config";
    }
}elseif($action =='rankable'){
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params, $url);
    if($courseid && $userid) {
        $func = "local_notemyprogress_set_rankable_player";
    }
}elseif($action =='saveEnable'){
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params,$enable);
    array_push($params,$url);
    if($courseid && $userid) {
        $func = "local_notemyprogress_save_enable";
    }
}elseif($action =='studentGamificationViewed'){
    array_push($params, $courseid);
    array_push($params, $userid);
    array_push($params,$url);
    if($courseid && $userid) {
        $func = "local_notemyprogress_viewed_student_lvl";
    }
}elseif($action == 'saveautoeval') {
    array_push($params, $userid);
    array_push($params, $courseid);
    array_push($params, $auto_eval_answers);

    if($userid && $auto_eval_answers){
        $func = "local_notemyprogress_save_auto_eval";
    }
}elseif($action == 'updateautoeval') {
    array_push($params, $userid);
    array_push($params, $courseid);
    array_push($params, $auto_eval_answers);

    if($userid && $auto_eval_answers){
        $func = "local_notemyprogress_update_auto_eval";
    }
}

if (isset($params) && isset($func)) {
    call_user_func_array($func, $params);
    
}else{
    $message = get_string('api_invalid_data', 'local_notemyprogress');
    local_notemyprogress_ajax_response(array($message));//,$action,$courseid, $userid ,$rules , $levels, $settings, $url,$func), 442);
}


function local_notemyprogress_save_auto_eval($userid,$courseid, $auto_eval_answers)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", "section", "Auto_Evaluation", "auto_evaluation", $url, "AutoEvaluationSaved");
    $auto_evaluation = new \local_notemyprogress\auto_evaluation($userid);
    $auto_evaluation->auto_eval_answers = json_decode($auto_eval_answers) ;
    $response = $auto_evaluation->save_answers();
    local_notemyprogress_ajax_response(array('responseautoevaluation' => $response));
}

function local_notemyprogress_update_auto_eval($userid,$courseid, $auto_eval_answers)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Updated", "section", "Auto_Evaluation", "auto_evaluation", $url, "AutoEvaluationUpdated");
    $auto_evaluation = new \local_notemyprogress\auto_evaluation($userid);
    $auto_evaluation->auto_eval_answers = json_decode($auto_eval_answers) ;
    $response = $auto_evaluation->update_answers();
    local_notemyprogress_ajax_response(array('responseautoevaluation' => $response));
}

function local_sr_metareflexion_get_week($weekcode, $courseid, $userid, $profile)
{

    if ($profile == "teacher") {
        $reports = new \local_notemyprogress\teacher($courseid, $userid);
    } else {
        $reports = new \local_notemyprogress\student($courseid, $userid);
        $teacher = new \local_notemyprogress\teacher($courseid, $userid);
    }
    $interactions_goals = $reports->goals_report_metereflexion($weekcode);
    $interactions_days = $reports->days_report_metereflexion($weekcode);
    $interactions_hours = $reports->hours_report_metereflexion($weekcode);
    $interactions_questions = $reports->questions_report_metereflexion($weekcode);
    $status_planning = $reports->status_planning($weekcode);
    //$response = array("interactions_days" => $interactions_days, "interactions_hours" => $interactions_hours, "status_planning" => $status_planning);
    $response = array("interactions_hours" => $interactions_hours,"interactions_days" => $interactions_days,"interactions_goals" => $interactions_goals,"interactions_questions" => $interactions_questions, "status_planning" => $status_planning);
    //if($profile == 'student'){
    $course_interaction = $teacher->hours_report_metereflexion($weekcode);
    $response['course_report_hours'] = $course_interaction;
    //}
    //$response['students_planification'] = $reports->students_planification_summary($weekcode);
    local_notemyprogress_ajax_response($response);
}


function local_sr_save_metareflexion($userid, $weekcode, $dayscommitted, $goalscommitted, $hourscommitted, $courseid,$url)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", "Meta_Reflection", "LOGFILES", "nmp", $currentUrl, "MetaReflectionSaved");

    $metareflexion = new \local_notemyprogress\metareflexion($courseid, $userid);
    $metareflexion->weekcode = $weekcode;
    $metareflexion->days = $dayscommitted;
    $metareflexion->goals_committed = $goalscommitted;
    $metareflexion->hours = $hourscommitted;

    $metareflexionid = $metareflexion->save_metareflexion();
    local_notemyprogress_ajax_response(array('responsemetareflexion' => $metareflexionid));
}

function local_sr_update_metareflexion($userid, $weekcode, $idmetareflexion, $dayscommitted, $goalscommitted, $hourscommitted, $courseid,$url)
{   
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Updated", "section", "Meta_Reflection", "student_gamification", $url, "MetaReflectionUpdated");
    $metareflexion = new \local_notemyprogress\metareflexion($courseid, $userid, $idmetareflexion);
    $metareflexion->id = $idmetareflexion;
    $metareflexion->weekcode = $weekcode;
    $metareflexion->days = $dayscommitted;
    $metareflexion->goals_committed = $goalscommitted;
    $metareflexion->hours = $hourscommitted;

    $metareflexionid = $metareflexion->update_metareflexion();
    local_notemyprogress_ajax_response(array('responsemetareflexion' => $metareflexionid));
}

function local_sr_save_lastweek($courseid, $userid,$weekcode ,$classroom_hours, $hours_off, $objectives_reached, $previous_class, $benefit, $feeling)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", "Meta_Reflection", "LOGFILES", "nmp", $currentUrl, "MetaReflectionSaved");
    $lastweek = new \local_notemyprogress\metareflexion($courseid, $userid);
    $lastweek->classroom_hours = $classroom_hours;
    $lastweek->hours_off_course = $hours_off;
    $lastweek->weekcode = $weekcode;
    $lastweek->objectives_reached = $objectives_reached;
    $lastweek->previous_class_learning = $previous_class;
    $lastweek->benefit_going_class = $benefit;
    $lastweek->feeling = $feeling;

    $lastweekresponse = $lastweek->save_lastweek();
    local_notemyprogress_ajax_response(array('response_save_last_week' => $lastweekresponse));
}

function local_sr_update_lastweek($courseid, $userid, $lastweekid, $classroom_hours, $hours_off, $objectives_reached, $previous_class, $benefit, $feeling)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Updated", "section", "Meta_Reflection", "student_gamification", $url, "MetaReflectionUpdated");
  $lastweek = new \local_notemyprogress\metareflexion($courseid, $userid);
  $lastweek->lastweekid = $lastweekid;
  $lastweek->classroom_hours = $classroom_hours;
  $lastweek->hours_off_course = $hours_off;
  $lastweek->objectives_reached = $objectives_reached;
  $lastweek->previous_class_learning = $previous_class;
  $lastweek->benefit_going_class = $benefit;
  $lastweek->feeling = $feeling;

  $lastweekresponse = $lastweek->update_lastweek();
  local_notemyprogress_ajax_response(array('response_update_last_week' => $lastweekresponse));
}

function local_notemyprogress_logsNMP($beginDate, $lastDate, $courseid, $userid, $currentUrl) {
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("downloaded", "logfile", "LOGFILES", "nmp", $currentUrl, "File that contains all the activities performed on the moodle course in a time interval");
    $jsonData = $logs->searchLogsNMP($beginDate, $lastDate);
    local_notemyprogress_ajax_response(array("jsonData" => $jsonData));
}

function local_notemyprogress_save_weeks_config($weeks, $courseid, $userid, $newinstance)
{
    $weeks = json_decode($weeks);
    $configweeks = new \local_notemyprogress\configweeks($courseid, $userid);
    if ($newinstance) {
        $configweeks->create_instance();
    }
    $configweeks->last_instance();
    $configweeks->save_weeks($weeks);
    $configweeks = new \local_notemyprogress\configweeks($courseid, $userid);
    local_notemyprogress_ajax_response(["settings" => $configweeks->get_settings()]);
}

function local_notemyprogress_change_group($courseid, $userid, $groupid)
{
    set_time_limit(300);
    global $DB;
    if (is_null($groupid)) {
        $groupid = 0;
    }
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $user = $DB->get_record('user', array('id' => $userid), '*', MUST_EXIST);
    $group_manager = new \local_notemyprogress\group_manager($course, $user);
    $participants = new \local_notemyprogress\course_participant($user->id, $course->id);
    $groups = array_values($participants->current_user_groups_with_all_group($course->groupmode));
    $selectedgroupid = $group_manager->selected_group()->groupid;
    $groups = local_notemyprogress_add_selected_property($groups, $selectedgroupid);
    $group_manager->set_group($groupid);
    $groups = local_notemyprogress_get_groups($course, $user);
    local_notemyprogress_ajax_response(array("groups" => $groups));
}

function local_notemyprogress_get_sessions($weekcode, $courseid, $userid, $profile)
{
    set_time_limit(300);
    $reports = new \local_notemyprogress\teacher($courseid, $userid);
    $indicators = $reports->get_sessions($weekcode);
    $body = array("indicators" => $indicators);
    local_notemyprogress_ajax_response($body);
}

function local_notemyprogress_get_inverted_time($weekcode, $courseid, $userid, $profile)
{
    set_time_limit(300);
    if ($profile == "teacher") {
        $reports = new \local_notemyprogress\teacher($courseid, $userid);
    } else {
        $reports = new \local_notemyprogress\student($courseid, $userid);
    }
    $inverted_time = $reports->inverted_time($weekcode);
    $body = array(
        "inverted_time" => $inverted_time,
    );
    local_notemyprogress_ajax_response($body);
}

function local_notemyprogress_get_assignments_submissions($weekcode, $courseid, $userid, $profile)
{
    set_time_limit(300);
    if ($profile == "teacher") {
        $reports = new \local_notemyprogress\teacher($courseid, $userid);
    } else {
        $reports = new \local_notemyprogress\student($courseid, $userid);
    }
    $submissions = $reports->assignments_submissions($weekcode);
    $access = $reports->resources_access($weekcode);
    $body = array(
        "submissions" => $submissions,
        "access" => $access,
    );
    local_notemyprogress_ajax_response($body);
}


function local_notemyprogress_send_email($course, $user, $subject, $recipients, $text, $moduleid, $modulename)
{
    set_time_limit(300);
    $email = new \local_notemyprogress\email($course, $user);
    $email->sendmail($subject, $recipients, $text, $moduleid, $modulename);

    $body = array(
        "data" => [$subject, $recipients, $text, $moduleid],
    );
    local_notemyprogress_ajax_response($body);
}

function local_notemyprogress_get_quiz_attempts($weekcode, $courseid, $userid, $profile)
{
    set_time_limit(300);
    if ($profile == "teacher") {
        $reports = new \local_notemyprogress\teacher($courseid, $userid);
    } else {
        $reports = new \local_notemyprogress\student($courseid, $userid);
    }
    $quiz = $reports->quiz_attempts($weekcode);
    $body = array(
        "quiz" => $quiz,
    );
    local_notemyprogress_ajax_response($body);
}

function local_notemyprogress_generate_dropout_data($courseid, $userid, $profile)
{
    set_time_limit(300);
    if ($profile == "teacher") {
        $dropout = new \local_notemyprogress\dropout($courseid, $userid);
        $dropout->generate_data();
        local_notemyprogress_ajax_response([], "ok", true, 200);
    } else {
        local_notemyprogress_ajax_response([], "", false, 400);
    }
}

function local_notemyprogress_get_student_sessions($weekcode, $courseid, $userid, $profile)
{
    set_time_limit(300);
    $reports = new \local_notemyprogress\student($courseid, $userid);
    $indicators = $reports->get_sessions($weekcode, false);
    $body = array(
        "indicators" => $indicators,
    );
    local_notemyprogress_ajax_response($body);
}

function local_notemyprogress_downloadMoodleLogs($beginDate, $lastDate, $courseid, $userid, $currentUrl)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("downloaded", "logfile", "LOGFILES", "moodle", $currentUrl, "File that contains all the activities performed on the Note My Progress plugin in a time interval");
    $jsonData = $logs->searchLogsMoodle($beginDate, $lastDate);
    local_notemyprogress_ajax_response(array("jsonData" => $jsonData));
}

function local_notemyprogress_addLogs($sectionname, $actiontype, $courseid, $userid, $objectType, $objectName, $currentUrl, $objectDescription)
{
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP($actiontype, $objectType, $sectionname, $objectName, $currentUrl, $objectDescription);
    local_notemyprogress_ajax_response(array("ok"=>"ok"));
}
function local_notemyprogress_save_gamification_config($courseid, $userid, $rules, $levels, $settings, $url,$section){
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", $section, "CONFIGURATION_GAMIFICATION", "configuration_gamification", $url, "GamificationSaved");

     $configLevels = new \local_notemyprogress\configgamification($courseid, $userid);
     $configLevels->save_levels($levels, $settings, $rules);
     //$configLevels->save_enable($enable);
     $message = get_string('nmp_api_save_successful', 'local_notemyprogress');
     local_notemyprogress_ajax_response($message);
 }

function local_notemyprogress_set_rankable_player($courseid, $userid,$url){
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", "section", "STUDENT_GAMIFICATION", "student_gamification", $url, "GamificationSaved");
    GLobal $DB;
    $sql = "update {notemyprogress_xp} set rankable = ? where courseid = ? and userid = ?";
    $DB->execute($sql, array(1,$courseid, $userid));
    // $sql = "UPDATE {notemyprogress_xp} SET rankable = 1 WHERE courseid = 2 AND userid = 3";
    // $DB->execute($sql);
    $message = get_string('nmp_api_save_successful', 'local_notemyprogress');
    local_notemyprogress_ajax_response($message);
}

function local_notemyprogress_save_enable($courseid, $userid, $enable,$url){
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Saved", "section", "CONFIGURATION_GAMIFICATION", "configuration_gamification", $url, "GamificationSaved");
     $configLevels = new \local_notemyprogress\configgamification($courseid, $userid);
     $configLevels->save_enable($enable);
     $message = get_string('nmp_api_save_successful', 'local_notemyprogress');
     local_notemyprogress_ajax_response($message);
 }
 function local_notemyprogress_viewed_student_lvl($courseid, $userid,$url){
    $logs = new \local_notemyprogress\logs($courseid, $userid);
    $logs->addLogsNMP("Viewed", "section", "STUDENT_GAMIFICATION", "student_gamification", $url, "GamificationSaved");
    //  $configLevels = new \local_notemyprogress\configgamification($courseid, $userid);
    //  $configLevels->save_enable($enable);
    //  $message = get_string('nmp_api_save_successful', 'local_notemyprogress');
    //  local_notemyprogress_ajax_response($message);
 }