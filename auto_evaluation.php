<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/auto_evaluation.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_student', $context);
require_capability('local/notemyprogress:auto_evaluation', $context);

if (is_siteadmin()) {
    print_error(get_string("only_student", "local_notemyprogress"));
}

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "AUTO_EVALUATION", "auto_evaluation", $actualLink, "A page containing a form with question for the students");

$reports = new \local_notemyprogress\student($COURSE->id, $USER->id);
$auto_evaluation = new \local_notemyprogress\auto_evaluation($USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => [
        "section_help_title" => get_string("ss_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("ss_section_help_description", "local_notemyprogress"),
        "inverted_time_help_title" => get_string("ss_inverted_time_help_title", "local_notemyprogress"),
        "inverted_time_help_description_p1" => get_string("ss_inverted_time_help_description_p1", "local_notemyprogress"),
        "inverted_time_help_description_p2" => get_string("ss_inverted_time_help_description_p2", "local_notemyprogress"),
        "hours_session_help_title" => get_string("ss_hours_session_help_title", "local_notemyprogress"),
        "hours_session_help_description_p1" => get_string("ss_hours_session_help_description_p1", "local_notemyprogress"),
        "hours_session_help_description_p2" => get_string("ss_hours_session_help_description_p2", "local_notemyprogress"),
        "resources_access_help_title" => get_string("ss_resources_access_help_title", "local_notemyprogress"),
        "resources_access_help_description_p1" => get_string("ss_resources_access_help_description_p1", "local_notemyprogress"),
        "resources_access_help_description_p2" => get_string("ss_resources_access_help_description_p2", "local_notemyprogress"),
        "resources_access_help_description_p3" => get_string("ss_resources_access_help_description_p3", "local_notemyprogress"),

        "title" => get_string("nmp_title", "local_notemyprogress"),

        "no_data" => get_string("no_data", "local_notemyprogress"),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name", "local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to", "local_notemyprogress"),
        "pagination_title" => get_string("pagination_title", "local_notemyprogress"),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),

        "btn_save_auto_eval" => get_string("btn_save_auto_eval", "local_notemyprogress"),
        "message_save_successful" => get_string("message_save_successful", "local_notemyprogress"),
        "message_update_successful" => get_string("message_update_successful", "local_notemyprogress"),
        "page_title_auto_eval" => get_string("page_title_auto_eval", "local_notemyprogress"),
        "form_questions_auto_eval" => get_string("form_questions_auto_eval", "local_notemyprogress"),
        "description_auto_eval" => get_string("description_auto_eval", "local_notemyprogress"),
        "qsts" => array(
            "qst1" => get_string("qst1", "local_notemyprogress"),
            "qst2" => get_string("qst2", "local_notemyprogress"),
            "qst3" => get_string("qst3", "local_notemyprogress"),
            "qst4" => get_string("qst4", "local_notemyprogress"),
            "qst5" => get_string("qst5", "local_notemyprogress"),
            "qst6" => get_string("qst6", "local_notemyprogress"),
            "qst7" => get_string("qst7", "local_notemyprogress"),
            "qst8" => get_string("qst8", "local_notemyprogress"),
            "qst9" => get_string("qst9", "local_notemyprogress"),
            "qst10" => get_string("qst10", "local_notemyprogress"),
            "qst11" => get_string("qst11", "local_notemyprogress"),
            "qst12" => get_string("qst12", "local_notemyprogress"),
            "qst13" => get_string("qst13", "local_notemyprogress"),
            "qst14" => get_string("qst14", "local_notemyprogress"),
            "qst15" => get_string("qst15", "local_notemyprogress"),
            "qst16" => get_string("qst16", "local_notemyprogress"),
            "qst17" => get_string("qst17", "local_notemyprogress"),
            "qst18" => get_string("qst18", "local_notemyprogress"),
            "qst19" => get_string("qst19", "local_notemyprogress"),
            "qst20" => get_string("qst20", "local_notemyprogress"),
            "qst21" => get_string("qst21", "local_notemyprogress"),
            "qst22" => get_string("qst22", "local_notemyprogress"),
            "qst23" => get_string("qst23", "local_notemyprogress"),
            "qst24" => get_string("qst24", "local_notemyprogress"),
        )

    ],
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'indicators' => $reports->get_sessions(),
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    'auto_eval_answers' => $auto_evaluation->get_auto_eval_answers(),
    'auto_eval_saved' => $auto_evaluation->is_saved(),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/auto_evaluation', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/auto_evaluation', ['content' => $content]);
echo $OUTPUT->footer();
