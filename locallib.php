<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions for Flip my Learning.
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @author      Edisson Sigua
 * @author      Bryan Aguilar
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__).'/../../config.php');

/**
 * Retorna un nuevo ítem para el menú de navegación de Fliplearning
 *
 * @param string $name nombre de la página para el ítem
 * @param string $url url de la página para el ítem
 *
 * @return stdClass objeto que contiene el nuevo ítem
 */
function local_notemyprogress_new_menu_item($name, $url){
    $item = new stdClass();
    $item->name = $name;
    $item->url = $url;
    return $item;
}

/**
 * Agrega una nueva página a la navegación de Moodle
 *
 * @param object $course objeto con datos del curso curso
 * @param string $url url de la página a poner en la navegación
 */
function local_notemyprogress_set_page($course, $url){
    global $PAGE;
    require_login($course, false);

    $url = new moodle_url($url);
    $url->param('courseid', $course->id);
    $PAGE->set_url($url);
    $plugin_name = get_string('pluginname', 'local_notemyprogress');
    $PAGE->set_title($plugin_name);
    $PAGE->set_pagelayout('standard');
    $PAGE->set_heading($course->fullname);
    local_notemyprogress_render_styles();
}

/**
 * Agrega las importaciones de los archivos css necesarios para Fliplearning
 */
function local_notemyprogress_render_styles(){
    global $PAGE;
    $PAGE->requires->css('/local/notemyprogress/css/googlefonts.css');
    $PAGE->requires->css('/local/notemyprogress/css/materialicon.css');
    $PAGE->requires->css('/local/notemyprogress/css/materialdesignicons.css');
    $PAGE->requires->css('/local/notemyprogress/css/vuetify.css');
    $PAGE->requires->css('/local/notemyprogress/css/alertify.css');
    $PAGE->requires->css('/local/notemyprogress/css/quill.core.css');
    $PAGE->requires->css('/local/notemyprogress/css/quill.snow.css');
    $PAGE->requires->css('/local/notemyprogress/css/quill.bubble.css');
    $PAGE->requires->css('/local/notemyprogress/styles.css');
}

/**
 * Envuelve en un contenedor la respuesta a una petición Ajax
 *
 * @param object $data los datos a devolver
 * @param string $message mensaje de la respuesta
 * @param boolean $valid campo opcional para especificar si la respuesta es correcta válida
 * @param string $code campo opcionar para especificar la respuesta http
 */
function local_notemyprogress_ajax_response($data = array(), $message=null, $ok=true, $code = 200){
    local_notemyprogress_set_api_headers();
    $response = [
        'ok' => $ok,
        'message' => $message,
        'data' => $data
    ];
    http_response_code($code);
    echo json_encode($response);
}

/**
 * Coloca cabeceras a la respuesta http de una peticion ajax
 */
function local_notemyprogress_set_api_headers(){
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json');
}

function local_notemyprogress_get_groups($course, $user){
    global $COURSE;
    $group_manager = new \local_notemyprogress\group_manager($course, $user);
    $participants = new \local_notemyprogress\course_participant($user->id, $course->id);
    $groups = array_values($participants->current_user_groups_with_all_group($COURSE->groupmode));
    $selectedgroupid = $group_manager->selected_group()->groupid;
    $groups = local_notemyprogress_add_selected_property($groups, $selectedgroupid);
    return $groups;
}

function local_notemyprogress_add_selected_property($groups, $groupid = null){
    foreach ($groups as $group) {
        if(!is_null($groupid) && $group->id == $groupid){
            $group->selected = true;
        }else{
            $group->selected = false;
        }
    }
    return $groups;
}

////////////////////////////////////
//    Gamification function      ///
////////////////////////////////////
function local_notemyprogress_generate_token($courseid, $userid, $profile){
    $key = local_notemyprogress_get_token_signature();
    $payload = array(
        "uid" => $userid,
        "cid" => $courseid,
        "pro" => $profile,
    );
    $time = local_notemyprogress_get_token_time_valid();
    if ($time) {
        $date = new DateTime();
        $date = $date->format('U');
        $payload['exp'] = intval($date) + $time;
    }
    return \local_notemyprogress\jwt\JWT::encode($payload, $key);
}
function local_notemyprogress_get_token_signature() {
    $key = get_config("local_notemyprogress", "token_signature");
    if (empty($key)) {
        $key = "m73YvSn2mhFVeXtut5UY";  // default signature
    }
    return $key;
}

function local_notemyprogress_get_token_time_valid() {
    return intval(get_config("local_notemyprogress", "token_time_valid"));
}

function local_notemyprogress_validate_token($token){
    try {
        global $DB;
        $key = local_notemyprogress_get_token_signature();

        $token = validate_param($token, PARAM_TEXT);

        $decoded = \local_notemyprogress\jwt\JWT::decode($token, $key, array('HS256'));

        $userid = validate_param($decoded->uid, PARAM_INT);
        $courseid = validate_param($decoded->cid, PARAM_INT);

        $COURSE = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $USER = $DB->get_record('user', array('id' => $userid), '*', MUST_EXIST);

        require_login($COURSE, false);
        $context = context_course::instance($courseid);
        require_capability('local/notemyprogress:ajax', $context);

        $result = new stdClass();
        $result->courseid = $COURSE->id;
        $result->userid = $USER->id;
        $result->profile = $decoded->pro;

        return $result;
    } catch (Exception $e) {
        $message = get_string('nmp_api_invalid_token_error', 'local_notemyprogress');
        local_notemyprogress_ajax_response(null, $message, false, 403);
    }
    function local_notemyprogress_save_gamification_config($courseid, $userid, $rules, $levels, $settings, $url,$enable){
            // \local_notemyprogress\logs::create(
            //     "setgamification",
            //     "configweeks",
            //     "saved",
            //     "weeks_settings",
            //     $url,
            //     4,
            //     $userid,
            //     $courseid
            // );
            $logs = new \local_notemyprogress\logs($courseid, $userid);
            $logs->addLogsNMP("Saved", "section", "CONFIGURATION_GAMIFICATION", "configuration_gamification", $url, "GamificationSaved");
        
            $configLevels = new \local_notemyprogress\configgamification($courseid, $userid);
            $configLevels->save_levels($levels, $settings, $rules);
            $configLevels->save_enable($enable);
            $message = get_string('nmp_api_save_successful', 'local_notemyprogress');
            local_notemyprogress_ajax_response(true, $message);
        }

        function local_notemyprogress_validate_action_and_profile($action, $profile){
            $message = "";
        
            // Validate action
            try {
                $action = validate_param($action, PARAM_ALPHA);
            } catch (Exception $e) {
                $message = get_string('nmp_api_invalid_transaction', 'local_notemyprogress');
            }
        
            // Validate profile
            if ($profile == "teacher") {
                if ($action != "saveconfigweek" && $action != "changegroup" && $action != "worksessions" &&
                    $action != "assignments"    && $action != "sendmail"    && $action != "quiz"         &&
                    $action != "wordcloud"      && $action != "dropoutdata" && $action != "downloadlogs" &&
                    $action != "saveinteraction" && $action != "saveconfiggamification") {
                    $message = get_string('nmp_api_invalid_transaction', 'local_notemyprogress');
                }
            } elseif ($profile == "student") {
                if ($action != "studentsessions" && $action != "studentassigns" &&
                    $action != "studenttimes" && $action != "saveinteraction") {
                    $message = get_string('nmp_api_invalid_transaction', 'local_notemyprogress');
                }
            } else {
                $message = get_string('nmp_api_invalid_profile', 'local_notemyprogress');
            }
        
            if ($message != "") {
                local_notemyprogress_ajax_response(null, $message, false, 400);
            }
        
            return $action;
        }
}

