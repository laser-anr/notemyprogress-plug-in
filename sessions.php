<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/sessions.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:teacher_sessions', $context);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "TEACHER_STUDY_SESSIONS", "study_sessions", $actualLink, "Section where you can consult various indicators on the study sessions carried out by students");


$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => [
        "section_help_title" => get_string("ts_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("ts_section_help_description", "local_notemyprogress"),
        "inverted_time_help_title" => get_string("ts_inverted_time_help_title", "local_notemyprogress"),
        "inverted_time_help_description_p1" => get_string("ts_inverted_time_help_description_p1", "local_notemyprogress"),
        "inverted_time_help_description_p2" => get_string("ts_inverted_time_help_description_p2", "local_notemyprogress"),
        "hours_sessions_help_title" => get_string("ts_hours_sessions_help_title", "local_notemyprogress"),
        "hours_sessions_help_description_p1" => get_string("ts_hours_sessions_help_description_p1", "local_notemyprogress"),
        "hours_sessions_help_description_p2" => get_string("ts_hours_sessions_help_description_p2", "local_notemyprogress"),
        "sessions_count_help_title" => get_string("ts_sessions_count_help_title", "local_notemyprogress"),
        "sessions_count_help_description_p1" => get_string("ts_sessions_count_help_description_p1", "local_notemyprogress"),
        "sessions_count_help_description_p2" => get_string("ts_sessions_count_help_description_p2", "local_notemyprogress"),

        "title" => get_string("nmp_title", "local_notemyprogress"),
        "chart" => $reports->get_chart_langs(),
        "days" => array(
            get_string("nmp_mon_short", "local_notemyprogress"),
            get_string("nmp_tue_short", "local_notemyprogress"),
            get_string("nmp_wed_short", "local_notemyprogress"),
            get_string("nmp_thu_short", "local_notemyprogress"),
            get_string("nmp_fri_short", "local_notemyprogress"),
            get_string("nmp_sat_short", "local_notemyprogress"),
            get_string("nmp_sun_short", "local_notemyprogress"),
        ),
        "hours" => array(
            get_string("nmp_00", "local_notemyprogress"),
            get_string("nmp_01", "local_notemyprogress"),
            get_string("nmp_02", "local_notemyprogress"),
            get_string("nmp_03", "local_notemyprogress"),
            get_string("nmp_04", "local_notemyprogress"),
            get_string("nmp_05", "local_notemyprogress"),
            get_string("nmp_06", "local_notemyprogress"),
            get_string("nmp_07", "local_notemyprogress"),
            get_string("nmp_08", "local_notemyprogress"),
            get_string("nmp_09", "local_notemyprogress"),
            get_string("nmp_10", "local_notemyprogress"),
            get_string("nmp_11", "local_notemyprogress"),
            get_string("nmp_12", "local_notemyprogress"),
            get_string("nmp_13", "local_notemyprogress"),
            get_string("nmp_14", "local_notemyprogress"),
            get_string("nmp_15", "local_notemyprogress"),
            get_string("nmp_16", "local_notemyprogress"),
            get_string("nmp_17", "local_notemyprogress"),
            get_string("nmp_18", "local_notemyprogress"),
            get_string("nmp_19", "local_notemyprogress"),
            get_string("nmp_20", "local_notemyprogress"),
            get_string("nmp_21", "local_notemyprogress"),
            get_string("nmp_22", "local_notemyprogress"),
            get_string("nmp_23", "local_notemyprogress"),
        ),
        "weeks" => array(
            get_string("nmp_week1", "local_notemyprogress"),
            get_string("nmp_week2", "local_notemyprogress"),
            get_string("nmp_week3", "local_notemyprogress"),
            get_string("nmp_week4", "local_notemyprogress"),
            get_string("nmp_week5", "local_notemyprogress"),
            get_string("nmp_week6", "local_notemyprogress"),
        ),
        "table_title" => get_string("table_title", "local_notemyprogress"),
        "thead_name" => get_string("thead_name", "local_notemyprogress"),
        "thead_lastname" => get_string("thead_lastname", "local_notemyprogress"),
        "thead_email" => get_string("thead_email", "local_notemyprogress"),
        "thead_progress" => get_string("thead_progress", "local_notemyprogress"),
        "thead_sessions" => get_string("thead_sessions", "local_notemyprogress"),
        "thead_time" => get_string("thead_time", "local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),

        "module_label" => get_string("nmp_module_label", "local_notemyprogress"),
        "modules_label" => get_string("nmp_modules_label", "local_notemyprogress"),
        "of_conector" => get_string("nmp_of_conector", "local_notemyprogress"),
        "finished_label" => get_string("nmp_finished_label", "local_notemyprogress"),
        "finisheds_label" => get_string("nmp_finisheds_label", "local_notemyprogress"),

        "session_count_title" => get_string("nmp_session_count_title", "local_notemyprogress"),
        "session_count_yaxis_title" => get_string("nmp_session_count_yaxis_title", "local_notemyprogress"),
        "session_count_tooltip_suffix" => get_string("nmp_session_count_tooltip_suffix", "local_notemyprogress"),

        "hours_sessions_title" => get_string("nmp_hours_sessions_title", "local_notemyprogress"),
        "weeks_sessions_title" => get_string("nmp_weeks_sessions_title", "local_notemyprogress"),

        "no_data" => get_string("no_data", "local_notemyprogress"),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name", "local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to", "local_notemyprogress"),
        "pagination_title" => get_string("pagination_title", "local_notemyprogress"),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),

        "session_text" => get_string("nmp_session_text", "local_notemyprogress"),
        "sessions_text" => get_string("nmp_sessions_text", "local_notemyprogress"),

        "time_inverted_title" => get_string("nmp_time_inverted_title", "local_notemyprogress"),
        "time_inverted_x_axis" => get_string("nmp_time_inverted_x_axis", "local_notemyprogress"),
        "inverted_time" => get_string("nmp_inverted_time", "local_notemyprogress"),
        "expected_time" => get_string("nmp_expected_time", "local_notemyprogress"),

        "hours_short" => get_string("nmp_hours_short", "local_notemyprogress"),
        "minutes_short" => get_string("nmp_minutes_short", "local_notemyprogress"),
        "seconds_short" => get_string("nmp_seconds_short", "local_notemyprogress"),
    ],
    'inverted_time_colors' => array('#118AB2', '#06D6A0'),
    'sessions_count_colors' => array('#FFD166', '#06D6A0', '#118AB2'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'indicators' => $reports->get_sessions(),
    'session_count' => $reports->count_sessions(),
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/sessions', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/sessions', ['content' => $content]);
echo $OUTPUT->footer();
