<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER , $DB;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/gamification.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:teacher_gamification', $context);
require_capability('local/notemyprogress:setweeks', $context);

$maxTimeSql = "SELECT MAX(timecreated) as maximum from {notemyprogress_gamification} where courseid=? ";
$timecrated = $DB->get_record_sql($maxTimeSql, array("courseid="=>($courseid)));

$sql = "SELECT enablegamification from {notemyprogress_gamification} where courseid=? AND timecreated=? ";
$value = $DB->get_record_sql($sql, array("courseid="=>($courseid),"timecreated="=>$timecrated->maximum));
if($value->enablegamification==1){
    $swValue = true;
}else{
    $swValue = false;
}

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "CONFIGURATION_GAMIFICATION", "configuration_gamification", $actualLink, "Section where teacher can configure gamification"); 

$configgamification = new \local_notemyprogress\configgamification($COURSE->id, $USER->id);
$es = new \local_notemyprogress\event_strategy($COURSE, $USER);
$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);



$content = [
    'strings' =>[
        'title' => get_string('tg_section_title', 'local_notemyprogress'),
        'help_title' => get_string('tg_section_help_title', 'local_notemyprogress'),
        'help_description' => get_string('tg_section_help_description', 'local_notemyprogress'),
        'helplabel' => get_string("helplabel","local_notemyprogress"),
        'error_network' => get_string('nmp_api_error_network', 'local_notemyprogress'),
        'save_successful' => get_string('nmp_api_save_successful', 'local_notemyprogress'),
        'cancel_action' => get_string('nmp_api_cancel_action', 'local_notemyprogress'),
        'save_warning_title' => get_string('tg_save_warning_title', 'local_notemyprogress'),
        'save_warning_content' => get_string('tg_save_warning_content', 'local_notemyprogress'),
        'confirm_ok' => get_string('tg_confirm_ok', 'local_notemyprogress'),
        'confirm_cancel' => get_string('tg_confirm_cancel', 'local_notemyprogress'),
        'exitbutton' => get_string("exitbutton","local_notemyprogress"),
        'save' => get_string("tg_save","local_notemyprogress"),
        'preview' => get_string("tg_section_preview","local_notemyprogress"),
        'information' => get_string("tg_section_information","local_notemyprogress"),
        'ranking' => get_string("tg_section_ranking","local_notemyprogress"),
        'settings' => get_string("tg_section_settings","local_notemyprogress"),
        'points' => get_string("tg_section_points","local_notemyprogress"),
        'no_description' => get_string("tg_section_no_description","local_notemyprogress"),
        'no_name' => get_string("tg_section_no_name","local_notemyprogress"),
        'description' => get_string("tg_section_description","local_notemyprogress"),
        'to_next_level' => get_string("tg_section_preview_next_level","local_notemyprogress"),
        'ranking_text' => get_string("tg_section_ranking_ranking_text","local_notemyprogress"),
        'level' => get_string("tg_section_ranking_level","local_notemyprogress"),
        'student' => get_string("tg_section_ranking_student","local_notemyprogress"),
        'total' => get_string("tg_section_ranking_total","local_notemyprogress"),
        'progress' => get_string("tg_section_ranking_progress","local_notemyprogress"),
        'appearance' => get_string("tg_section_settings_appearance","local_notemyprogress"),
        'appearance_title' => get_string("tg_section_settings_appearance_title","local_notemyprogress"),
        'settings_level' => get_string("tg_section_settings_levels","local_notemyprogress"),
        'quantity' => get_string("tg_section_settings_levels_quantity","local_notemyprogress"),
        'base_points' => get_string("tg_section_settings_levels_base_points","local_notemyprogress"),
        'calculated' => get_string("tg_section_settings_levels_calculated","local_notemyprogress"),
        'manually' => get_string("tg_section_settings_levels_manually","local_notemyprogress"),
        'name' => get_string("tg_section_settings_levels_name","local_notemyprogress"),
        'levels_required' => get_string("tg_section_settings_levels_required","local_notemyprogress"),
        'rules' => get_string("tg_section_settings_rules","local_notemyprogress"),
        'add_rule' => get_string("tg_section_settings_add_rule","local_notemyprogress"),
        'earn' => get_string("tg_section_settings_earn","local_notemyprogress"),
        'select_event' => get_string("tg_section_settings_select_event","local_notemyprogress"),
        'enable'=>get_string("EnableGame","local_notemyprogress"),
        'swValue'=>$swValue,
        'chart' => $reports->get_chart_langs(),
        'pointError'=>get_string("game_point_error","local_notemyprogress"),
        'eventError'=>get_string("game_event_error","local_notemyprogress"),
        'nameError'=>get_string("game_name_error","local_notemyprogress"),
        'chartTitle'=>get_string("gm_Chart_Title","local_notemyprogress"),
        'chartYaxis'=>get_string("gm_Chart_Y","local_notemyprogress"),
    ],
    'token' => local_notemyprogress_generate_token($COURSE->id, $USER->id, "teacher"),
    'userid' => $USER->id,
    'courseid' => $courseid,
    'levels_data' => $configgamification->get_levels_data(),
    'ranking' => $es->get_ranking(0),
    'events' => $configgamification->events_list(),
    'image' => $OUTPUT->image_url('badge', 'local_notemyprogress'),
    'indicators' => $reports->get_general_indicators(),
    'profile_render' => $reports->render_has(),
    'timezone' => $reports->timezone,
    'chart_data' =>$configgamification->get_spread_chart($COURSE->id),
];

$templatecontext = [
    'image' => $OUTPUT->image_url('badge', 'local_notemyprogress')
];

$PAGE->requires->js_call_amd('local_notemyprogress/gamification','init', ['content' => $content]); 
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/gamification', $templatecontext);
echo $OUTPUT->footer();