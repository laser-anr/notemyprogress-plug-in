<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/teacher.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:teacher_general', $context);


$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "TEACHER_GENERAL_INDICATORS", "teacher_general_indicators", $actualLink, "Section where you can consult various general indicators about the class of the whole course");

$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => [
        "section_help_title" => get_string("tg_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("tg_section_help_description", "local_notemyprogress"),
        "week_resources_help_title" => get_string("tg_week_resources_help_title", "local_notemyprogress"),
        "week_resources_help_description_p1" => get_string("tg_week_resources_help_description_p1", "local_notemyprogress"),
        "week_resources_help_description_p2" => get_string("tg_week_resources_help_description_p2", "local_notemyprogress"),
        "weeks_sessions_help_title" => get_string("tg_weeks_sessions_help_title", "local_notemyprogress"),
        "week_sessions_help_description_p1" => get_string("tg_week_sessions_help_description_p1", "local_notemyprogress"),
        "week_sessions_help_description_p2" => get_string("tg_week_sessions_help_description_p2", "local_notemyprogress"),
        "progress_table_help_title" => get_string("tg_progress_table_help_title", "local_notemyprogress"),
        "progress_table_help_description" => get_string("tg_progress_table_help_description", "local_notemyprogress"),

        "title" => get_string("nmp_teacher_indicators_title", "local_notemyprogress"),
        "chart" => $reports->get_chart_langs(),
        "helplabel" => get_string("helplabel","local_notemyprogress"),
        "exitbutton" => get_string("exitbutton","local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),
        "about_table" => get_string("nmp_about_table", "local_notemyprogress"),

        "table_title" => get_string("table_title", "local_notemyprogress"),
        "thead_name" => get_string("thead_name", "local_notemyprogress"),
        "thead_lastname" => get_string("thead_lastname", "local_notemyprogress"),
        "thead_email" => get_string("thead_email", "local_notemyprogress"),
        "thead_progress" => get_string("thead_progress", "local_notemyprogress"),
        "thead_sessions" => get_string("thead_sessions", "local_notemyprogress"),
        "thead_time" => get_string("thead_time", "local_notemyprogress"),
        "of_conector" => get_string("nmp_of_conector", "local_notemyprogress"),

        "teacher_indicators_students" => get_string("nmp_teacher_indicators_students", "local_notemyprogress"),
        "teacher_indicators_weeks" => get_string("nmp_teacher_indicators_weeks", "local_notemyprogress"),
        "teacher_indicators_modules" => get_string("nmp_modules_label", "local_notemyprogress"),
        "teacher_indicators_grademax" => get_string("nmp_teacher_indicators_grademax", "local_notemyprogress"),
        "teacher_indicators_course_start" => get_string("nmp_teacher_indicators_course_start", "local_notemyprogress"),
        "teacher_indicators_course_end" => get_string("nmp_teacher_indicators_course_end", "local_notemyprogress"),
        "teacher_indicators_course_format" => get_string("nmp_teacher_indicators_course_format", "local_notemyprogress"),
        "teacher_indicators_course_completion" => get_string("nmp_teacher_indicators_course_completion", "local_notemyprogress"),
        "teacher_indicators_finalized" => get_string("nmp_finished_label", "local_notemyprogress"),
        "teacher_indicators_finished" => get_string("nmp_finisheds_label", "local_notemyprogress"),
        "teacher_indicators_session" => get_string("nmp_session_text","local_notemyprogress"),
        "teacher_indicators_sessions" => get_string("nmp_sessions_text","local_notemyprogress"),
        "teacher_indicators_student_progress" => get_string("nmp_teacher_indicators_student_progress", "local_notemyprogress"),

        "teacher_indicators_week_resources_chart_title" => get_string("nmp_teacher_indicators_week_resources_chart_title", "local_notemyprogress"),
        "teacher_indicators_week_resources_yaxis_title" => get_string("nmp_teacher_indicators_week_resources_yaxis_title", "local_notemyprogress"),

        "weeks_sessions_title" => get_string("nmp_weeks_sessions_title", "local_notemyprogress"),
        "weeks" => array(
            get_string("nmp_week1", "local_notemyprogress"),
            get_string("nmp_week2", "local_notemyprogress"),
            get_string("nmp_week3", "local_notemyprogress"),
            get_string("nmp_week4", "local_notemyprogress"),
            get_string("nmp_week5", "local_notemyprogress"),
            get_string("nmp_week6", "local_notemyprogress"),
        ),
    ],
    'week_resources_colors' => array('#118AB2'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'indicators' => $reports->get_general_indicators(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/teacher', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/teacher', ['content' => $content]);
echo $OUTPUT->footer();