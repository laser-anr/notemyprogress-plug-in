<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/time.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:quiz', $context);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "ASSESSMENTS_MONITORING", "assessment_monitoring", $actualLink, "Section where you can consult the results of the different quizzes set up in the course");

$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if(!$configweeks->is_set()){
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' =>[
        "section_help_title" => get_string("tq_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("tq_section_help_description", "local_notemyprogress"),
        "questions_attempts_help_title" => get_string("tq_questions_attempts_help_title", "local_notemyprogress"),
        "questions_attempts_help_description_p1" => get_string("tq_questions_attempts_help_description_p1", "local_notemyprogress"),
        "questions_attempts_help_description_p2" => get_string("tq_questions_attempts_help_description_p2", "local_notemyprogress"),
        "questions_attempts_help_description_p3" => get_string("tq_questions_attempts_help_description_p3", "local_notemyprogress"),
        "hardest_questions_help_title" => get_string("tq_hardest_questions_help_title", "local_notemyprogress"),
        "hardest_questions_help_description_p1" => get_string("tq_hardest_questions_help_description_p1", "local_notemyprogress"),
        "hardest_questions_help_description_p2" => get_string("tq_hardest_questions_help_description_p2", "local_notemyprogress"),
        "hardest_questions_help_description_p3" => get_string("tq_hardest_questions_help_description_p3", "local_notemyprogress"),

        "chart" => $reports->get_chart_langs(),
        "title" => get_string("menu_quiz","local_notemyprogress"),
        "no_data" => get_string("no_data", "local_notemyprogress"),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name","local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to","local_notemyprogress"),
        "pagination_title" => get_string("pagination_title","local_notemyprogress"),
        "helplabel" => get_string("helplabel","local_notemyprogress"),
        "exitbutton" => get_string("exitbutton","local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),

        "quiz_info_text" => get_string("nmp_quiz_info_text", "local_notemyprogress"),
        "question_text" => get_string("nmp_question_text", "local_notemyprogress"),
        "questions_text" => get_string("nmp_questions_text", "local_notemyprogress"),
        "doing_text_singular" => get_string("nmp_doing_text_singular", "local_notemyprogress"),
        "doing_text_plural" => get_string("nmp_doing_text_plural", "local_notemyprogress"),
        "attempt_text" => get_string("nmp_attempt_text", "local_notemyprogress"),
        "attempts_text" => get_string("nmp_attempts_text", "local_notemyprogress"),
        "student_text" => get_string("nmp_student_text", "local_notemyprogress"),
        "students_text" => get_string("nmp_students_text", "local_notemyprogress"),
        "of_conector" => get_string("nmp_of_conector", "local_notemyprogress"),
        "quiz_label" => get_string("nmp_quiz", "local_notemyprogress"),

        "questions_attempts_chart_title" => get_string("nmp_questions_attempts_chart_title", "local_notemyprogress"),
        "questions_attempts_yaxis_title" => get_string("nmp_questions_attempts_yaxis_title", "local_notemyprogress"),
        "hardest_questions_chart_title" => get_string("nmp_hardest_questions_chart_title", "local_notemyprogress"),
        "hardest_questions_yaxis_title" => get_string("nmp_hardest_questions_yaxis_title", "local_notemyprogress"),

        "correct_attempt" => get_string("nmp_correct_attempt", "local_notemyprogress"),
        "partcorrect_attempt" => get_string("nmp_partcorrect_attempt", "local_notemyprogress"),
        "incorrect_attempt" => get_string("nmp_incorrect_attempt", "local_notemyprogress"),
        "blank_attempt" => get_string("nmp_blank_attempt", "local_notemyprogress"),
        "needgraded_attempt" => get_string("nmp_needgraded_attempt", "local_notemyprogress"),
        "review_question" => get_string("nmp_review_question", "local_notemyprogress"),

    ],
    'questions_attempts_colors' => array('#06D6A0', '#FFD166', '#EF476F', '#118AB2', '#264653'),
    'hardest_questions_colors' => array('#EF476F'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'quiz' => $reports->quiz_attempts(),
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/quiz','init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/quiz', ['content' => $content]);
echo $OUTPUT->footer();