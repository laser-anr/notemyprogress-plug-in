<?php
global $COURSE, $USER;
require_once('locallib.php');

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

$context = context_course::instance($course->id);
require_capability('local/student_reports:usepluggin', $context);

$url = '/local/student_reports/notes.php';
local_sr_set_page($course, $url);

$notes = new local_student_reports_notes($COURSE, $USER->id);

$content = [
    'strings' => array(
        'title_notes' => get_string('notes_title_notes', 'local_notemyprogress'),
        'notes_description' => get_string('notes_description', 'local_notemyprogress'),
        'btn_notes' => get_string('notes_btn_notes', 'local_notemyprogress'),
        'btn_note_new' => get_string('notes_btn_new',  'local_notemyprogress'),
        'btn_note_update' => get_string('notes_btn_update',  'local_notemyprogress'),
        'btn_note_delete' => get_string('notes_btn_delete',  'local_notemyprogress'),
        'string_note_new' => get_string('notes_string_new',  'local_notemyprogress'),
        'string_note_update' => get_string('notes_string_update',  'local_notemyprogress'),
        'title' => get_string('notes_placeholder_title',  'local_notemyprogress'),
        'search' => get_string('notes_placeholder_search',  'local_notemyprogress'),
        'writenote' => get_string('notes_placeholder_note',  'local_notemyprogress'),
        'savenote' => get_string('notes_save_success',  'local_notemyprogress'),
        'saveerror' => get_string('api_error_network',  'local_notemyprogress'),
        'created' => get_string('notes_created',  'local_notemyprogress'),
        'notes_not_found' => get_string('notes_not_found',  'local_notemyprogress'),
        "helplabel" => get_string("helplabel", 'local_notemyprogress'),
        "exitbutton" => get_string("exitbutton", 'local_notemyprogress'),
        "note_created" => get_string("notes_message_created", 'local_notemyprogress'),
        "note_not_created" => get_string("notes_message_not_created", 'local_notemyprogress'),
        "note_deleted" => get_string("notes_message_deleted", 'local_notemyprogress'),
        "note_not_deleted" => get_string("notes_message_not_deleted", 'local_notemyprogress'),
        "note_updated" => get_string("notes_message_updated", 'local_notemyprogress'),
        "note_not_updated" => get_string("notes_message_not_updated", 'local_notemyprogress'),
    ),
    'notes' => $notes->get_notes(),
    'courseid' => $courseid,
    'userid' => $USER->id,
];
$PAGE->requires->js_call_amd('local_student_reports/notes', 'init', ['content' => $content]);

echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_student_reports/notes', ['content' => $content]);
echo $OUTPUT->footer();
