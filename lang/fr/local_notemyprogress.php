<?php
// Ce fichier fait partie de Moodle - http://moodle.org/
//
// Moodle est un logiciel libre: vous pouvez le redistribuer et / ou le modifier
// selon les termes de la licence publique générale GNU comme publié par
// la Free Software Foundation, soit la version 3 de la licence, soit
// (à votre choix) toute version ultérieure.
//
// Moodle est distribué dans l\'espoir qu'il sera utile,
// mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
// QUALITÉ MARCHANDE ou d\'aDÉQUATION À UN USAGE PARTICULIER. Voir la
// Licence publique générale GNU pour plus de détails.
//
// Vous devriez avoir reçu une copie de la licence publique générale GNU
// avec Moodle. Sinon, consultez <http://www.gnu.org/licenses/>.

/**
 * Les chaînes de plugins sont définies ici.
 *
 * @package local_notemyprogress
 * @category string
 * @copyright 2020 Edisson Sigma <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license http://www.gnu.org/copyleft /gpl.html GNU GPL v3 ou version ultérieure
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'notemyprogress';

/* Global */
$string['pagination'] = 'Semaine:';
$string['graph_generating'] = 'Nous sommes en train de construire le rapport, veuillez patienter un moment.';
$string['weeks_not_config'] = 'Le cours n\'a pas été configuré par l\'enseignant, il n\'y a donc pas de visualisations à afficher. ';
$string['pagination_title'] = 'Sélection de la semaine';
$string['helplabel'] = 'Aide';
$string['exitbutton'] = 'OK!';
$string['no_data'] = 'Il n\'y a aucune donnée à afficher';
$string['only_student'] = 'Ce rapport est réservé aux étudiants';
$string["nmp_send_mail"] = "(Cliquez pour envoyer un e-mail)";
$string["nmp_about"] = "À propos de ce graphique";
$string["nmp_about_table"] = "À propos de cette table";
$string["nmp_not_configured"] = "Non configuré";
$string["nmp_activated"] = "Activé";
$string["nmp_disabled"] = "Désactivé";

/* Menu */
$string['menu_main_title'] = "Progression du tableau de bord";
$string['menu_sessions'] = 'Sessions d\'étude';
$string['menu_setweek'] = "Définir les semaines";
$string['menu_time'] = 'Suivi du temps';
$string['menu_assignments'] = 'Suivi des dépôts';
$string['menu_grades'] = 'Suivi des notes';
$string['menu_quiz'] = 'Suivi des évaluations';
$string['menu_dropout'] = 'Décrochage';
$string['menu_logs'] = "Journaux d'activités";
$string['menu_general'] = "Indicateurs généraux";
$string['menu_auto_evaluation'] = "Auto-évaluation";

/* Nav Bar Menu */
$string['togglemenu'] = 'Afficher / Masquer le menu nmp';

/* Composant de pagination */
$string['pagination_component_to'] = 'al';
$string['pagination_component_name'] = 'Semaine';

/* Goups */
$string['group_allstudent'] = 'Tous les étudiants';

/* Erreurs générales */
$string['api_error_network'] = "Une erreur s'est produite lors de la communication avec le serveur.";
$string['api_invalid_data'] = 'Données incorrectes';
$string['api_save_successful'] = 'Les données ont été correctement enregistrées sur le serveur';
$string['api_cancel_action'] = 'Vous avez annulé l\'action ';
$string['api_error_conditions_setweek'] = 'Echec de l\'enregistrement des modifications. Conditions non remplies.';
/* Admin Task Screen */
$string['generate_data_task'] = 'Processus de génération de données pour le plugin note my progress';

/* Graphique */
$string['chart_loading'] = 'Chargement ...';
$string['chart_exportButtonTitle'] = "Exporter";
$string['chart_printButtonTitle'] = "Imprimer";
$string['chart_rangeSelectorFrom'] = "De";
$string['chart_rangeSelectorTo'] = "Jusqu'à";
$string['chart_rangeSelectorZoom'] = "Plage";
$string['chart_downloadPNG'] = 'Télécharger une image PNG';
$string['chart_downloadJPEG'] = 'Télécharger une image JPEG';
$string['chart_downloadPDF'] = 'Télécharger le document PDF';
$string['chart_downloadSVG'] = 'Télécharger l\'image SVG';
$string['chart_downloadCSV'] = 'Télécharger CSV';
$string['chart_downloadXLS'] = 'Télécharger XLS';
$string['chart_exitFullscreen'] = 'Quitter le plein écran';
$string['chart_hideData'] = 'Masquer la table de données';
$string['chart_noData'] = 'Il n\' y a aucune donnée à afficher ';
$string['chart_printChart'] = 'Imprimer le graphique';
$string['chart_viewData'] = 'Afficher la table de données';
$string['chart_viewFullscreen'] = 'Afficher en plein écran';
$string['chart_resetZoom'] = 'Redémarrer le zoom';
$string['chart_resetZoomTitle'] = 'Réinitialiser le niveau de zoom 1: 1';

/* Définir les semaines */
$string['setweeks_title'] = 'Définition des semaines de cours';
$string['setweeks_description'] = 'Pour commencer, vous devez configurer le cours par semaines et définir une date de début pour la première semaine (le reste des semaines se déroulera automatiquement à partir de cette date. Ensuite, vous devez associer les activités ou des modules liés à chaque semaine en les faisant glisser de la colonne de droite vers la semaine correspondante. Il n\'est pas nécessaire d\'affecter toutes les activités ou modules aux semaines, mais uniquement celles que vous souhaitez envisager pour suivre les étudiants. Enfin, vous devez cliquez sur le bouton Enregistrer pour conserver vos paramètres. ';
$string['setweeks_sections'] = "Sections disponibles dans le cours";
$string['setweeks_weeks_of_course'] = "Planification des semaines";
$string['setweeks_add_new_week'] = "Ajouter une semaine";
$string['setweeks_start'] = "Commence le:";
$string['setweeks_end'] = "Se termine le:";
$string['setweeks_week'] = "Semaine";
$string['setweeks_save'] = "Enregistrer la configuration";
$string['setweeks_time_dedication'] = 'Combien d\'heures de travail attendez-vous que les étudiants consacrent à votre cours cette semaine?';
$string['setweeks_enable_scroll'] = "Activer le mode de défilement pour les semaines et les thèmes";
$string['setweeks_label_section_removed'] = "Retiré du cours";
$string['setweeks_error_section_removed'] = "Une section affectée à une semaine a été supprimée du cours, vous devez la supprimer de votre planification pour continuer.";
$string['setweeks_save_warning_title'] = "Êtes-vous sûr de vouloir enregistrer les modifications?";
$string['setweeks_save_warning_content'] = "Si vous modifiez la configuration des semaines où le cours a déjà commencé, des données peuvent être perdues...";
$string['setweeks_confirm_ok'] = "Enregistrer";
$string['setweeks_confirm_cancel'] = "Annuler";
$string['setweeks_error_empty_week'] = "Vous ne pouvez pas enregistrer les modifications avec une semaine vide. Veuillez la supprimer et réessayer.";
$string['setweeks_new_group_title'] = "Nouvelle instance de configuration";
$string['setweeks_new_group_text'] = "Nous avons détecté que votre cours est terminé, si vous souhaitez configurer les semaines pour travailler avec de nouveaux étudiants, vous devez activer le bouton ci-dessous. Cela séparera les données des étudiants actuels de celles des précédents cours, en évitant de les mélanger. ";
$string['setweeks_new_group_button_label'] = "Enregistrer la configuration en tant que nouvelle instance";
$string['course_format_weeks'] = 'Semaine';
$string['course_format_topics'] = 'Sujet';
$string['course_format_social'] = 'Social';
$string['course_format_singleactivity'] = 'Activité unique';
$string['plugin_requirements_title'] = 'Statut:';
$string['plugin_requirements_descriptions'] = 'Le plugin sera visible et affichera les rapports pour les étudiants et les enseignants lorsque les conditions suivantes sont remplies...';
$string['plugin_requirements_has_users'] = 'Le cours doit avoir au moins un étudiant inscrit';
$string['plugin_requirements_course_start'] = 'La date actuelle doit être postérieure à la date de début de la première semaine configurée.';
$string['plugin_requirements_has_sections'] = 'Les semaines configurées doivent contenir au moins une section.';
$string['plugin_visible'] = 'Rapports visibles.';
$string['plugin_hidden'] = 'Rapports masqués.';
$string['title_conditions'] = 'Conditions d\' utilisation ';

/* Heure */
$string['nmp_mon'] = 'Lundi';
$string['nmp_tue'] = 'Mardi';
$string['nmp_wed'] = 'Mercredi';
$string['nmp_thu'] = 'Jeudi';
$string['nmp_fri'] = 'Vendredi';
$string['nmp_sat'] = 'Samedi';
$string['nmp_sun'] = 'Dimanche';
$string['nmp_mon_short'] = 'Lun';
$string['nmp_tue_short'] = 'Mar';
$string['nmp_wed_short'] = 'Mer';
$string['nmp_thu_short'] = 'Jeu';
$string['nmp_fri_short'] = 'Ven';
$string['nmp_sat_short'] = 'Sam';
$string['nmp_sun_short'] = 'Dim';

$string['nmp_jan'] = 'Janvier';
$string['nmp_feb'] = 'Février';
$string['nmp_mar'] = 'Mars';
$string['nmp_apr'] = 'Avril';
$string['nmp_may'] = 'Mai';
$string['nmp_jun'] = 'Juin';
$string['nmp_jul'] = 'Juillet';
$string['nmp_aug'] = 'Août';
$string['nmp_sep'] = 'Septembre';
$string['nmp_oct'] = 'Octobre';
$string['nmp_nov'] = 'Novembre';
$string['nmp_dec'] = 'Décembre';
$string['nmp_jan_short'] = 'Jan';
$string['nmp_feb_short'] = 'Fév';
$string['nmp_mar_short'] = 'Mar';
$string['nmp_apr_short'] = 'Apr';
$string['nmp_may_short'] = 'Mai';
$string['nmp_jun_short'] = 'Juin';
$string['nmp_jul_short'] = 'Juil';
$string['nmp_aug_short'] = 'Août';
$string['nmp_sep_short'] = 'Sep';
$string['nmp_oct_short'] = 'Oct';
$string['nmp_nov_short'] = 'Nov';
$string['nmp_dec_short'] = 'Déc';

$string['nmp_week1'] = 'Sem 1';
$string['nmp_week2'] = 'Sem 2';
$string['nmp_week3'] = 'Sem 3';
$string['nmp_week4'] = 'Sem 4';
$string['nmp_week5'] = 'Sem 5';
$string['nmp_week6'] = 'Sem 6';

$string['nmp_00'] = '00h';
$string['nmp_01'] = '01h';
$string['nmp_02'] = '02h';
$string['nmp_03'] = '03h';
$string['nmp_04'] = '04h';
$string['nmp_05'] = '05h';
$string['nmp_06'] = '06h';
$string['nmp_07'] = '07h';
$string['nmp_08'] = '08h';
$string['nmp_09'] = '09h';
$string['nmp_10'] = '10h';
$string['nmp_11'] = '11h';
$string['nmp_12'] = '12h';
$string['nmp_13'] = '13h';
$string['nmp_14'] = '14h';
$string['nmp_15'] = '15h';
$string['nmp_16'] = '16h';
$string['nmp_17'] = '17h';
$string['nmp_18'] = '18h';
$string['nmp_19'] = '19h';
$string['nmp_20'] = '20h';
$string['nmp_21'] = '21h';
$string['nmp_22'] = '22h';
$string['nmp_23'] = '23h';

/* Enseignant général */
$string['tg_section_help_title'] = 'Indicateurs généraux';
$string['tg_section_help_description'] = 'Cette section contient des visualisations avec des indicateurs généraux liés à la configuration du cours, aux ressources assignées par semaine, aux sessions d\'étude et à la progression des étudiants dans le cours. Les visualisations de cette section montrent les indicateurs de la date de début à la date de fin du cours (ou à la date actuelle si le cours n\'est pas encore terminé).';
$string['tg_week_resources_help_title'] = 'Ressources par semaines';
$string['tg_week_resources_help_description_p1'] = 'Ce graphique affiche la quantité de ressources pour chacune des sections de cours affectées à chaque semaine d\'étude configurée dans la section <i> Configurer les semaines </i>. Si deux sections de cours ou plus sont attribuées à une semaine, les ressources de ces sections sont additionnées pour calculer le total des ressources pour une semaine. ';
$string['tg_week_resources_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent les ressources et activités totales des sections affectées à chaque semaine configurée de note my progress. Sur l’axe des y figurent les semaines d’étude configurées. ';
$string['tg_weeks_sessions_help_title'] = 'Sessions par semaine';
$string['tg_week_sessions_help_description_p1'] = 'Ce graphique montre le nombre de sessions d\' étude complétées par les étudiants chaque semaine à partir de la date de début du cours. L\'accès au cours par l\'étudiant est considéré comme le début d\'une session d\'étude. Une session est considérée comme terminée lorsque le temps entre deux interactions d\'un élève dépasse 30 minutes. ';
$string['tg_week_sessions_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent les semaines de chaque mois. l\'axe des y du graphique montre les différents mois de l\'année à partir du mois de création du cours. Pour maintenir la symétrie du graphique, un total de cinq semaines a été placé pour chaque mois, cependant, chaque mois ne compte pas autant de semaines. Ces mois n’ajouteront que des sessions jusqu’à la quatrième semaine.';
$string['tg_progress_table_help_title'] = 'Progression des étudiants';
$string['tg_progress_table_help_description'] = 'Ce tableau montre une liste de tous les étudiants inscrits au cours avec leur progression, le nombre de sessions et le temps passé. Pour calculer la progression, toutes les ressources du cours ont été prises en compte, à l\'exception de celles de type <i> Label </i>. Pour déterminer si un étudiant a terminé une ressource, il est d\'abord vérifié pour voir si le paramètre d\'exhaustivité de la ressource est activé. Si tel est le cas, il est recherché si l\'élève a déjà terminé l\'activité basée sur cette configuration. Sinon, l’activité est considérée comme terminée si l’élève l’a vue au moins une fois. ';

$string['nmp_title'] = 'Sessions d\'étude';
$string['table_title'] = 'Progression du cours';
$string['thead_name'] = 'Prénom';
$string['thead_lastname'] = 'Nom';
$string['thead_email'] = 'Mail';
$string['thead_progress'] = 'Progression (%)';
$string['thead_sessions'] = 'Sessions';
$string['thead_time'] = 'Temps investi';

$string['nmp_module_label'] = 'ressource';
$string['nmp_modules_label'] = 'Ressources';
$string['nmp_of_conector'] = 'de';
$string['nmp_finished_label'] = 'terminé';
$string['nmp_finisheds_label'] = 'terminé';

$string['nmp_smaller30'] = 'Moins de 30 minutes';
$string['nmp_greater30'] = 'Plus de 30 minutes';
$string['nmp_greater60'] = 'Plus de 60 minutes';

$string['nmp_session_count_title'] = 'Sessions de la semaine';
$string['nmp_session_count_yaxis_title'] = 'Nombre de sessions';
$string['nmp_session_count_tooltip_suffix'] = 'sessions';

$string['nmp_hours_sessions_title'] = 'Sessions par jour et heure';
$string['nmp_weeks_sessions_title'] = 'Sessions par semaine';

$string["nmp_session_text"] = "session";
$string["nmp_sessions_text"] = "sessions";

$string['ss_change_timezone'] = 'Fuseau horaire:';
// $string['ss_activity_inside_plataform_student'] = 'Mon activité sur la plateforme';
// $string['ss_activity_inside_plataform_teacher'] = 'Activité des étudiants sur la plateforme';
// $string['ss_time_inside_plataform_student'] = 'Mon temps sur la plateforme';
// $string['ss_time_inside_plataform_teacher'] = 'Temps moyen passé par les étudiants sur la plateforme cette semaine';
// $string['ss_time_inside_plataform_description_teacher'] = 'Temps que l’élève a investi dans la semaine sélectionnée, par rapport au temps que l’enseignant a prévu de l’investir. Le temps passé affiché correspond à la moyenne de tous les élèves. Le temps prévu par l’enseignant est le temps attribué par l’enseignant dans <i> Configurer les semaines </i>. ';
// $string['ss_time_inside_plataform_description_student'] = 'Temps passé cette semaine par rapport au temps que l’enseignant a prévu de passer.';
// $string['ss_activity_inside_plataform_description_teacher'] = 'Les heures de la journée sont indiquées sur l\'axe Y et les jours de la semaine sur l\'axe X. Dans le graphique, vous pouvez trouver plusieurs points qui, en les survolant, offrent des informations détaillées sur les interactions des étudiants, regroupées par type de ressource (nombre d\'interactions, nombre d\'étudiants qui ont interagi avec la ressource et moyenne des interactions). <br/> <br/> <b> En cliquant sur les balises, vous pourrez filtrer par type de ressource, ne laissant visibles que celles qui ne sont pas barrées. </b> ';
// $string['ss_activity_inside_plataform_description_student'] = 'Afficher les interactions par type de ressource et planification. Lorsque vous survolez un point visible du graphique, vous verrez le nombre d\'interactions regroupées par type de ressource. En cliquant sur les balises, vous pourrez filtrer par type de ressource. ';

/* Sessions de l\'enseignant */
$string['ts_section_help_title'] = 'Sessions d\'étude';
$string['ts_section_help_description'] = 'Cette section contient des visualisations liées à l’activité des étudiants dans le cours mesurée en termes de sessions effectuées, de temps moyen passé dans le cours par semaine et de sessions d’étude à intervalles de temps. Les données présentées dans cette section varient en fonction de la semaine d\'étude choisie. ';
$string['ts_inverted_time_help_title'] = 'Temps investi par les étudiants sur le cours';
$string['ts_inverted_time_help_description_p1'] = 'Ce graphique montre le temps moyen passé par les étudiants dans la semaine par rapport au temps moyen prévu par le professeur.';
$string['ts_inverted_time_help_description_p2'] = 'Sur l\'axe des x du graphique se trouve le nombre d\'heures que l\'enseignant a prévu pour une semaine spécifique. Sur l’axe des y figurent les étiquettes du temps moyen passé et du temps moyen à passer. ';
$string['ts_hours_sessions_help_title'] = 'Sessions par jour et heure';
$string['ts_hours_sessions_help_description_p1'] = 'Ce graphique montre les sessions d\'étude par jour et heure pour la semaine sélectionnée. L\'accès au cours par l\'étudiant est considéré comme le début d\'une session d\'étude. Une session est considérée comme terminée lorsque le temps entre deux interactions d\'un élève dépasse 30 minutes. ';
$string['ts_hours_sessions_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les jours de la semaine. Sur l’axe des y figurent les heures de la journée allant de 00h00 à 23h00';
$string['ts_sessions_count_help_title'] = 'Sessions de la semaine';
$string['ts_sessions_count_help_description_p1'] = 'Ce graphique montre le nombre de sessions classées par durée dans des plages horaires: moins de 30 minutes, plus de 30 minutes et plus de 60 minutes. l\'accès au cours par l\'étudiant est considéré comme le début d\'une session d\'étude. Une session est considérée comme terminée lorsque le temps entre deux interactions d\'un élève dépasse 30 minutes. ';
$string['ts_sessions_count_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les jours de la semaine configurés. Sur l’axe des y figure le nombre de sessions effectuées. ';

$string['nmp_time_inverted_title'] = 'Temps investi par les étudiants sur le cours';
$string['nmp_time_inverted_x_axis'] = 'Nombre d\'heures';
$string['nmp_inverted_time'] = 'Temps moyen investi';
$string['nmp_expected_time'] = 'Temps moyen à investir';

$string['nmp_year'] = 'année';
$string['nmp_years'] = 'années';
$string['nmp_month'] = 'mois';
$string['nmp_months'] = 'mois';
$string['nmp_day'] = 'jour';
$string['nmp_days'] = 'jours';
$string['nmp_hour'] = 'heure';
$string['nmp_hours'] = 'heures';
$string['nmp_hours_short'] = 'h';
$string['nmp_minute'] = 'minute';
$string['nmp_minutes'] = 'minutes';
$string['nmp_minutes_short'] = 'm';
$string['nmp_second'] = 'seconde';
$string['nmp_seconds'] = 'secondes';
$string['nmp_seconds_short'] = 's';
$string['nmp_ago'] = 'avant';
$string['nmp_now'] = 'à l\'instant';

/*Devoirs des enseignants */

$string['ta_section_help_title'] = 'Suivi des tâches';
$string['ta_section_help_description'] = 'Cette section contient des indicateurs liés à la livraison des devoirs dans les zones de dépôts et à l\' accès aux ressources. Les données présentées dans cette section varient en fonction de la semaine d\'étude choisie. ';
$string['ta_assigns_submissions_help_title'] = 'Suivi des devoirs soumis dans les zones de dépôts';
$string['ta_assigns_submissions_help_description_p1'] = 'Ce graphique montre la distribution du nombre d\'étudiants, par rapport à l\'état de livraison d\'un devoir dans les zones de dépôts.';
$string['ta_assigns_submissions_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent le nom des dépôts des sections de la semaine sélectionnée avec la date et l\'heure de livraison attendue. Sur l\'axe des y se trouve la distribution du nombre d\'étudiants en fonction du statut de livraison soumis (vert), non soumis (rouge), soumis en retard (jaune). En cliquant sur les différentes zones du graphique, vous pouvez envoyer un mail aux groupes d\'étudiants que vous souhaitez (ceux ayant envoyé le devoir en retard ou non, ceux ayant envoyé leur devoir à temps...).';
$string['ta_access_content_help_title'] = 'Accès au contenu du cours ';
$string['ta_access_content_help_description_p1'] = 'Ce graphique montre le nombre d\'étudiants qui ont accédé et n\'ont pas accédé aux ressources du cours. En haut se trouvent les différents types de ressources Moodle, avec la possibilité de filtrer les informations du graphe en fonction du type de ressource sélectionné. ';
$string['ta_access_content_help_description_p2'] = 'L\' axe des x du graphique montre le nombre d\'étudiants inscrits au cours. l\'axe des y du graphique montre les ressources des sections affectées à la semaine. De plus, ce graphique vous permet d’envoyer un e-mail aux étudiants qui ont accédé à la ressource ou à ceux qui n’y ont pas accédé en cliquant sur le graphique. ';

/* Assign Submissions */
$string['nmp_intime_sub'] = 'Devoir déposé à temps';
$string['nmp_late_sub'] = 'Devoir déposé en retard';
$string['nmp_no_sub'] = 'Devoir non déposé';
$string['nmp_assign_nodue'] = 'Pas de date limite';
$string['nmp_assignsubs_title'] = 'Suivi des devoirs soumis dans les zones de dépôts';
$string['nmp_assignsubs_yaxis'] = 'Nombre d\'étudiants';


/* Accès au contenu */
$string['nmp_assign'] = 'Tâche';
$string['nmp_assignment'] = 'Tâche';
$string['nmp_attendance'] = 'Participation';
$string['nmp_book'] = 'Livre';
$string['nmp_chat'] = 'Chatter';
$string['nmp_choice'] = 'Choix';
$string['nmp_data'] = 'Base de données';
$string['nmp_feedback'] = 'Commentaires';
$string['nmp_folder'] = 'Dossier';
$string['nmp_forum'] = 'Forum';
$string['nmp_glossary'] = 'Glossaire';
$string['nmp_h5pactivity'] = 'H5P';
$string['nmp_imscp'] = 'Contenu IMS';
$string['nmp_label'] = 'Label';
$string['nmp_lesson'] = 'Leçon';
$string['nmp_lti'] = 'Contenu IMS';
$string['nmp_page'] = 'Page';
$string['nmp_quiz'] = 'Quiz';
$string['nmp_resource'] = 'Ressource';
$string['nmp_scorm'] = 'Package SCORM';
$string['nmp_survey'] = 'Sondage';
$string['nmp_url'] = 'Url';
$string['nmp_wiki'] = 'Wiki';
$string['nmp_workshop'] = 'Atelier';

$string['nmp_access'] = 'Ressources accédées';
$string['nmp_no_access'] = 'Ressources non accédées';
$string['nmp_access_chart_title'] = 'Accès au contenu du cours';
$string['nmp_access_chart_yaxis_label'] = 'Nombre d\'étudiants';
$string['nmp_access_chart_suffix'] = 'étudiants';


/* Email */
$string['nmp_validation_subject_text'] = 'Le sujet est obligatoire';
$string['nmp_validation_message_text'] = 'Veuillez écrire un message';
$string['nmp_subject_label'] = 'Ajouter un sujet';
$string['nmp_message_label'] = 'Ajouter un message';

$string['nmp_submit_button'] = 'Envoyer';
$string['nmp_cancel_button'] = 'Annuler';
$string['nmp_close_button'] = 'Fermer';
$string['nmp_emailform_title'] = 'Envoyer un e-mail';
$string['nmp_sending_text'] = 'Envoi de courriels';

$string['nmp_recipients_label'] = 'À';
$string['nmp_mailsended_text'] = 'Emails envoyés';

$string['nmp_email_footer_text'] = 'Ceci est un email envoyé avec note my progress.';
$string['nmp_email_footer_prefix'] = 'Aller à';
$string['nmp_email_footer_suffix'] = 'pour plus d\'informations.';
$string['nmp_mailsended_text'] = 'Emails envoyés';

$string['nmp_assign_url'] = '/mod/assign/view.php?id=';
$string['nmp_assignment_url'] = '/mod/assignment/view.php?id=';
$string['nmp_book_url'] = '/mod/book/view.php?id=';
$string['nmp_chat_url'] = '/mod/chat/view.php?id=';
$string['nmp_choice_url'] = '/mod/choice/view.php?id=';
$string['nmp_data_url'] = '/mod/data/view.php?id=';
$string['nmp_feedback_url'] = '/mod/feedback/view.php?id=';
$string['nmp_folder_url'] = '/mod/folder/view.php?id=';
$string['nmp_forum_url'] = '/mod/forum/view.php?id=';
$string['nmp_glossary_url'] = '/mod/glossary/view.php?id=';
$string['nmp_h5pactivity_url'] = '/mod/h5pactivity/view.php?id=';
$string['nmp_imscp_url'] = '/mod/imscp/view.php?id=';
$string['nmp_label_url'] = '/mod/label/view.php?id=';
$string['nmp_lesson_url'] = '/mod/lesson/view.php?id=';
$string['nmp_lti_url'] = '/mod/lti/view.php?id=';
$string['nmp_page_url'] = '/mod/page/view.php?id=';
$string['nmp_quiz_url'] = '/mod/quiz/view.php?id=';
$string['nmp_resource_url'] = '/mod/resource/view.php?id=';
$string['nmp_scorm_url'] = '/mod/scorm/view.php?id=';
$string['nmp_survey_url'] = '/mod/survey/view.php?id=';
$string['nmp_url_url'] = '/mod/url/view.php?id=';
$string['nmp_wiki_url'] = '/mod/wiki/view.php?id=';
$string['nmp_workshop_url'] = '/mod/workshop/view.php?id=';
$string['nmp_course_url'] = '/course/view.php?id=';


/* Évaluation de l\'enseignant */
$string['tr_section_help_title'] = 'Suivi des notes';
$string['tr_section_help_description'] = 'Cette section contient des indicateurs liés aux moyennes des notes dans les activités évaluables. Les différentes unités d\'enseignement (Catégories de Qualification) créées par l\'enseignant sont affichées dans le sélecteur <i> Catégorie de Qualification </i>. Ce sélecteur vous permettra de basculer entre les différentes unités définies et de montrer les activités qui peuvent être évaluées dans chacune. ';
$string['tr_grade_items_average_help_title'] = 'Moyenne des activités évaluables';
$string['tr_grade_items_average_help_description_p1'] = 'Ce graphique présente la moyenne (en pourcentage) des notes des étudiants dans chacune des activités évaluables du cours. La moyenne en pourcentage est calculée en fonction de la note maximale de l\'activité évaluable (exemple: une activité évaluable avec un score maximum de 80 et une note moyenne de 26 présentera une barre d\'une hauteur égale à 33%, puisque 26 est 33% de la note totale). La moyenne pondérée cumulative a été exprimée en fonction de pourcentages afin de préserver la symétrie du graphique, car Moodle vous permet de créer des activités et d’attribuer des notes personnalisées. ';
$string['tr_grade_items_average_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent les différentes activités évaluables du cours. Sur l’axe des y se trouve la moyenne pondérée exprimée en pourcentage. ';
$string['tr_grade_items_average_help_description_p3'] = 'En cliquant sur la barre correspondant à une activité évaluable, les données des deux graphiques inférieurs seront mises à jour pour afficher des informations supplémentaires sur l\'activité évaluable sélectionnée.';
$string['tr_item_grades_details_help_title'] = 'Meilleure note, pire note et note moyenne';
$string['tr_item_grades_details_help_description_p1'] = 'Ce graphique montre la meilleure note, la note moyenne et la pire note pour une activité évaluable (l’activité sélectionnée dans le tableau des activités évaluables moyennes).';
$string['tr_item_grades_details_help_description_p2'] = 'Sur l\' axe des x du graphique se trouve le score de la note d\'activité, la note maximale de l\' activité étant la valeur maximale sur cet axe. Sur l’axe des y figurent les libellés de la meilleure note, de la note moyenne et de la pire note. ';
$string['tr_item_grades_distribution_help_title'] = 'Répartition des notes';
$string['tr_item_grades_distribution_help_description_p1'] = 'Ce graphique montre la répartition des élèves dans différentes gammes de notes. Les gammes de notes sont calculées en fonction de pourcentages. Les plages suivantes sont prises en compte: moins de 50%, plus de 50%, plus de 60%, plus de 70%, plus de 80% et plus de 90%. Ces fourchettes sont calculées en fonction du poids maximum que l\'enseignant attribue à une activité évaluable. ';
$string['tr_item_grades_distribution_help_description_p2'] = 'Sur l\' axe des x se trouvent les plages de notes d\'activité. Sur l’axe des y figure le nombre d’élèves appartenant à un certain rang. ';
$string['tr_item_grades_distribution_help_description_p3'] = 'En cliquant sur la barre correspondant à un rang, vous pouvez envoyer un email aux étudiants dans le classement.';

/* Notes */
$string['nmp_grades_select_label'] = 'Catégorie de note';
$string['nmp_grades_chart_title'] = 'Moyennes des activités évaluables';
$string['nmp_grades_yaxis_title'] = 'Note moyenne (%)';
$string['nmp_grades_tooltip_average'] = 'Note moyenne';
$string['nmp_grades_tooltip_grade'] = 'Note maximale';
$string['nmp_grades_tooltip_student'] = 'étudiant noté de';
$string['nmp_grades_tooltip_students'] = 'élèves notés de';

$string['nmp_grades_best_grade'] = 'Meilleure note';
$string['nmp_grades_average_grade'] = 'Note moyenne';
$string['nmp_grades_worst_grade'] = 'Pire note';
$string['nmp_grades_details_subtitle'] = 'Meilleure note, pire note et note moyenne';

$string['nmp_grades_distribution_subtitle'] = 'Répartition des notes';
$string['nmp_grades_distribution_greater_than'] = 'supérieur à';
$string['nmp_grades_distribution_smaller_than'] = 'inférieur à';
$string['nmp_grades_distribution_yaxis_title'] = 'Nombre d\'étudiants';
$string['nmp_grades_distribution_tooltip_prefix'] = 'Plage';
$string['nmp_grades_distribution_tooltip_suffix'] = 'dans cette plage';
$string["nmp_view_details"] = "(Cliquez pour voir les détails)";


/* Quiz enseignant */
$string['tq_section_help_title'] = 'Suivi des évaluations';
$string['tq_section_help_description'] = 'Cette section contient des indicateurs liés au résumé des tentatives dans les différentes évaluations du cours et à l\' analyse des questions d\'une évaluation. Les données présentées dans cette section varient en fonction de la semaine d\'étude sélectionnée et d\'un sélecteur contenant toutes les activités de type Evaluation des sections de cours affectées à la semaine sélectionnée. ';
$string['tq_questions_attempts_help_title'] = 'Tentatives de questions';
$string['tq_questions_attempts_help_description_p1'] = 'Ce graphique montre la distribution des tentatives de résolution pour chaque question dans une évaluation ainsi que leur statut de révision.';
$string['tq_questions_attempts_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent les questions d\'évaluation. l\'axe des y montre le nombre de tentatives de résolution pour chacune de ces questions. La symétrie du graphique sera affectée par les paramètres d\'évaluation (exemple: dans une évaluation qui comporte toujours les mêmes questions, le graphique présentera le même nombre de tentatives pour chaque barre correspondant à une question. Dans une évaluation comportant des questions aléatoires ( d\'une banque de questions), le graphique présentera dans la barre de chaque question la somme des tentatives d\'évaluation dans lesquelles elle est apparue, et peut ne pas être la même pour chaque question d\'évaluation). ';
$string['tq_questions_attempts_help_description_p3'] = 'En cliquant sur l\' une des barres correspondant à une question, il est possible de voir la question d\'évaluation dans une fenêtre pop-up.';
$string['tq_hardest_questions_help_title'] = 'Questions les plus difficiles';
$string['tq_hardest_questions_help_description_p1'] = 'Ce graphique montre les questions d’évaluation classées par niveau de difficulté. Une tentative de résolution d\'une question avec le statut Partiellement correct, incorrect ou vide est considérée comme incorrecte, de sorte que le nombre total de tentatives incorrectes d\'une question est la somme des tentatives avec les statuts susmentionnés. Le niveau de difficulté est représenté sous forme de pourcentage calculé sur la base du nombre total de tentatives. ';
$string['tq_hardest_questions_help_description_p2'] = 'Sur l\' axe des x du graphique se trouvent les questions d\'évaluation identifiées par leur nom. l\'axe des y montre le pourcentage de tentatives incorrectes par rapport au nombre total de tentatives pour la question. Cet axe permet d\'identifier les questions qui ont représenté la plus grande difficulté pour les étudiants qui ont passé l\'évaluation. ';
$string['tq_hardest_questions_help_description_p3'] = 'En cliquant sur l\' une des barres correspondant à une question, il est possible de voir la question d\'évaluation dans une fenêtre pop-up.';

$string["nmp_quiz_info_text"] = "Cette évaluation a";
$string["nmp_question_text"] = "question";
$string["nmp_questions_text"] = "questions";
$string["nmp_doing_text_singular"] = "tentative faite par";
$string["nmp_doing_text_plural"] = "tentatives faites par";
$string["nmp_attempt_text"] = "tentative";
$string["nmp_attempts_text"] = "tentatives";
$string["nmp_student_text"] = "étudiant";
$string["nmp_students_text"] = "étudiants";
$string["nmp_quiz"] = "Évaluations";
$string["nmp_questions_attempts_chart_title"] = "Tentatives de questions";
$string["nmp_questions_attempts_yaxis_title"] = "Nombre de tentatives";
$string["nmp_hardest_questions_chart_title"] = "Questions plus difficiles";
$string["nmp_hardest_questions_yaxis_title"] = "Tentatives incorrectes";
$string["nmp_correct_attempt"] = "Correcte";
$string["nmp_partcorrect_attempt"] = "Partiellement correcte";
$string["nmp_incorrect_attempt"] = "Incorrecte";
$string["nmp_blank_attempt"] = "Vide";
$string["nmp_needgraded_attempt"] = "Non noté";
$string["nmp_review_question"] = "(Cliquez pour revoir la question)";


/* Décrochage */
$string['td_section_help_title'] = 'Décrochage';
$string['td_section_help_description'] = 'Cette section contient des indicateurs liés à la prédiction du décrochage des étudiants dans un cours. Les informations sont affichées en fonction de groupes d\'étudiants calculés par un algorithme qui analyse le comportement de chaque élève en fonction du temps investi, du nombre de sessions d\'étudiants, du nombre de jours d\'activité et des interactions qu\'ils ont faites avec chaque ressource et avec l\'autre étudiants dans le cours. l\'algorithme place les étudiants ayant un comportement similaire dans le même groupe, afin que les étudiants qui sont de plus en moins engagés dans le cours puissent être identifiés. Les données présentées dans cette section varient en fonction du groupe sélectionné dans le sélecteur qui contient les groupes identifiés dans le cours. ';
$string['td_group_students_help_title'] = 'Regrouper les étudiants';
$string['td_group_students_help_description_p1'] = 'Dans ce tableau se trouvent les étudiants appartenant au groupe sélectionné dans le sélecteur de groupe d\'étudiants. La photo de chaque élève, les noms et le pourcentage de progression dans le cours sont répertoriés. Pour le calcul de la progression, toutes les ressources du cours ont été prises en compte, à l\'exception de celles de type Label. Pour déterminer si un étudiant a terminé une ressource, il est d\'abord vérifié pour voir si le paramètre d\'exhaustivité de la ressource est activé. Si tel est le cas, il est recherché si l\'élève a déjà terminé l\'activité basée sur cette configuration. Sinon, l’activité est considérée comme terminée si l’élève l’a vue au moins une fois. ';
$string['td_group_students_help_description_p2'] = 'Cliquer sur un élève dans ce tableau mettra à jour les graphiques ci-dessous avec les informations de l\'élève sélectionné.';
$string['td_modules_access_help_title'] = 'Ressources du cours';
$string['td_modules_access_help_description_p1'] = 'Ce graphique montre la quantité de ressources auxquelles l\'étudiant a accédé et complété. Les données présentées dans ce graphique varient en fonction de l\'élève sélectionné dans le tableau des étudiants du groupe. Pour déterminer la quantité de ressources et terminer les activités, la configuration Moodle appelée Achèvement des activités est utilisée. Si l\'enseignant ne fait pas la configuration d\'exhaustivité des activités du cours, le nombre d\'activités accédées et terminées sera toujours le même, car sans une telle configuration, une ressource est considérée comme terminée lorsque l\'étudiant y accède. ';
$string['td_modules_access_help_description_p2'] = 'Sur l\'axe des x se trouve la quantité de ressources du cours. Sur l’axe des y figurent les libellés des ressources consultées, complètes et totales du cours. ';
$string['td_modules_access_help_description_p3'] = 'En cliquant sur n\'importe quelle barre, il est possible de voir les ressources et activités disponibles dans le cours (dans une fenêtre pop-up) ainsi que le nombre d\'interactions des étudiants avec chaque ressource et une étiquette de non consulté, consulté ou terminé. ';
$string['td_week_modules_help_title'] = 'Ressources par semaines';
$string['td_week_modules_help_description_p1'] = 'Ce graphique montre la quantité de ressources auxquelles l\'étudiant a accédé et complété pour chacune des semaines configurées dans le plugin. Les données présentées dans ce graphique varient en fonction de l’élève sélectionné dans le tableau <i> Groupe d’étudiants </i>. ';
$string['td_week_modules_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les différentes semaines d\'étude configurées. L’axe des y montre la quantité de ressources et d’activités consultées et réalisées par l’élève. ';
$string['td_week_modules_help_description_p3'] = 'En cliquant sur n\'importe quelle barre, il est possible de voir les ressources et activités disponibles dans le cours (dans une fenêtre pop-up) ainsi que le nombre d\'interactions des étudiants avec chaque ressource et une étiquette de non consulté, consulté ou terminé. ';
$string['td_sessions_evolution_help_title'] = 'Sessions et temps passé';
$string['td_sessions_evolution_help_description_p1'] = 'Ce graphique montre comment les sessions d\'étude ont évolué depuis que votre première session a été enregistrée dans le cours. Les données présentées dans ce graphique varient en fonction de l’élève sélectionné dans le tableau <i> Groupe d’étudiants </i>. ';
$string['td_sessions_evolution_help_description_p2'] = 'L\'axe des x du graphique montre une chronologie avec les jours qui se sont écoulés depuis que l\'étudiant a fait la première session d\'étude jusqu\'au jour de la dernière session enregistrée. Sur l\'axe des y, ils affichent 2 valeurs, sur le côté gauche le nombre de sessions d\'étudiants et sur le côté droit le temps passé en heures. Entre ces axes, le nombre de sessions et le temps investi de l\'étudiant sont tirés comme une série de temps. ';
$string['td_sessions_evolution_help_description_p3'] = 'Cette visualisation vous permet de zoomer sur une région sélectionnée. Cette approche permet de montrer clairement cette évolution dans différentes plages de dates. ';
$string['td_user_grades_help_title'] = 'Notes';
$string['td_user_grades_help_description_p1'] = 'Ce graphique montre une comparaison des notes de l\' élève avec les moyennes des notes (moyenne en pourcentage) de leurs pairs dans les différentes activités évaluables du cours. Les données présentées dans ce graphique varient en fonction de l’élève sélectionné dans le tableau <i> Groupe d’étudiants </i>. ';
$string['td_user_grades_help_description_p2'] = 'Les différentes activités évaluables sont affichées sur l\' axe des x du graphique. Sur l\'axe des y se trouvent la note de l\'élève et la note moyenne de ses pairs. La note de l\'étudiant et la moyenne du cours sont affichées sous forme de pourcentage pour maintenir la symétrie du graphique. ';
$string['td_user_grades_help_description_p3'] = 'Avec un clic sur la barre correspondant à une activité, il est possible d\' aller à ladite analysée. ';

$string["nmp_cluster_label"] = "Groupe";
$string["nmp_cluster_select"] = 'Groupe d\'étudiants';
$string["nmp_dropout_table_title"] = "Étudiants du groupe";
$string["nmp_dropout_see_profile"] = "Afficher le profil";
$string["nmp_dropout_user_never_access"] = "Jamais accédé";
$string["nmp_dropout_student_progress_title"] = "Progression de l'élève";
$string["nmp_dropout_student_grade_title"] = "Note";
$string['nmp_dropout_no_data'] = "Il n'y a pas encore de données de décrochage scolaire concernant ce cours.";
$string['nmp_dropout_no_users_cluster'] = "Il n'y a aucun étudiant dans ce groupe";
$string['nmp_dropout_generate_data_manually'] = "Générer manuellement les données";
$string['nmp_dropout_generating_data'] = "Génération de données...";
$string["nmp_modules_access_chart_title"] = "Ressources du cours";
$string["nmp_modules_access_chart_series_total"] = "Total";
$string["nmp_modules_access_chart_series_complete"] = "Terminé";
$string["nmp_modules_access_chart_series_viewed"] = "Consulté";
$string["nmp_week_modules_chart_title"] = "Ressources par semaines";
$string["nmp_modules_amount"] = "Quantité de ressources";
$string["nmp_modules_details"] = "(Cliquez pour voir les ressources)";
$string["nmp_modules_interaction"] = "interaction";
$string["nmp_modules_interactions"] = "interactions";
$string["nmp_modules_viewed"] = "Consulté";
$string["nmp_modules_no_viewed"] = "Non consulté";
$string["nmp_modules_complete"] = "Terminé";
$string["nmp_sessions_evolution_chart_title"] = "Sessions et temps investi";
$string["nmp_sessions_evolution_chart_xaxis1"] = "Nombre de sessions";
$string["nmp_sessions_evolution_chart_xaxis2"] = "Nombre d'heures";
$string["nmp_sessions_evolution_chart_legend1"] = "Nombre de sessions";
$string["nmp_sessions_evolution_chart_legend2"] = "Temps investi";
$string["nmp_user_grades_chart_title"] = "Notes";
$string["nmp_user_grades_chart_yaxis"] = "Note en pourcentage";
$string["nmp_user_grades_chart_xaxis"] = "Activités évaluables";
$string["nmp_user_grades_chart_legend"] = "Cours (moyen)";
$string["nmp_user_grades_chart_tooltip_no_graded"] = "Aucune note";
$string["nmp_user_grades_chart_view_activity"] = "Cliquez pour voir l'activité";
$string['nmp_send_mail_to_user'] = 'Mail à ';
$string['nmp_send_mail_to_group'] = 'Mail au groupe';


/* Général étudiant */
$string['sg_section_help_title'] = 'Indicateurs généraux';
$string['sg_section_help_description'] = 'Cette section contient des indicateurs liés à vos informations, progrès, indicateurs généraux, ressources du cours, sessions tout au long du cours et notes obtenues. Les affichages de cette section montrent les indicateurs tout au long du cours (jusqu\'à la date actuelle). ';
$string['sg_modules_access_help_title'] = 'Ressources du cours';
$string['sg_modules_access_help_description_p1'] = 'Ce graphique montre la quantité de ressources que vous avez consultées et complétées. Pour déterminer la quantité de ressources que vous avez terminées, utilisez la configuration Moodle appelée Achèvement des activités. Si l\'enseignant n\'a pas configuré l\'exhaustivité des activités du cours, le nombre d\'activités accédées et terminées sera toujours le même, car sans une telle configuration, une ressource est considérée comme terminée lorsque vous y accédez. ';
$string['sg_modules_access_help_description_p2'] = 'Sur l\'axe des x se trouve la quantité de ressources du cours. Sur l’axe des y figurent les libellés des ressources accessibles, complètes et totales en référence à vos interactions avec les ressources du cours. ';
$string['sg_modules_access_help_description_p3'] = 'En cliquant sur n\'importe quelle barre, il est possible de voir les ressources et activités disponibles dans le cours (dans une fenêtre pop-up) ainsi que le nombre d\'interactions que vous avez faites avec chaque ressource et une étiquette non consulté, consulté ou terminé. ';
$string['sg_weeks_session_help_title'] = 'Sessions par semaine';
$string['sg_weeks_session_help_description_p1'] = 'Ce graphique montre le nombre de sessions d\'étude que vous avez suivies chaque semaine à partir de la date de début du cours. l\'accès au cours est considéré comme le début d\'une session d\'étude. Une session est considérée comme terminée lorsque le temps écoulé entre deux interactions dépasse 30 minutes. ';
$string['sg_weeks_session_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les semaines de chaque mois. l\'axe des y du graphique montre les différents mois de l\'année à partir du mois de création du cours. Pour maintenir la symétrie du graphique, un total de cinq semaines a été placé pour chaque mois, cependant, chaque mois ne compte pas autant de semaines. Ces mois n’ajouteront que des sessions jusqu’à la quatrième semaine. ';
$string['sg_sessions_evolution_help_title'] = 'Sessions et temps investi';
$string['sg_sessions_evolution_help_description_p1'] = 'Ce graphique montre comment vos sessions d\'étude ont évolué depuis que votre première session a été inscrite au cours. ';
$string['sg_sessions_evolution_help_description_p2'] = 'L\'axe des x du graphique montre une chronologie avec les jours qui se sont écoulés depuis votre première session d\'étude jusqu\'au jour de votre dernière session enregistrée. Sur l\'axe des y, ils affichent 2 valeurs, sur le côté gauche votre nombre de sessions et sur le côté droit votre temps passé en heures. Entre ces axes, votre nombre de sessions et votre temps passé en tant qu\'étudiant sont représentés sous forme de séries chronologiques. ';
$string['sg_sessions_evolution_help_description_p3'] = 'Cette visualisation vous permet de zoomer sur une région sélectionnée.';
$string['sg_user_grades_help_title'] = 'Notes';
$string['sg_user_grades_help_description_p1'] = 'Ce graphique montre une comparaison de vos notes avec les moyennes des notes (moyenne en pourcentage) de vos camarades de classe dans les différentes activités évaluables du cours.';
$string['sg_user_grades_help_description_p2'] = 'L\'axe des x du graphique montre les différentes activités évaluables. Sur l\'axe des y, vous trouverez vos notes et la note moyenne de vos camarades de classe. Votre note et la moyenne du cours sont affichées en pourcentage pour maintenir la symétrie du graphique. ';
$string['sg_user_grades_help_description_p3'] = 'En cliquant sur la barre correspondant à une activité, il est possible d\'accéder à celle analysée. ';

/* Sessions utilisateur */
$string['ss_section_help_title'] = 'Sessions d\'étude';
$string['ss_section_help_description'] = 'Cette section contient des visualisations avec des indicateurs liés à votre activité dans le cours mesurés en termes de sessions d\'étude, de temps passé et de progression dans chacune des semaines configurées par l\'enseignant. Les affichages de cette section varient en fonction de la semaine d\'étude sélectionnée. ';
$string['ss_inverted_time_help_title'] = 'Votre temps investi';
$string['ss_inverted_time_help_description_p1'] = 'Ce graphique montre votre temps passé dans la semaine par rapport au temps prévu par le professeur.';
$string['ss_inverted_time_help_description_p2'] = 'Sur l\'axe des x du graphique se trouve le nombre d\'heures que l\'enseignant a prévu pour une semaine spécifique. Sur l’axe des y figurent les étiquettes du temps passé et du temps à y consacrer. ';
$string['ss_hours_session_help_title'] = 'Sessions par jour et heure';
$string['ss_hours_session_help_description_p1'] = 'Ce graphique montre vos sessions d\' étude par jour et heure de la semaine sélectionnée. l\'accès au cours est considéré comme le début d\'une session d\'étude. Une session est considérée comme terminée lorsque le temps écoulé entre deux interactions dépasse 30 minutes. ';
$string['ss_hours_session_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les jours de la semaine. Sur l’axe des y figurent les heures de la journée commençant à 12h et se terminant à 23h ou 23h. ';
$string['ss_resources_access_help_title'] = 'Interaction par types de ressources';
$string['ss_resources_access_help_description_p1'] = 'Ce graphique montre combien de ressources vous avez en attente et celles que vous avez déjà complétées dans la semaine sélectionnée. Les ressources sont regroupées par type dans ce graphique. De plus, une barre s\'affiche en haut qui représente le pourcentage de ressources accédées par rapport au total des ressources affectées à la semaine sélectionnée. ';
$string['ss_resources_access_help_description_p2'] = 'Sur l\'axe des x du graphique se trouvent les différents types de ressources. L’axe des y indique la quantité de ressources consultées pour la semaine.';
$string['ss_resources_access_help_description_p3'] = 'En cliquant sur n\'importe quelle barre, il est possible de voir les ressources et activités disponibles dans le cours (dans une fenêtre pop-up) ainsi que le nombre d\'interactions que vous avez faites avec chaque ressource et une étiquette non consulté, consulté ou terminé. ';


$string['nmp_student_time_inverted_title'] = 'Votre temps investi';
$string['nmp_student_time_inverted_x_axis'] = 'Nombre d\'heures';
$string['nmp_student_inverted_time'] = 'Temps investi';
$string['nmp_student_expected_time'] = 'Temps à investir';

$string['nmp_resource_access_title'] = 'Interaction par types de ressources';
$string['nmp_resource_access_y_axis'] = 'Quantité de ressources';
$string['nmp_resource_access_x_axis'] = 'Types de ressources';
$string['nmp_resource_access_legend1'] = 'Terminé';
$string['nmp_resource_access_legend2'] = 'En attente';

$string['nmp_week_progress_title'] = 'Progrès de la semaine';



/* Indicateurs de l\'enseignant */
$string['nmp_teacher_indicators_title'] = 'Indicateurs généraux';
$string['nmp_teacher_indicators_students'] = 'Etudiants inscrits';
$string['nmp_teacher_indicators_weeks'] = 'Semaines configurées';
$string['nmp_teacher_indicators_grademax'] = 'Note maximum';
$string['nmp_teacher_indicators_course_start'] = 'Démarre le';
$string['nmp_teacher_indicators_course_end'] = 'Se termine le';
$string['nmp_teacher_indicators_course_format'] = 'Format';
$string['nmp_teacher_indicators_course_completion'] = 'Complétude des modules';
$string["nmp_teacher_indicators_student_progress"] = "Progression des élèves";
$string["nmp_teacher_indicators_week_resources_chart_title"] = "Ressources par semaines";
$string["nmp_teacher_indicators_week_resources_yaxis_title"] = "Quantité de ressources";

/* Logs visualisation */
$string['nmp_logs_title'] = 'Télécharger les journaux d\'activités';
$string['nmp_logs_help_description'] = 'Cette section vous permet de télécharger les journaux d\'activités qui ont été réalisés. C\'est-à-dire que vous avez accès aux actions qui ont été réalisées par les utilisateurs inscrits sur la plate-forme sous forme d\'un tableur.';
$string['nmp_logs_title_MoodleSetpoint_title'] = 'Sélectionnez un interval de date pour les actions réalisées sur Moodle';
$string['nmp_logs_title_MMPSetpoint_title'] = 'Sélectionnez un interval de date pour les actions réalisées sur Note My Progress';
$string['nmp_logs_help'] = 'Cette section vous permet de télécharger un fichier de journal des activités effectuées.';
$string['nmp_logs_select_date'] = 'Sélectionnez un interval de temps pour le journal';
$string['nmp_logs_first_date'] = 'Date de début';
$string['nmp_logs_last_date'] = 'Date de fin';
$string['nmp_logs_valid_Moodlebtn'] = 'Télécharger le journal d\'activités de Moodle';
$string['nmp_logs_valid_NMPbtn'] = 'Télécharger le journal d\'activités de NMP';
$string['nmp_logs_invalid_date'] = 'Veuillez saisir une date';
$string['nmp_logs_download_btn'] = 'Téléchargement en cours';
$string['nmp_logs_download_nmp_help_title'] = 'A propos des actions réalisées sur Note My Progress';
$string['nmp_logs_download_moodle_help_title'] = 'A propos des actions réalisées sur Moodle';
$string['nmp_logs_download_nmp_help_description'] = 'Le fichier de logs qui est téléchargé répertorie toutes les actions qui ont été réalisées par l\'utilisateur au sein du plugin Note My Progress uniquement (consultation des avancées, consultation des indicateurs généraux, etc.).';
$string['nmp_logs_download_moodle_help_description'] = 'Le fichier de logs qui est téléchargé répertorie toutes les actions qui ont été réalisées par l\'utilisateur au sein de Moodle uniquement (visualisation du cours, visualisation des ressources, dépôt d\'un devoir, etc.)';
/* Logs CSV Header */
$string['nmp_logs_csv_headers_username'] = 'Nom d\'utilisateur';
$string['nmp_logs_csv_headers_firstname'] = 'Prénom';
$string['nmp_logs_csv_headers_lastname'] = 'Nom';
$string['nmp_logs_csv_headers_date'] = 'Date';
$string['nmp_logs_csv_headers_hour'] = 'Heure';
$string['nmp_logs_csv_headers_action'] = 'Action';
$string['nmp_logs_csv_headers_coursename'] = 'Nom du cours';
$string['nmp_logs_csv_headers_detail'] = 'Détail';
$string['nmp_logs_csv_headers_detailtype'] = 'Type d\'objet utilisé';

$string['nmp_logs_error_begin_date_superior'] = 'La date de début ne peut pas être supérieure à la date actuelle';
$string['nmp_logs_error_begin_date_inferior'] = 'La date de début doit être antérieure à la date de fin';
$string['nmp_logs_error_empty_dates'] = 'Les dates ne peuvent pas être vides';
$string['nmp_logs_error_problem_encountered'] = 'Un problème a été rencontré, veuillez réessayer';

$string['nmp_logs_success_file_downloaded'] = 'Fichier téléchargé !';


$string['nmp_logs_moodle_csv_headers_role'] = 'Role';
$string['nmp_logs_moodle_csv_headers_email'] = 'Email';
$string['nmp_logs_moodle_csv_headers_username'] = 'Username';
$string['nmp_logs_moodle_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_moodle_csv_headers_date'] = 'Date';
$string['nmp_logs_moodle_csv_headers_hour'] = 'Hour';
$string['nmp_logs_moodle_csv_headers_action'] = 'Action';
$string['nmp_logs_moodle_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_moodle_csv_headers_coursename'] = 'Course_name';
$string['nmp_logs_moodle_csv_headers_detailid'] = 'Detail ID';
$string['nmp_logs_moodle_csv_headers_details'] = 'Details';
$string['nmp_logs_moodle_csv_headers_detailstype'] = 'Details_type';

$string['nmp_logs_moodle_csv_headers_role_description'] = 'Donne le rôle qu\'a l\'utilisateur sur le cours sur lequel il a fait une action (étudiant, enseignant...)';
$string['nmp_logs_moodle_csv_headers_email_description'] = 'Donne l\'e-mail de l\'utilisateur';
$string['nmp_logs_moodle_csv_headers_username_description'] = 'Donne le nom d\'utilisateur moodle de la personne ayant réalisée l\'action';
$string['nmp_logs_moodle_csv_headers_fullname_description'] = 'Donne le nom complet de l\'utilisateur (Prénom + Nom)';
$string['nmp_logs_moodle_csv_headers_date_description'] = 'Donne la date à laquelle l\'action a été réalisée au format dd-MM-YYYY';
$string['nmp_logs_moodle_csv_headers_hour_description'] = 'Donne l\'heure a laquelle a été réalisée l\'action';
$string['nmp_logs_moodle_csv_headers_action_description'] = 'Donne un verbe décrivant l\'action qui a été réalisée (ex: cliqué, vu...)';
$string['nmp_logs_moodle_csv_headers_courseid_description'] = 'Donne l\'identifiant du cours sur lequel a été réalisée l\'action';
$string['nmp_logs_moodle_csv_headers_coursename_description'] = 'Donne le nom du cours sur lequel a été réalisée l\'action';
$string['nmp_logs_moodle_csv_headers_detailid_description'] = 'Donne l\'identifiant de l\'objet avec lequel l\'utilisateur a interagi';
$string['nmp_logs_moodle_csv_headers_details_description'] = 'Donne le nom de l\'objet qui a été visé';
$string['nmp_logs_moodle_csv_headers_detailstype_description'] = 'Donne le type d\'objet qui a été visé (exemples d\'objets: Dépôt, Quizz, Ressources...)';


$string['nmp_logs_nmp_csv_headers_role_description'] = 'Donne le rôle qu\'a l\'utilisateur sur le cours sur lequel il a fait une action (étudiant, enseignant...)';
$string['nmp_logs_nmp_csv_headers_email_description'] = 'Donne l\'e-mail de l\'utilisateur';
$string['nmp_logs_nmp_csv_headers_username_description'] = 'Donne le nom d\'utilisateur moodle de la personne ayant réalisée l\'action';
$string['nmp_logs_nmp_csv_headers_fullname_description'] = 'Donne le nom complet de l\'utilisateur (Prénom + Nom)';
$string['nmp_logs_nmp_csv_headers_date_description'] = 'Donne la date à laquelle l\'action a été réalisée au format dd-MM-YYYY';
$string['nmp_logs_nmp_csv_headers_hour_description'] = 'Donne l\'heure a laquelle a été réalisée l\'action';
$string['nmp_logs_nmp_csv_headers_courseid_description'] = 'Donne l\'identifiant du cours sur lequel a été réalisée l\'action';
$string['nmp_logs_nmp_csv_headers_section_name_description'] = 'Donne le nom de la section de note my progress dans laquelle se trouvait l\'utilisateur lorsqu\'il a réalisé l\'action';
$string['nmp_logs_nmp_csv_headers_action_type_description'] = 'Donne une description complète de l\'action qui a été réalisée par l\'utilisateur sous la forme verbe + sujet + objet (ex: downloaded_moodle_logfile)';


$string['nmp_logs_moodle_table_title'] = 'Description des intitulés';
$string['nmp_logs_moodle_table_subtitle'] = 'Concernant les logs de Moodle';

$string['nmp_logs_nmp_table_title'] = 'Description des intitulés';
$string['nmp_logs_nmp_table_subtitle'] = 'Concernant les logs de Note My Progress';





$string['nmp_logs_nmp_csv_headers_role'] = 'Role';
$string['nmp_logs_nmp_csv_headers_email'] = 'Email';
$string['nmp_logs_nmp_csv_headers_username'] = 'Username';
$string['nmp_logs_nmp_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_nmp_csv_headers_date'] = 'Date';
$string['nmp_logs_nmp_csv_headers_hour'] = 'Hour';
$string['nmp_logs_nmp_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_nmp_csv_headers_section_name'] = 'NMP_SECTION_NAME';
$string['nmp_logs_nmp_csv_headers_action_type'] = 'NMP_ACTION_TYPE';

$string['nmp_logs_table_title'] = 'Intitulé';
$string['nmp_logs_table_title_bis'] = 'Description';

$string['nmp_logs_help_button_nmp'] = 'A propos des actions réalisées sur Note My Progress';
$string['nmp_logs_help_button_moodle'] = 'A propos des actions réalisées sur Moodle';



$string['nmp_logs_download_details_link'] = 'En savoir plus';
$string['nmp_logs_download_details_title'] = 'Êtes-vous sûr de vouloir un rapport d\'explications détaillées ?';
$string['nmp_logs_download_details_description'] = 'Si vous acceptez, un fichier au format PDF sera téléchargé.';
$string['nmp_logs_download_details_ok'] = 'Télécharger';
$string['nmp_logs_download_details_cancel'] = 'Annuler';
$string['nmp_logs_download_details_validation'] = 'Le rapport a bien été téléchargé';



/* NoteMyProgress admin settings */

$string['nmp_settings_bddusername_label'] = 'Nom d\'utilisateur de la base de données';
$string['nmp_settings_bddusername_description'] = 'Ce paramètre désigne le nom d\'utilisateur depuis lequel la base de données MongoDB est accessible. Si ce paramètre est entré, il vous faut entrer le mot de passe ainsi que le nom de la base de données sur laquelle vous souhaitez vous connecter.';
$string['nmp_settings_bddusername_default'] = 'Vide';

$string['nmp_settings_bddpassword_label'] = 'Mot de passe du compte';
$string['nmp_settings_bddpassword_description'] = 'Ce paramètre désigne le mot de passe du compte depuis lequel la base de données MongoDB est accessible. Si ce paramètre est entré, il vous faut entrer le nom d\'utilisateur ainsi que le nom de la base de données sur laquelle vous souhaitez vous connecter.';
$string['nmp_settings_bddpassword_default'] = 'Vide';


$string['nmp_settings_bddaddress_label'] = 'Adresse du serveur MongoDB *';
$string['nmp_settings_bddaddress_description'] = 'Ce paramètre est l\'adresse depuis laquelle la base de données MongoDB est accessible. Ce paramètre est obligatoire et est sous la forme : 151.125.45.58    ou bien    votreserveur.com';
$string['nmp_settings_bddaddress_default'] = 'localhost';

$string['nmp_settings_bddport_label'] = 'Port de communication *';
$string['nmp_settings_bddport_description'] = 'Ce paramètre désigne le port à utiliser pour communiquer avec la base de données. Ce paramètre est obligatoire et doit être un nombre.';
$string['nmp_settings_bddport_default'] = '27017';


$string['nmp_settings_bddname_label'] = 'Nom de la base de données';
$string['nmp_settings_bddname_description'] = 'Ce paramètre désigne le nom de la base de données MongoDB dans laquelle vont être enregistrées les informations.';
$string['nmp_settings_bddname_default'] = 'Vide';

//Planning
/* Global */
$string['pagination'] = "Semaine:";
$string['graph_generating'] = "Nous construisons le rapport, veuillez patienter un instant";
$string['txt_hour'] = "Temps";
$string['txt_hours'] = "Heures";
$string['txt_minut'] = "Minutes";
$string['txt_minuts'] = "Minutes";
$string['pagination_week'] = "Semaine";
$string['only_student'] = "This report is for students only";
$string['sr_hour'] = "Heure";
$string['sr_hours'] = "Heures";
$string['sr_minute'] = "Minute";
$string['sr_minutes'] = "Minutes";
$string['sr_second'] = "Secondes";
$string['sr_seconds'] = "Secondes";
$string['weeks_not_config'] = "Le cours n'a pas été configuré par le professeur, vous ne pouvez donc pas utiliser l'outil de rapport";
$string['helplabel'] = "Aide";
$string['exitbutton'] = "Compris!";
$string['hours_unit_time_label'] = "Nombre d’heures";

/* Menú */
$string['sr_menu_main_title'] = "Rapports";
$string['sr_menu_setweek'] = "Mettre en place des semaines";
$string['sr_menu_logs'] = "Télécharger des enregistrements";
$string['sr_menu_time_worked_session_report'] = "Sessions d'étude par semaine’";
$string['sr_menu_time_visualization'] = "Temps passé (heures par semaine)";
$string['sr_menu_activities_performed'] = "Activités réalisées";
$string['sr_menu_notes'] = "Annotations";

/* General Errors */
$string['pluginname'] = "Note My Progress";
$string['previous_days_to_create_report'] = "Jours pris en compte pour la construction du rapport";
$string['previous_days_to_create_report_description'] = "Jours avant la date actuelle qui sera prise en compte pour générer le rapport";
$string['student_reports:usepluggin'] = "Utilisation du plug-in";
$string['student_reports:downloadreport'] = "Télécharger l'activité de l'élève";
$string['student_reports:setweeks'] = "Mettre en place des semaines";
$string['student_reports:activities_performed'] = "Voir le rapport 'Activités réalisées";
$string['student_reports:metareflexion'] = "Voir le rapport 'Méta-réflexion'";
$string['student_reports:notes'] = "Utilisez les bloc-notes";

/* Set weeks */
$string['setweeks_title'] = "Mettre en place des semaines";
$string['setweeks_description'] = "Pour commencer, vous devez organiser le cours par semaines et définir une date de début pour la première semaine (le reste des semaines se fera automatiquement à partir de cette date). Ensuite, vous devez associer les activités ou modules liés à chaque semaine en les faisant glisser de la colonne de droite à la semaine correspondante. Il n'est pas nécessaire d'attribuer toutes les activités ou modules aux semaines, mais seulement ceux que vous voulez prendre en compte pour le suivi des étudiants. Enfin, vous devez cliquer sur le bouton 'Enregistrer' pour conserver vos paramètres.";
$string['setweeks_sections'] = "Sections disponibles dans le cours";
$string['setweeks_weeks_of_course'] = "Planification des semaines";
$string['setweeks_add_new_week'] = "Ajouter une semaine";
$string['setweeks_start'] = "Date de début :";
$string['setweeks_end'] = "Date de finalisation :";
$string['setweeks_week'] = "Semaine";
$string['setweeks_save'] = "Sauvegarder";
$string['setweeks_time_dedication'] = "Combien d'heures de travail pensez-vous que les étudiants consacreront à leur cours cette semaine ?";
$string['setweeks_enable_scroll'] = "Activer le mode de défilement pour les semaines et les thèmes";
$string['setweeks_label_section_removed'] = "Effacer le cours";
$string['setweeks_error_section_removed'] = " Une section assignée à une semaine a été supprimée du cours, vous devez la supprimer de votre emploi du temps pour pouvoir continuer";
$string['setweeks_save_warning_title'] = "Êtes-vous sûr de vouloir sauvegarder les changements";
$string['setweeks_save_warning_content'] = "Si vous modifiez la configuration des semaines alors que le cours a déjà commencé, il est possible que des données soient perdues...";
$string['setweeks_confirm_ok'] = "Sauvegarder";
$string['setweeks_confirm_cancel'] = "Annuler";
$string['setweeks_error_empty_week'] = "Impossible de sauvegarder les changements avec une semaine sans sections. Veuillez l'effacer et réessayer.";
$string['setweeks_new_group_title'] = "Nouvelle instance de configuration";
$string['setweeks_new_group_text'] = "Nous avons détecté que votre cours est terminé, si vous souhaitez configurer les semaines pour travailler avec de nouveaux étudiants, vous devez activer le bouton ci-dessous. Cela vous permettra de séparer les données des étudiants actuels de celles des cours précédents, en évitant de les mélanger";
$string['setweeks_new_group_button_label'] = "Enregistrer configuration comme nouvelle instance";
$string['course_format_weeks'] = "Semaine";
$string['course_format_topics'] = "Sujet";
$string['course_format_social'] = "Social";
$string['course_format_singleactivity'] = "Activité unique";
$string['plugin_requirements_title'] = "Situation:";
$string['plugin_requirements_descriptions'] = "Le plugin sera visible et affichera les rapports pour les étudiants et les enseignants lorsque les conditions suivantes sont remplies...";
$string['plugin_requirements_has_users'] = "Le cours doit avoir au moins un étudiant inscrit";
$string['plugin_requirements_course_start'] = "La date actuelle doit être supérieure à la date de début de la première semaine configurée";
$string['plugin_requirements_a_sections'] = "Les semaines configurées ont au moins une section.";
$string['plugin_visible'] = "Rapports visibles";
$string['plugin_hidden'] = "Rapports cachés";
$string['title_conditions'] = "Conditions d'utilisation";

/* Logs */
$string['logs_title'] = "Télécharger le fichier du logs (les actions effectuées par les étudiants sur Moodle) de l'étudiant";
$string['logs_description'] = "Pour télécharger le fichier du logs (les actions effectuées par les étudiants sur Moodle), vous devez cliquer sur le bouton 'Générer du logs'. Plus tard, un lien sera activé qui permettra le téléchargement du fichier.";
$string['logs_btn_download'] = "Générer fichier du logs";
$string['logs_label_creating'] = "Le fichier du logs étant généré, cela peut prendre plusieurs minutes. S'il vous plaît, attendez...";
$string['logs_generation_finished'] = "Le fichier du logs a été généré avec success.";
$string['logs_download_link'] = "Télécharger le fichier du logs";
$string['logs_th_title_column'] = "Colonne";
$string['logs_th_title_description'] = "Description";
$string['logs_tb_title_logid'] = "Identifiant d'enregistrement";
$string['logs_tb_title_userid'] = "Identifiant d'utilisateur";
$string['logs_tb_title_username'] = "Nom d'utilisateur";
$string['logs_tb_title_names'] = "Nom";
$string['logs_tb_title_lastnames'] = "Nom de famille";
$string['logs_tb_title_email'] = "E-mail";
$string['logs_tb_title_roles'] = "Rôles";
$string['logs_tb_title_courseid'] = "Identifiant du course";
$string['logs_tb_title_component'] = "Composant";
$string['logs_tb_title_action'] = "Acction";
$string['logs_tb_title_timecreated'] = "Date (Timestamp)";
$string['logs_tb_description_logid'] = "Numéro du fichier logs enregistré";
$string['logs_tb_description_userid'] = "Id de la personne qui a généré le fichier du logs";
$string['logs_tb_description_username'] = "Utilisateur enregistré dans la plate-forme de la personne qui a généré le fichier du logs";
$string['logs_tb_description_names'] = "Noms des participants au cours";
$string['logs_tb_description_lastnames'] = "Noms de famille des participants au cours";
$string['logs_tb_description_email'] = "Email des participants au cours";
$string['logs_tb_description_roles'] = "Rôle dans la plateforme de la personne qui a généré le fichier du logs";
$string['logs_tb_description_courseid'] = "Parcours à partir duquel le fichier de logs a été généré";
$string['logs_tb_description_component'] = "Composant de cours interactif";
$string['logs_tb_description_action'] = "Action qui a déclenché la création du fichier du journal";
$string['logs_tb_description_timecreated'] = "Date au format timestamp dans laquelle le fichier du logs a été généré";

/*new string*/
$string['studentreports'] = "Rapports des étudiants";

/*Menu*/
$string['binnacle'] = "Blog";
$string['report1'] = "Rapport 1";
$string['report2'] = "Rapport 2";
$string['report3'] = "Rapport 3";
$string['report4'] = "Rapport 4";
$string['report5'] = "Rapport 5";
$string['set_weeks_title'] = "Configuration des semaines de cours";
$string['set_weeks'] = "Configuration des semaines de cours";

/*Notes*/
$string['notes_btn_delete'] = "Supprimer";
$string['notes_btn_new'] = "Écarter les changements";
$string['notes_btn_notes'] = "Sauvegarder";
$string['notes_btn_update'] = "Editor";
$string['notes_notes'] = "Annotations";
$string['notes_string_new'] = "Nouveau";
$string['notes_string_update'] = "Mise à jour";
$string['notes_title_notes'] = "Annotations";
$string['notes_placeholder_title'] = "Title";
$string['notes_placeholder_search'] = "Recherche.";
$string['notes_placeholder_note'] = "Rédiger une note";
$string['notes_save_success'] = "Sauvegarder la note";
$string['notes_created'] = "Créé à la date..";
$string['notes_not_found'] = "Aucune note trouvée";
$string['notes_description'] = "Rédigez et conservez les notes que vous jugez pertinentes pour ce cours. Vous pourrez voir toutes les notes que vous avez écrites pour ce cours, comme rappels, points forts ou autres. Utilisez le moteur de recherche pour trouver une note particulière parmi celles générées à partir d'un mot-clé ou d'une phrase";
$string['notes_message_created'] = "La note a été créée";
$string['notes_message_non_créé'] = "Note n'a pas pu être créée";
$string['notes_message_deleted'] = "La note a été supprimée";
$string['notes_message_not_deleted'] = "Note n'a pas pu être supprimée";
$string['notes_message_updated'] = "La note a été mise à jour";
$string['notes_message_not_updated'] = "Note n'a pas pu être mise à jour'";

/*Metareflexion*/

$string['metareflexion_goal1'] = "Apprendre de nouvelles choses";
$string['metareflexion_goal2'] = "Revoir ce que j'ai vu";
$string['metareflexion_goal3'] = "Planifier mon apprentissage";

$string['metareflexion_no_modules_in_section'] = "Cette semaine n'as pas de modules";
$string['compare_with_course'] = "Comparer mes résultats avec ceux de mes collègues";
$string['sr_menu_metareflexion'] = "Méta-réflexion";
$string['metareflexion_last_week_created'] = "Les réflexions de la semaine dernière ont été sauvegardées";
$string['metareflexion_last_week_update'] = "Mise à jour des réflexions de la semaine dernière";
$string['metareflexion_update_planification_success'] = "Engagement actualisé";
$string['metareflexion_save_planification_success'] = "Engagement enregistré";
$string['metareflexion_tab_reflexion'] = "Statistiques sur l'efficacité";
$string['metareflexion_tab_reflexion_title'] = "Liste des ressources de cette semaine";
$string['metareflexion_tab_planification'] = "Planification de la semaine";
$string['metareflexion_tab_lastweek'] = "Réflexions de la semaine dernière";
$string['metareflexion_tab_graphic_lastweek'] = "Réflexions";
$string['metareflexion_effectiveness_title'] = "Metaréflexion - Statistiques sur l'efficacité";
$string['effectiveness_graphic_legend_pending'] = "Ressources planifiées";
$string['metareflexion_graphic_legend_unplanned'] = "Aucune ressource planifiée";
$string['effectiveness_graphic_legend_completed'] = "Toutes les ressources réalisées";
$string['effectiveness_graphic_legend_failed'] = "Certaines ressources n'ont pas été réalisées";
$string['metareflexion_cw_card_title'] = "Metaréflexion - Planification de la semaine";
$string['metareflexion_cw_description_student'] = "Dans cette section, vous pouvez planifier les horaires, vos objectifs personnels et les jours de travail du cours pour cette semaine. Ces informations seront utilisées pour calculer votre efficacité à travailler sur le cours, que vous pouvez consulter dans la section Efficacité.";
$string['metareflexion_cw_card_daysdedicate'] = "Jours que je consacrerai au cours :";
$string['metareflexion_cw_card_hoursdedicate'] = "Heures que je consacrerai au cours :";
$string['metareflexion_cw_card_msg'] = "Vous n'avez pas encore planifié le temps de travail que vous souhaitez consacrer à cette ressource";
$string['metareflexion_cw_card_btn_plan'] = "Planification de la semaine";
$string['metareflexion_cw_table_title'] = "Plannification des ressources en fonction des jours";
$string['metareflexion_cw_table_thead_activity'] = "Activité";
$string['metareflexion_cw_table_thead_type'] = "Type de ressource";
$string['metareflexion_cw_table_thead_days_committed'] = "Journées consacrées";
$string['metareflexion_cw_table_thead_hours_committed'] = "Heures consacrées";
$string['metareflexion_cw_table_thead_plan'] = "Plan";
$string['metareflexion_cw_dialog_daysdedicate'] = "Choisissez quels jours vous prévoyez de compléter les différentes ressources";
$string['metareflexion_cw_dialog_hoursdedicate'] = "Combien d'heures de travail prévoyez-vous cette semaine?";
$string['metareflexion_cw_dialog_goalsdedicate'] = "Quels objectifs vous fixez-vous pour cette semaine ?";
$string['metareflexion_cw_dialog_btn_cancel'] = "Annuler";
$string['metareflexion_cw_dialog_btn_accept'] = "Sauvegarder le plan";
$string['metareflexion_pw_title'] = "Réflexions de la semaine dernière";
$string['metareflexion_pw_description_student'] = "Dans cette section, vous pourrez indiquer combien d'heures de cours en face à face vous avez consacrées au cours et combien vous en avez pris en dehors des heures de cours. En outre, vous pourrez indiquer si les objectifs d'apprentissage de la semaine dernière ont été atteints et donner votre avis sur les avantages d'avoir suivi des cours. Ces informations seront fournies à l'enseignant de manière anonyme afin d'améliorer le cours";
$string['metareflexion_pw_classroom_hours'] = "Combien d'heures de cours en face à face avez-vous consacrées à ce cours cette semaine ?";
$string['metareflexion_pw_hours_off_course'] = "Combien d'heures de travail avez-vous consacrées au cours en dehors de la plateforme Moodle ?";
$string['metareflexion_pw_btn_save'] = "Sauvegarder";
$string['metareflexion_report_title'] = "Graphique lié au formulaire d'engagement de Planification/Méta-réflexion";
$string['metareflexion_report_description_days_teacher'] = "Dans ce graphique, vous trouverez la planification des jours que les étudiants se sont engagés à travailler (première barre) par rapport aux jours réellement travaillés (deuxième barre). Si la première barre n'est pas visible, cela signifie que vos étudiants n'ont pas planifié de jours de travail pour cette semaine ; ils ont planifié des jours de travail. Si la deuxième barre n'est pas visible, cela signifie que les étudiants n'ont pas fait le travail qu'ils s'étaient engagés à faire le jour prévu.";
$string['metareflexion_report_description_days_student'] = "Vous trouverez en dessous des objectifs une table vous indiquant si vous avez respecté le planning des modules en fonction des jours que vous vous étiez fixé. Si le délai prévu est respecté, un pouce vert sera affiché. Dans le cas où le travail prévu n'est pas réalisé, un pouce rouge sera affiché.<br> Il faut noter qu'un module est considéré comme validé quand il est consulté. Ainsi, pour valider une journée, il faut consulter tous les modules que vous avez planifié dans la section 'Planification de la semaine'.<br>";
$string['metareflexion_report_subtitle_days_student'] = "Mes engagements prévus pour cette semaine";
$string['metareflexion_report_subtitle_days_teacher'] = "Pourcentage d'élèves qui ont respecté leur plan ce jour-là";
$string['metareflexion_report_description_goals_student'] = "En dessous de ce graphique, vous trouverez un rappel des objectifs que vous vous étiez fixés la semaine dernière.";
$string['metareflexion_report_subtitle_hours_student'] = "Mon efficacité dans la semaine";
$string['metareflexion_report_subtitle_hours_teacher'] = "Efficacité moyenne de la semaine";
$string['metareflexion_report_descrition_hours_teacher'] = "Ce graphique montre les heures que les étudiants ont prévu de travailler pendant cette semaine par rapport aux heures effectivement travaillées. <br> Notez qu'il est considéré comme un jour ouvré si l'étudiant a fait une interaction avec les ressources de cette semaine. Le temps prévu est obtenu à partir de la Méta-réflexion de l'étudiant dans son Planification de la semaine.<br>";
$string['metareflexion_report_descrition_hours_student'] = "Ce graphique montre les heures que l'étudiant a prévu de travailler pendant cette semaine par rapport aux heures effectivement travaillées et la moyenne des heures travaillées par les étudiants du cours. Le temps prévu est obtenu à partir de la méta-réflexion hebdomadaire que vous avez faite dans la section 'Planification de la semaine'.";
$string['metareflexion_report_description_meta_student'] = "Enfin, vous trouverez en dernière position un questionnaire sur la semaine passée. Il faut noter que ce formulaire n'est pas disponible pour une semaine sans planification ou pour la semaine courante.";
$string['metareflexion_cw_day_lun'] = "Lundi";
$string['metareflexion_cw_day_mar'] = "Mardi";
$string['metareflexion_cw_day_mie'] = "Mercredi";
$string['metareflexion_cw_day_jue'] = "Jeudi";
$string['metareflexion_cw_day_vie'] = "Vendredi";
$string['metareflexion_cw_day_sab'] = "Samedi";
$string['metareflexion_cw_day_dom'] = "Dimanche";
$string['metareflexion_myself'] = "Mon";
$string['metareflexion_inverted_time'] = "Temps investi";
$string['metareflexion_inverted_time_course'] = "Temps moyen investi (cours)";
$string['metareflexion_planified_time'] = "Temps planifié";
$string['metareflexion_planified_time_course'] = "Temps moyen prévu (cours)";
$string['metareflexion_belonging'] = "Appartenance à";
$string['metareflexion_student'] = "Etudiants";
$string['metareflexion_interaction_user'] = "Utilisateurs qui ont interagi avec cette ressource";
$string['metareflexion_hover_title_teacher'] = "Efficacité des étudiants (moyenne)";
$string['metareflexion_hover_title_student'] = "Efficacité par semaine";
$string['metareflexion_planning_week'] = "Il/Elle eat en train de planificar";
$string['metareflexion_planning_week_start'] = ", commence le ";
$string['metareflexion_planning_week_end'] = " et se termine le ";
$string['metareflexion_report_pw_avegare_hour_clases'] = "Nombre moyen d'heures passées en cours en face à face";
$string['metareflexion_report_pw_avegare_hour_outside_clases'] = "Moyenne des heures passées en dehors des cours";
$string['metareflexion_report_pw_summary_card'] = "Durée moyenne du cours";
$string['metareflexion_report_pw_learning_objetives'] = "Concernant aux objectifs d'apprentissage";
$string['metareflexion_report_pw_attending_classes'] = "Concernant l'avantage de suivre des cours ";
$string['metareflexion_report_pw_text_graphic_no_data'] = "Pas de données pour cette semaine";
$string['metareflexion_report_pw_description_teacher'] = "Dans ces graphiques, vous pouvez voir les informations obtenues des étudiants concernant leurs impressions sur les cours de la semaine précédente.";
$string['metareflexion_weekly_planning_subtitle'] = "Voir les résultats de votre planification hebdomadaire sous l'onglet 'Efficacité par semaine'.";
$string['metareflexion_saved_form'] = "Vous avez déjà rempli le formulaire sur Méta-réflexion cette semaine";
$string['metareflexion_efficiency_title'] = "Votre efficacité";
$string['metareflexion_goals_title'] = "Choix des objectifs personnels";
$string['metareflexion_pres_question_time_invested_outside'] = "Combien de temps ai-je investi pour travailler en-dehors de la plateforme Moodle ?";
$string['metareflexion_pres_question_objectives_reached'] = "Ai-je atteint mes objectifs ?";
$string['metareflexion_pres_question_pres_question_feel'] = "Quel est mon avis sur cette semaine ?";
$string['metareflexion_pres_question_learn_and_improvement'] = "Qu'ai-je appris et comment puis-je m'améliorer ?";
$string['metareflexion_goals_reflexion_title'] = "Les objectifs choisis pour cette semaine";
$string['metareflexion_title_hours_plan'] = "Prévision du temps de travail pour la semaine courante";
$string['metareflexion_title_retrospective'] = "Retrospective de la semaine";
$string['metareflexion_title_metareflexion'] = "Planning";
/* Reports */
$string['student_reports:view_report_as_teacher'] = "Voir le rapport en tant qu'enseignant";
$string['student_reports:view_report_as_student'] = "Voir le rapport en tant qu'étudiant";
$string['student_reports:time_visualizations'] = "Travail sur la plate-forme";
$string['student_reports:time_worked_session_report'] = "Rapport sur le 'Travail sur la plate-forme'";
$string['student_reports:write'] = "Sauvegarder les modifications apportées aux rapports";

/* time_worked_session_report */
$string['twsr_title'] = "Sessions d'étude par semaine";
$string['twsr_description_student'] = "Une session correspond au temps consacré à l'interaction au sein de la plate-forme et au temps moyen des sessions respectives. Vous trouverez ci-dessous la liste des semaines avec le nombre total de sessions. En déplaçant le curseur sur le graphique, vous pouvez trouver des informations détaillées sur le groupe d'étudiants dans le cours.";
$string['twsr_description_teacher'] = "Une session correspond au temps pendant lequel les étudiants ont interagi avec le matériel de cours de Moodle. Dans ce rapport, vous trouverez la liste des semaines avec le nombre total de sessions effectuées par les étudiants du cours. En plaçant votre curseur sur l'une des barres, vous pouvez voir le nombre total de sessions, la durée moyenne des sessions et le nombre d'étudiants différents qui ont participé à chaque session.";
$string['twsr_axis_x'] = "Nombre de sessions";
$string['twsr_axis_y'] = "Semaines de cours";
$string['twsr_student'] = "Étudiant";
$string['twsr_students'] = "Étudiants";
$string['twsr_sessions_by'] = "Les sessions appartenant à";
$string['twsr_sessions_middle_time'] = "Duré moyenne des sessions";
$string['twsr_total_sessions'] = "Total des sessions";

/* time_visualizations */
$string['tv_title'] = "Temps passé (par heure)";
$string['tv_lunes'] = "Lundi";
$string['tv_martes'] = "Mardi";
$string['tv_miercoles'] = "Mercredi";
$string['tv_jueves'] = "Jeudi";
$string['tv_viernes'] = "Vendredi";
$string['tv_sabado'] = "Samedi";
$string['tv_domingo'] = "Dimange";
$string['tv_axis_x'] = "Jours de la semaine";
$string['tv_axis_y'] = "Heures de la journée";
$string['tv_url'] = "URL";
$string['tv_resource_document'] = "Document";
$string['tv_resource_image'] = "Image";
$string['tv_resource_audio'] = "Audio";
$string['tv_resource_video'] = "Video";
$string['tv_resource_file'] = "Fichier";
$string['tv_resource_script'] = "Script/Code";
$string['tv_resource_text'] = "Texte";
$string['tv_resource_download'] = "Téléchargements";
$string['tv_assign'] = "Tâche";
$string['tv_assignment'] = "Tache";
$string['tv_book'] = "Livre";
$string['tv_choice'] = "Selection";
$string['tv_feedback'] = "Retour";
$string['tv_folder'] = "Dossier";
$string['tv_forum'] = "Forum";
$string['tv_glossary'] = "Glossaire";
$string['tv_label'] = "Étiquette";
$string['tv_lesson'] = "Leçon";
$string['tv_page'] = "Page";
$string['tv_quiz'] = "Quiz";
$string['tv_survey'] = "Enquête";
$string['tv_lti'] = "Outil externe";
$string['tv_other'] = "Un autre";
$string['tv_interaction'] = "Interaction par";
$string['tv_interactions'] = "Interaction par";
$string['tv_course_module'] = "Module";
$string['tv_course_modules'] = "Module";
$string['tv_student'] = "Étudiant";
$string['tv_students'] = "Étudiants";
$string['tv_average'] = "Interactions moyennes";
$string['tv_change_timezone'] = "Fuseau horaire:";
$string['tv_activity_inside_plataform_student'] = "Mon activité sur la plateforme";
$string['tv_activity_inside_plataform_teacher'] = "Activité des élèves sur la plateforme";
$string['tv_time_inside_plataform_student'] = "Mon temps sur la plateforme";
$string['tv_time_inside_plataform_teacher'] = "Temps moyen passé par les étudiants sur la plateforme cette semaine";
$string['tv_time_inside_plataform_description_teacher'] = "Le temps que l'étudiant a investi dans la semaine choisie, comparé au temps que l'enseignant a prévu d'investir. Le temps passé affiché correspond à la moyenne de tous les étudiants. Le temps prévu par l'enseignant est le temps attribué dans <i>Planification de la semaine</i> par l'enseignant.";
$string['tv_time_inside_plataform_description_student'] = "Temps passé cette semaine par rapport au temps que l'enseignant avait prévu de passer.";
$string['tv_activity_inside_plataform_description_teacher'] = "L'axe des Y indique les heures de la journée et l'axe des X les jours de la semaine. Dans le graphique, vous pouvez trouver plusieurs points qui, lorsque vous passez le curseur dessus, fournissent des informations détaillées sur les interactions des élèves, regroupées par type de ressource (nombre d'interactions, nombre d'élèves ayant interagi avec la ressource et nombre moyen d'interactions). <b>En cliquant sur les étiquettes, vous pouvez filtrer par type de ressources, en ne laissant visibles que celles qui ne sont pas barrées</b>";
$string['tv_activity_inside_plataform_description_student'] = "Ce graphique montre les interactions par type de ressource et par temps. Lorsque vous déplacez votre curseur sur un point visible du graphique, vous verrez le nombre d'interactions regroupées par type de ressource. En cliquant sur les étiquettes, vous pouvez filtrer par type de ressource.";
$string['tv_to'] = "dans le";
$string['tv_time_spend'] = "Temps passé";
$string['tv_time_spend_teacher'] = "Temps moyen passé";
$string['tv_time_should_spend'] = "Le temps qu'il faut consacrer";
$string['tv_time_should_spend_teacher'] = "Temps moyen que vous devriez passer'";

/* activities_performed */
$string['ap_title'] = "Activités réalisées";
$string['ap_description_student'] = "Ce rapport présente les activités réalisées et en cours cette semaine. La barre de progression de la semaine correspond au pourcentage d'activités à réaliser cette semaine et prévues par l'enseignant. L'option Semaine vous permet de choisir et de visualiser le rapport correspondant à la semaine choisie. Les activités sont regroupées par type de ressources. Les activités marquées en vert sont celles qui ont été réalisées et celles marquées en rouge sont celles qui sont en cours. Pour qu'une activité soit considérée comme terminée, vous devez avoir interagi au moins une fois avec la ressource. <br/> <br/> En cliquant sur les étiquettes terminées ou en attente, vous pourrez filtrer les informations.";
$string['ap_description_teacher'] = "Text en attente";
$string['ap_axis_x'] = "Type de ressources";
$string['ap_axis_y'] = "Montant des ressources";
$string['ap_progress'] = "Progrès de la semaine";
$string['ap_to'] = "dans la";
$string['ap_week'] = "Semaine";
$string['ap_activities'] = "Durée moyenne d'interaction pour resource";

/* activities_performed teacher */
$string['apt_title'] = "Activités de cours effectuées";
$string['apt_description'] = "Ce rapport présente les activités disponibles du cours. L'option Semaine vous permet de choisir et de visualiser le rapport de cette semaine. En déplaçant le curseur sur l'une des barres, vous trouverez la moyenne des vues et des interactions. Si vous cliquez sur la barre, vous trouverez plus de détails (nom de l'élève, courriel, interactions, temps moyen de visualisation).";
$string['apt_axis_x'] = "Interactions";
$string['apt_axis_y'] = "Ressources";
$string['apt_visualization_average'] = "Affichages moyens";
$string['apt_interacctions_average'] = "Moyenne des interactions";
$string['apt_thead_name'] = "Nom";
$string['apt_thead_lastname'] = "Nom de famille";
$string['apt_thead_email'] = "Email";
$string['apt_thead_interactions'] = "Interactions";
$string['apt_thead_visualization'] = "Durée moyenne d'interaction pour resource";

/* Composante de la pagination */
$string['pagination_component_to'] = "dans la";
$string['pagination_component_name'] = "Semaine";

/* Questions et réponses */
$string['question_number_one'] = "Avez-vous atteint les objectifs d'apprentissage la semaine dernière ?";
$string['answers_number_one_option_one'] = "Oui";
$string['answers_number_one_option_two'] = "Non";


$string['question_number_two'] = "Quel est le résultat de votre apprentissage ?";
$string['answers_number_two_option_one'] = "J'ai une idée précise du ou des sujets et je sais comment l'appliquer";
$string['answers_number_two_option_two'] = "J'ai une idée précise du ou des sujets et non de la façon de les appliquer.";
$string['answers_number_two_option_three'] = "Je n'ai pas d'idée précise sur le(s) sujet(s).";

$string['question_number_three'] = " A quel point participer aux cours en face à face était-il utile pour vous ?";
$string['answers_number_three_option_one'] = "Rien d'utile";
$string['answers_number_three_option_two'] = "Plutôt utile";
$string['answers_number_three_option_three'] = "Utile";
$string['answers_number_three_option_four'] = "Tres utile";
$string['answers_number_three_option_five'] = "Vraiment utile";
$string['answers_number_three_option_six'] = "Je ne suis pas allé en classe";

$string['question_number_four'] = "Quel est votre ressenti par rapport à la semaine passée ?";
$string['answers_number_four_option_one'] = "Je ne suis pas satisfait de la façon dont je me suis organisé pour atteindre mes objectifs";
$string['answers_number_four_option_two'] = "Je suis assez satisfait de la façon dont je me suis organisé pour atteindre mes objectifs";
$string['answers_number_four_option_three'] = "Je suis satisfait de la façon dont je me suis organisé pour atteindre mes objectifs";
$string['answers_number_four_option_four'] = "Je suis très satisfait de la façon dont je me suis organisé pour atteindre mes objectifs";

/* Auto-evaluation */
$string['qst1'] = "Je me fixe des objectifs de réussite pour mon travail personnel.";
$string['qst2'] = "Je fixe des objectifs à court terme (quotidiens ou hebdomadaires) ainsi que des objectifs à long terme (mensuels ou semestriels).";
$string['qst3'] = "Je suis exigent avec mon propre apprentissage et je m'impose des critères d'exigence.";
$string['qst4'] = "Je me fixe des objectifs pour m'aider à gérer mon travail personnel.";
$string['qst5'] = "Je ne réduis pas la qualité de mon travail quand il est en ligne.";
$string['qst6'] = "Je choisis l'endroit où j'étudie pour éviter trop de distractions.";
$string['qst7'] = "Je choisis un endroit confortable pour étudier.";
$string['qst8'] = "Je sais où je peux étudier plus efficacement.";
$string['qst9'] = "Je choisis un moment avec peu de distractions pour travailler.";
$string['qst10'] = "J'essaie de prendre des cours en ligne de formulation de notes approfondies, car des notes sont encore plus importants pour apprendre en ligne que dans une salle de classe normale.";
$string['qst11'] = "Je lis mes documents à voix haute pour lutter contre les distractions.";
$string['qst12'] = "Je prépare mes questions avant de rejoindre la salle de discussion et la discussion.";
$string['qst13'] = "Je travaille sur des problèmes supplémentaires en plus de ceux qui me sont attribués pour maîtriser le contenu du cours.";
$string['qst14'] = "Je me garde du temps pour travailler en dehors des cours.";
$string['qst15'] = "J'essaie de planifier le même moment chaque jour ou chaque semaine pour étudier, et je respecte cet horaire.";
$string['qst16'] = "Bien que nous n'ayons pas cours tous les jours, j'essaie de répartir mon temps de travail uniformément sur la semaine.";
$string['qst17'] = "Je trouve un étudiant qui maitrise le cours afin de pouvoir le consulter quand j'ai besoin d'aide.";
$string['qst18'] = "Je partage mes problèmes avec mes camarades afin que nous puissions résoudre ces problèmes de manière collective.";
$string['qst19'] = "Si nécessaire, j'essaie de rencontrer mes camarades de classe en face à face.";
$string['qst20'] = "Je n'hésite pas à demander de l'aide de l'enseignant par courrier électronique.";
$string['qst21'] = "Je fais des résumés de mes cours afin d'être conscient de ce que j'ai compris.";
$string['qst22'] = "Je me pose beaucoup de questions sur le contenu du cours que j'étudie.";
$string['qst23'] = "Je communique avec mes camarades de classe pour savoir comment je me situe par rapport à eux.";
$string['qst24'] = "Je communique avec mes camarades de classe pour découvrir que ce que j'apprends est différent de ce qu'ils apprennent.";

$string['btn_save_auto_eval'] = "Sauvegarder";
$string['message_save_successful'] = "Auto-évaluation sauvegardée avec succès";
$string['message_update_successful'] = "Auto-évaluation mise à jour avec succès";
$string['page_title_auto_eval'] = "Auto-évaluation";
$string['form_questions_auto_eval'] = "Questions d'auto-évaluation";
$string['description_auto_eval'] = "Cette page permet d'auto-évaluer ses méthodes pour étudier. Ce questionnaire est propre à chaque étudiant mais commun à tous les cours sur Moodle. Une fois que vous l'avez rempli, vous pouvez sauvegarder vos réponses avec le bouton en bas de page ainsi que modifier vos choix quand vous le souhaitez.";

/* Groupes */
$string['group_allstudent'] = "Tous les étudiants";


/* Admin Settings */
$string['nmp_use_navbar_menu'] = 'Activer le nouveau menu';
$string['nmp_use_navbar_menu_desc'] = 'Afficher le menu du plugin dans la barre de navigation supérieure, sinon il inclura le menu dans le bloc de navigation.';

/* Gamification */
$string['tg_tab1'] = "Répartition";
$string['tg_tab2'] = "Classement";
$string['tg_tab3'] = "Mon profil";
$string['EnableGame'] = "Désactiver/Activer :";
$string['studentRanking1'] = "Voulez vous apparaitre dans le ranking board ? ";
$string['studentRanking2'] = "/!\ Tout les inscrits à ce cours ayant accepté vous verrons. Accepter est définitif:";
$string['studentRanking3'] = "Accepter";
$string['gamification_denied'] = 'La gamification a été désactivée par votre professeur';
$string['tg_colon'] = '{$a->a}: {$a->b}';
$string['tg_section_title'] = 'Gamification :';
$string['tg_section_help_title'] = 'Paramètres de gamification des cours';
$string['tg_section_help_description'] = 'Vous pouvez donner la possibilité de gamification aux étudiants du cours.';
$string['tg_save_warning_title'] = "Êtes-vous sûr de vouloir enregistrer les modifications?";
$string['tg_save_warning_content'] = "Si vous modifiez les paramètres de la gamification au début du cours, les données peuvent être perdues ...";
$string['tg_confirm_ok'] = "Sauver";
$string['tg_confirm_cancel'] = "Annuler";
$string['tg_save'] = "Enregistrer la configuration";
$string['tg_section_preview'] = 'Point de vue étudiant';
$string['tg_section_experience'] = 'Points d\'experience';
$string['tg_section_information'] = 'Information';
$string['tg_section_ranking'] = 'Classement / rapport';
$string['tg_section_settings'] = 'Réglages';

$string['tg_section_points'] = 'points';
$string['tg_section_description'] = 'Description';
$string['tg_section_no_description'] = 'Pas de description';
$string['tg_section_no_name'] = 'Sans nom';

$string['tg_section_preview_next_level'] = 'au niveau supérieur';
$string['tg_section_ranking_ranking_text'] = 'Classement';
$string['tg_section_ranking_level'] = 'Niveau';
$string['tg_section_ranking_student'] = 'Élève';
$string['tg_section_ranking_total'] = 'Total';
$string['tg_section_ranking_progress'] = 'Progréssion %';

$string['tg_section_settings_appearance'] = 'Apparence';
$string['tg_section_settings_appearance_title'] = 'Titre';
$string['tg_section_settings_levels'] = 'Paramètres de niveau';
$string['tg_section_settings_levels_quantity'] = 'Niveaux';
$string['tg_section_settings_levels_base_points'] = 'Points de base';
$string['tg_section_settings_levels_calculated'] = 'Automatique';
$string['tg_section_settings_levels_manually'] = 'Manuellement';
$string['tg_section_settings_levels_name'] = 'Nom';
$string['tg_section_settings_levels_required'] = 'Points requis';
$string['tg_section_settings_rules'] = 'Liste des régles';
$string['tg_section_settings_add_rule'] = 'Nouvelle règle';
$string['tg_section_settings_earn'] = 'Pour cet événement, victoire:';
$string['tg_section_settings_select_event'] = 'Sélectionnez un événement';
$string['gm_Chart_Title'] = 'Graphique de répartion';
$string['gm_Chart_Y'] = 'Etudiants';

$string['tg_timenow'] = 'Juste maintenant';
$string['tg_timeseconds'] = 'il y a {$a}s ';
$string['tg_timeminutes'] = 'il y a {$a}m ';
$string['tg_timehours'] = 'il y a {$a}h ';
$string['tg_timedays'] = 'il y a {$a}d ';
$string['tg_timeweeks'] = 'il y a {$a}w ';
$string['tg_timewithinayearformat'] = '%b %e';
$string['tg_timeolderyearformat'] = '%b %Y';

/* General Errors */
$string['nmp_api_error_network'] = "Une erreur s'est produite dans la communication avec le serveur.";
$string['nmp_api_invalid_data'] = 'Données incorrectes';
$string['nmp_api_json_decode_error'] = 'Décodage d erreur de données envoyées';
$string['nmp_api_invalid_token_error'] = 'Le jeton envoyé dans la transaction n\'est pas valide, veuillez actualiser la page';
$string['nmp_api_invalid_transaction'] = 'La demande est incorrecte';
$string['nmp_api_invalid_profile'] = 'Vous ne pouvez pas faire cette action, votre profil est incorrect';
$string['nmp_api_save_successful'] = 'Les données ont été enregistrées avec succès sur le serveur';
$string['nmp_api_cancel_action'] = 'Vous avez annulé l\'action';
$string['overview'] = 'Vue d\'ensemble : ';
$string['game_point_error'] = 'Veuillez saisir les points';
$string['game_event_error'] = 'Veuillez saisir l\'évènement';
$string['game_name_error'] = 'Le nom est requis';
