-<?php
global $COURSE, $USER;
require_once('locallib.php');



$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

$context = context_course::instance($course->id);
require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_student', $context);
require_capability('local/notemyprogress:student_sessions', $context);

$url = '/local/student_reports/metareflexion.php';
local_notemyprogress_set_page($course, $url);

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs->addLogsNMP("viewed", "section", "STUDENT_STUDY_SESSIONS", "student_study_sessions", $actualLink, "Section where you can consult various indicators on the study sessions carried out by the student");

$reports = new \local_notemyprogress\student($COURSE->id, $USER->id);

$metareflexion = new \local_notemyprogress\metareflexion($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => array(
        "inverted_time_chart_title" => get_string("nmp_student_time_inverted_title", "local_notemyprogress"),
        "inverted_time_chart_x_axis" => get_string("nmp_student_time_inverted_x_axis", "local_notemyprogress"),
        "inverted_time" => get_string("nmp_student_inverted_time", "local_notemyprogress"),
        "expected_time" => get_string("nmp_student_expected_time", "local_notemyprogress"),

        "resource_access_title" => get_string("nmp_resource_access_title", "local_notemyprogress"),
        "resource_access_x_axis" => get_string("nmp_resource_access_x_axis", "local_notemyprogress"),
        "resource_access_y_axis" => get_string("nmp_resource_access_y_axis", "local_notemyprogress"),
        "resource_access_legend1" => get_string("nmp_resource_access_legend1", "local_notemyprogress"),
        "resource_access_legend2" => get_string("nmp_resource_access_legend2", "local_notemyprogress"),

        "hours_sessions_title" => get_string("nmp_hours_sessions_title", "local_notemyprogress"),
        "week_progress_title" => get_string("nmp_week_progress_title", "local_notemyprogress"),

        "session_text" => get_string("nmp_session_text", "local_notemyprogress"),
        "sessions_text" => get_string("nmp_sessions_text", "local_notemyprogress"),
        "modules_details" => get_string("nmp_modules_details", "local_notemyprogress"),

        "hours_short" => get_string("nmp_hours_short", "local_notemyprogress"),
        "minutes_short" => get_string("nmp_minutes_short", "local_notemyprogress"),
        "seconds_short" => get_string("nmp_seconds_short", "local_notemyprogress"),

        "modules_access_chart_title" => get_string("nmp_modules_access_chart_title", "local_notemyprogress"),
        "modules_viewed" => get_string("nmp_modules_viewed", "local_notemyprogress"),
        "modules_no_viewed" => get_string("nmp_modules_no_viewed", "local_notemyprogress"),
        "modules_complete" => get_string("nmp_modules_complete", "local_notemyprogress"),
        "modules_interaction" => get_string("nmp_modules_interaction", "local_notemyprogress"),
        "modules_interactions" => get_string("nmp_modules_interactions", "local_notemyprogress"),
        "close_button" => get_string("nmp_close_button", "local_notemyprogress"),

        'hours_unit_time_label' => get_string('hours_unit_time_label', 'local_notemyprogress'),
        'graph_generating' => get_string('graph_generating', 'local_notemyprogress'),
        'last_week_update' => get_string('metareflexion_last_week_update', 'local_notemyprogress'),
        'last_week_created' => get_string('metareflexion_last_week_created', 'local_notemyprogress'),
        'update_planification_success' => get_string('metareflexion_update_planification_success', 'local_notemyprogress'),
        'api_error_network' => get_string('api_error_network', 'local_notemyprogress'),
        'save_planification_success' => get_string('metareflexion_save_planification_success', 'local_notemyprogress'),
        'tab_1' => get_string('metareflexion_tab_planification',  'local_notemyprogress'),
        'tab_2' => get_string('metareflexion_tab_reflexion', 'local_notemyprogress'),
        'title_reports' => get_string('metareflexion_report_title', 'local_notemyprogress'),
        'subtitle_reports_days_student' => get_string('metareflexion_report_subtitle_days_student', 'local_notemyprogress'),
        'subtitle_reports_days_teacher' => get_string('metareflexion_report_subtitle_days_teacher', 'local_notemyprogress'),
        'description_reports_goals_student' => get_string('metareflexion_report_description_goals_student', 'local_notemyprogress'),
        'description_reports_days_student' => get_string('metareflexion_report_description_days_student', 'local_notemyprogress'),
        'description_reports_days_teacher' => get_string('metareflexion_report_description_days_teacher', 'local_notemyprogress'),
        'subtitle_reports_hours_student' => get_string('metareflexion_report_subtitle_hours_student', 'local_notemyprogress'),
        'subtitle_reports_hours_teacher' => get_string('metareflexion_report_subtitle_hours_teacher', 'local_notemyprogress'),
        'description_reports_hours_teacher' => get_string('metareflexion_report_descrition_hours_teacher', 'local_notemyprogress'),
        'description_reports_hours_student' => get_string('metareflexion_report_descrition_hours_student', 'local_notemyprogress'),
        'description_reports_meta_student' => get_string('metareflexion_report_description_meta_student', 'local_notemyprogress'),
        'effectiveness_title' => get_string('metareflexion_effectiveness_title',  'local_notemyprogress'),
        'effectiveness_graphic_legend_unplanned' => get_string('metareflexion_graphic_legend_unplanned',  'local_notemyprogress'),
        'effectiveness_graphic_legend_pending' => get_string('effectiveness_graphic_legend_pending',  'local_notemyprogress'),
        'effectiveness_graphic_legend_completed' => get_string('effectiveness_graphic_legend_completed',  'local_notemyprogress'),
        'effectiveness_graphic_legend_failed' => get_string('effectiveness_graphic_legend_failed',  'local_notemyprogress'),
        'currentweek_title' => get_string('metareflexion_tab_reflexion_title',  'local_notemyprogress'),
        'currentweek_description_student' => get_string('metareflexion_cw_description_student',  'local_notemyprogress'),
        'currentweek_card_title' => get_string('metareflexion_cw_card_title',  'local_notemyprogress'),
        'currentweek_card_daysdedicate' => get_string('metareflexion_cw_card_daysdedicate',  'local_notemyprogress'),
        'currentweek_card_hoursdedicate' => get_string('metareflexion_cw_card_hoursdedicate',  'local_notemyprogress'),
        'currentweek_card_goalsdedicate' => get_string('metareflexion_cw_dialog_goalsdedicate',  'local_notemyprogress'),
        'currentweek_card_msg' => get_string('metareflexion_cw_card_msg',  'local_notemyprogress'),
        'currentweek_card_btn_plan' => get_string('metareflexion_cw_card_btn_plan',  'local_notemyprogress'),
        'currentweek_table_title' => get_string('metareflexion_cw_table_title',  'local_notemyprogress'),
        'currentweek_table_thead_activity' => get_string('metareflexion_cw_table_thead_activity',  'local_notemyprogress'),
        'currentweek_table_thead_type' => get_string('metareflexion_cw_table_thead_type',  'local_notemyprogress'),
        'currentweek_table_thead_days_committed' => get_string('metareflexion_cw_table_thead_days_committed',  'local_notemyprogress'),
        'currentweek_table_thead_hours_committed' => get_string('metareflexion_cw_table_thead_hours_committed',  'local_notemyprogress'),
        'currentweek_table_thead_plan' => get_string('metareflexion_cw_table_thead_plan',  'local_notemyprogress'),
        'currentweek_dialog_daysdedicate' => get_string('metareflexion_cw_dialog_daysdedicate',  'local_notemyprogress'),
        'currentweek_dialog_hoursdedicate' => get_string('metareflexion_cw_dialog_hoursdedicate',  'local_notemyprogress'),
        'currentweek_dialog_btn_cancel' => get_string('metareflexion_cw_dialog_btn_cancel',  'local_notemyprogress'),
        'currentweek_dialog_btn_accept' => get_string('metareflexion_cw_dialog_btn_accept',  'local_notemyprogress'),
        'currentweek_day_lun' => get_string('metareflexion_cw_day_lun',  'local_notemyprogress'),
        'currentweek_day_mar' => get_string('metareflexion_cw_day_mar',  'local_notemyprogress'),
        'currentweek_day_mie' => get_string('metareflexion_cw_day_mie',  'local_notemyprogress'),
        'currentweek_day_jue' => get_string('metareflexion_cw_day_jue',  'local_notemyprogress'),
        'currentweek_day_vie' => get_string('metareflexion_cw_day_vie',  'local_notemyprogress'),
        'currentweek_day_sab' => get_string('metareflexion_cw_day_sab',  'local_notemyprogress'),
        'currentweek_day_dom' => get_string('metareflexion_cw_day_dom',  'local_notemyprogress'),
        'pastweek_title' => get_string('metareflexion_pw_title',  'local_notemyprogress'),
        'pastweek_description_student' => get_string('metareflexion_pw_description_student',  'local_notemyprogress'),
        'pastweek_classroom_hours' => get_string('metareflexion_pw_classroom_hours',  'local_notemyprogress'),
        'pastweek_hours_off_course' => get_string('metareflexion_pw_hours_off_course',  'local_notemyprogress'),
        'pastweek_btn_save' => get_string('metareflexion_pw_btn_save',  'local_notemyprogress'),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "tv_to" => get_string("tv_to", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name", "local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to", "local_notemyprogress"),
        "tv_resource_document" => get_string("tv_resource_document", "local_notemyprogress"),
        "tv_resource_image" => get_string("tv_resource_image", "local_notemyprogress"),
        "tv_resource_audio" => get_string("tv_resource_audio", "local_notemyprogress"),
        "tv_resource_video" => get_string("tv_resource_video", "local_notemyprogress"),
        "tv_resource_file" => get_string("tv_resource_file", "local_notemyprogress"),
        "tv_resource_script" => get_string("tv_resource_script", "local_notemyprogress"),
        "tv_resource_text" => get_string("tv_resource_text", "local_notemyprogress"),
        "tv_resource_download" => get_string("tv_resource_download", "local_notemyprogress"),
        "tv_assign" => get_string("tv_assign", "local_notemyprogress"),
        "tv_assignment" => get_string("tv_assignment", "local_notemyprogress"),
        "tv_book" => get_string("tv_book", "local_notemyprogress"),
        "tv_choice" => get_string("tv_choice", "local_notemyprogress"),
        "tv_feedback" => get_string("tv_feedback", "local_notemyprogress"),
        "tv_folder" => get_string("tv_folder", "local_notemyprogress"),
        "tv_forum" => get_string("tv_forum", "local_notemyprogress"),
        "tv_glossary" => get_string("tv_glossary", "local_notemyprogress"),
        "tv_label" => get_string("tv_label", "local_notemyprogress"),
        "tv_lesson" => get_string("tv_lesson", "local_notemyprogress"),
        "tv_page" => get_string("tv_page", "local_notemyprogress"),
        "tv_quiz" => get_string("tv_quiz", "local_notemyprogress"),
        "tv_survey" => get_string("tv_survey", "local_notemyprogress"),
        "tv_lti" => get_string("tv_lti", "local_notemyprogress"),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),
        "pagination_title" => get_string("pagination_title", "local_notemyprogress"),
        "compare_with_course" => get_string("compare_with_course", "local_notemyprogress"),
        "myself" => get_string("metareflexion_myself", "local_notemyprogress"),
        "inverted_time" => get_string("metareflexion_inverted_time", "local_notemyprogress"),
        "inverted_time_course" => get_string("metareflexion_inverted_time_course", "local_notemyprogress"),
        "planified_time" => get_string("metareflexion_planified_time", "local_notemyprogress"),
        "planified_time_course" => get_string("metareflexion_planified_time_course", "local_notemyprogress"),
        "belonging" => get_string("metareflexion_belonging", "local_notemyprogress"),
        "student" => get_string("metareflexion_student", "local_notemyprogress"),
        "interaction_user" => get_string("metareflexion_interaction_user", "local_notemyprogress"),
        "hover_title_teacher" => get_string("metareflexion_hover_title_teacher", "local_notemyprogress"),
        "hover_title_student" => get_string("metareflexion_hover_title_student", "local_notemyprogress"),
        "planning_week" => get_string("metareflexion_planning_week", "local_notemyprogress"),
        "planning_week_start" => get_string("metareflexion_planning_week_start", "local_notemyprogress"),
        "planning_week_end" => get_string("metareflexion_planning_week_end", "local_notemyprogress"),
        "pastweek_avegare_hour_clases" => get_string("metareflexion_report_pw_avegare_hour_clases", "local_notemyprogress"),
        "pastweek_avegare_hour_outside_clases" => get_string("metareflexion_report_pw_avegare_hour_outside_clases", "local_notemyprogress"),
        "pastweek_summary_card" => get_string("metareflexion_report_pw_summary_card", "local_notemyprogress"),
        "pastweek_learning_objetives" => get_string("metareflexion_report_pw_learning_objetives", "local_notemyprogress"),
        "pastweek_attending_classes" => get_string("metareflexion_report_pw_attending_classes", "local_notemyprogress"),
        "pastweek_text_graphic_no_data" => get_string("metareflexion_report_pw_text_graphic_no_data", "local_notemyprogress"),
        "pastweek_description_teacher" => get_string("metareflexion_report_pw_description_teacher", "local_notemyprogress"),
        "weekly_planning_subtitle" => get_string("metareflexion_weekly_planning_subtitle", "local_notemyprogress"),
        "saved_form" => get_string("metareflexion_saved_form", "local_notemyprogress"),
        "goals_title" => get_string("metareflexion_goals_title", "local_notemyprogress"),
        "pres_question_time_invested_outside" => get_string("metareflexion_pres_question_time_invested_outside", "local_notemyprogress"),
        "pres_question_objectives_reached" => get_string("metareflexion_pres_question_objectives_reached", "local_notemyprogress"),
        "pres_question_feel" => get_string("metareflexion_pres_question_pres_question_feel", "local_notemyprogress"),
        "pres_question_learn_and_improvement" => get_string("metareflexion_pres_question_learn_and_improvement", "local_notemyprogress"),
        "goals_reflexion_title" => get_string("metareflexion_goals_reflexion_title", "local_notemyprogress"),
        "title_hours_plan" => get_string("metareflexion_title_hours_plan", "local_notemyprogress"),
        "title_retrospective" => get_string("metareflexion_title_retrospective", "local_notemyprogress"),
        "title_metareflexion"=> get_string("metareflexion_title_metareflexion", "local_notemyprogress"),
        "goal1" => get_string("metareflexion_goal1", "local_notemyprogress"),
        "goal2" => get_string("metareflexion_goal2", "local_notemyprogress"),
        "goal3" => get_string("metareflexion_goal3", "local_notemyprogress"),

    ),
    //'indicators' => $reports->get_general_indicators(),
    
    //general informations
    'userid' => $USER->id,
    'courseid' => $courseid,
    'module_groups' => $reports->get_modules_completion(),
    'indicators' => $reports->get_sessions(),
    
    //planning/schedule  
    'cmcurrentweeknew' => $metareflexion->find_current_week_new($reports),

    'week_schedule' => $reports->get_week_schedule(),
    'data_report_goals' => $reports->goals_report_metereflexion(),
    'data_report_days' => $reports->days_report_metereflexion(),
    'data_report_hours' => $reports->hours_report_metereflexion(),
    'status_planning' => $reports->status_planning(),
    
    //metareflexion/last_week
    'lastweek' => $metareflexion->find_last_week(),
    'data_report_hours_questions' => $reports->questions_report_metereflexion(),

    //other
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    //'groups' => local_notemyprogress_get_groups($course, $USER),

    'image_no_data' => "{$CFG->wwwroot}/local/notemyprogress/img/empty_char.png",
    'calendar_icon' => "{$CFG->wwwroot}/local/notemyprogress/img/calendar.png",
    'inverted_time_colors' => array('#118AB2', '#118AB2', '#06D6A0'),
    'sessions_count_colors' => array('#FFD166', '#06D6A0', '#118AB2'),
    
    //Classroom UNUSED
    //'data_report_classroom' => $reports->classroom_report_metareflexion(),
    //'data_report_of_classroom' => $reports->classroom_report_metareflexion(),
    //'report_last_week' => $reports->report_last_week(),
];
//if (!has_capability('local/notemyprogress:view_as_teacher', $context)) {
//$teacher = new \local_notemyprogress\teacher($COURSE->id, $USER->id);
$content['course_report_hours'] = $reports->hours_report_metereflexion();
//}
$PAGE->requires->js_call_amd('local_notemyprogress/metareflexion', 'init', ['content' => $content]);

echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/metareflexion', ['content' => $content]);
echo $OUTPUT->footer();
