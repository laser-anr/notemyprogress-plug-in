define([
  "local_notemyprogress/vue",
  "local_notemyprogress/vuetify",
  "local_notemyprogress/axios",
  "local_notemyprogress/alertify",
  "local_notemyprogress/pageheader",
  "local_notemyprogress/chartdynamic",
], function (Vue, Vuetify, Axios, Alertify, PageHeader, ChartDynamic) {
  "use strict";

  function init(content) {
    //console.log(content);
    Vue.use(Vuetify);
    Vue.component("pageheader", PageHeader);
    Vue.component("chart", ChartDynamic);
    const app = new Vue({
      delimiters: ["[[", "]]"],
      el: "#gamification",
      vuetify: new Vuetify(),
      data: {
        strings: content.strings,
        token: content.token,
        render_has: content.profile_render,
        ranking: content.ranking,
        notifications: [],
        loading: false,
        tab: null,
        levelsData: content.levels_data.levelsdata,
        settings: content.levels_data.settings,
        rulesData: content.levels_data.rules,
        courseid: content.levels_data.courseid,
        userid: content.levels_data.created_by,
        enable: false,
        events: content.events,
        setPointsOption: "calculated",
        pointsBase: 0,
        pointsBaseOri: 0,
        swDisableEnable: content.strings.swValue,
        spreadData: [],
        week_resources_categories: [],
        week_resources_data: [],
        week_resources_colors: "#FA4641",
        indicators: content.indicators,
        chartdata: content.chart_data,
      },

      beforeMount() {},
      mounted() {
        this.pointsBase = +this.settings.pointsbase;
        this.pointsBaseOri = +this.settings.pointsbase;
        setTimeout(function () {
          app.setGraphicsEventListeners();
        }, 500);
        app.setGraphicsEventListeners();
      },
      computed: {},
      methods: {
        get_help_content() {
          let help_contents = [];
          let help = new Object();
          help.title = this.strings.help_title;
          help.description = this.strings.help_description;
          help_contents.push(help);
          return help_contents;
        },
        update_dialog(value) {
          this.dialog = value;
        },
        update_help_dialog(value) {
          this.help_dialog = value;
        },
        openHelpSectionModalEvent() {
          this.saveInteraction(
            this.pluginSectionName,
            "viewed",
            "section_help_dialog",
            3
          );
        },
        get_timezone() {
          let information = `${this.strings.change_timezone} ${this.timezone}`;
          return information;
        },
        saveInteraction(component, interaction, target, interactiontype) {
          let data = {
            action: "saveinteraction",
            pluginsection: this.pluginSectionName,
            component,
            interaction,
            target,
            url: window.location.href,
            interactiontype,
            token: this.token,
          };
          Axios({
            method: "post",
            url: `${M.cfg.wwwroot}/local/notemyprogress/ajax.php`,
            data: data,
          })
            .then((r) => {})
            .catch((e) => {});
        },
        addLevel() {
          let newLevel = this.levelsData.length + 1;
          this.levelsData.push({
            lvl: newLevel,
            nam: `${this.strings.level} ${newLevel}`,
            des: ``,
            points: this.pointsBase * (newLevel - 1),
          });
        },
        removeLevel() {
          if (this.levelsData.length > 2) {
            this.levelsData.pop();
          }
        },
        calculatePoints(index) {
          let points = this.isNumber(this.pointsBase) * index;
          this.levelsData[index].points = points;
          return points;
        },
        isNumber(x) {
          if (x === "" || isNaN(x) || x == 0) {
            return this.pointsBaseOri;
          }
          return parseInt(x);
        },
        save_changes(logParam) {
          this.notifications = ["Do you want to save the changes"];
          Alertify.confirm(this.strings.save_warning_content, () => {
            this.saveGamificationConfig(logParam);
            console.log(logParam);
          }) // ON CONFIRM
            .set({ title: this.strings.save_warning_title })
            .set({
              labels: {
                cancel: this.strings.confirm_cancel,
                ok: this.strings.confirm_ok,
              },
            });
        },
        saveGamificationConfig(logParam) {
          this.loading = true;
          let settings = {
            tit: this.settings.tit,
            des: this.settings.des,
            pointsbase: this.pointsBase,
          };
          let data = {
            courseid: this.courseid,
            userid: this.userid,
            action: "savegamification",
            levels: JSON.stringify(this.levelsData),
            settings: JSON.stringify(settings),
            rules: JSON.stringify(this.rulesData),
            token: this.token,
            enable: this.enable,
            section: logParam,
          };
          let url = { url: window.location.href };
          Axios({
            method: "post",
            url:
              M.cfg.wwwroot +
              "/local/notemyprogress/ajax.php?courseid=" +
              this.courseid +
              "&userid=" +
              this.userid +
              "&action=savegamification&levels=" +
              data.levels +
              "&settings=" +
              data.settings +
              "&rules=" +
              data.rules +
              "&url=" +
              url.url +
              "&enable=" +
              data.enable +
              "&section=" +
              data.section,
            params: data,
          })
            .then((response) => {
              // console.log(response);
              // console.log(response.status);
              // console.log(url.url);
              if (response.status == 200 && response.data.ok) {
                this.showNotifications(response.data.data, "success");
              } else {
                let message =
                  response.data.error || this.strings.api_error_network;
                this.showNotifications(message, "error"); //in this line "error" define the kind of message send
              }
            })
            .catch((e) => {
              let message = e.response.data || this.strings.api_error_network;
              this.showNotifications(message, "error");
            })
            .finally(() => {
              this.loading = false;
            });
        },
        showNotifications(message, type, alert = true, notify = true) {
          if (alert) this.notifications.push({ message, type });
          if (notify) Alertify.notify(message, type);
        },
        table_headers() {
          let headers = [
            { text: this.strings.ranking_text, value: "ranking" },
            { text: this.strings.level, value: "level" },
            { text: this.strings.student, value: "student" },
            { text: this.strings.total, value: "total" },
            { text: this.strings.progress, value: "progress_percentage" },
          ];
          return headers;
        },
        addRule() {
          this.rulesData.push({
            rule: "",
            points: 0,
          });
        },
        removeRule(index) {
          if (this.rulesData.length > 2) {
            this.rulesData.splice(index, 1);
          }
        },
        disableEnable(swDisableEnable) {
          //traitement
          this.enable = swDisableEnable;
          let data = {
            courseid: this.courseid,
            userid: this.userid,
            action: "saveEnable",
            enable: this.enable,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                this.showNotifications(response.data.data, "success");
              } else {
                let message =
                  response.data.error || this.strings.api_error_network;
                this.showNotifications(message, "error"); //in this line "error" define the kind of message send
              }
            })
            .catch((e) => {
              let message = e.response.data || this.strings.api_error_network;
              this.showNotifications(message, "error");
            })
            .finally(() => {
              this.loading = false;
            });
        },
        //? ////////////////////////////////////////////////////////////////////// ?//
        //? //////////////////////////// ChartPart /////////////////////////////// ?//
        //? ////////////////////////////////////////////////////////////////////// ?//
        chart_spread() {
          let chart = new Object();
          chart.chart = {
            type: "column",
            backgroundColor: null,
          };
          chart.title = {
            text: this.strings.chartTitle,
          };
          chart.colors = ["#118AB2"];
          (chart.xAxis = {
            type: "category",
            labels: {
              rotation: -45,
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          }),
            (chart.yAxis = {
              min: 0,
              title: {
                text: this.strings.chartYaxis,
              },
            });
          chart.legend = {
            enabled: false,
          };
          (chart.series = [
            {
              name: null,
              data: this.chartdata, //[["level : 1", 1]],
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: "#FFFFFF",
                align: "right",
                format: "{point.y:.1f}", // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                  fontSize: "13px",
                  fontFamily: "Verdana, sans-serif",
                },
              },
            },
          ]),
            console.log("series: ");
          console.log(chart.series);
          return chart;
        },
        setGraphicsEventListeners() {
          console.log("Listeners set");
          let graphics = document.querySelectorAll(".highcharts-container");
          if (graphics.length < 1) {
            setTimeout(app.setGraphicsEventListeners, 500);
          } else {
            graphics[0].id = "SpreadChart";
            graphics.forEach((graph) => {
              graph.addEventListener("mouseenter", app.addLogsViewGraphic);
            });
          }
        },
        addLogsViewGraphic(e) {
          event.stopPropagation();
          var action = "";
          var objectName = "";
          var objectType = "";
          var objectDescription = "";
          switch (e.target.id) {
            case "SpreadChart":
              action = "viewed";
              objectName = "spreading_chart";
              objectType = "chart";
              objectDescription =
                "Bar chart that shows the level repartition in gamification";
              break;
            default:
              action = "viewed";
              objectName = "";
              objectType = "chart";
              objectDescription = "A chart";
              break;
          }
          app.addLogsIntoDB(action, objectName, objectType, objectDescription);
        },
        addLogsIntoDB(action, objectName, objectType, objectDescription) {
          let data = {
            courseid: content.courseid,
            userid: content.userid,
            action: "addLogs",
            sectionname: "TEACHER_GAMIFICATION",
            actiontype: action,
            objectType: objectType,
            objectName: objectName,
            currentUrl: document.location.href,
            objectDescription: objectDescription,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
              }
            })
            .catch((e) => {});
        },
      },
    });
  }
  return {
    init: init,
  };
});
