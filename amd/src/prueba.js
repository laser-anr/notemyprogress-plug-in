define([
  "local_notemyprogress/vue",
  "local_notemyprogress/vuetify",
  "local_notemyprogress/axios",
  "local_notemyprogress/moment",
  "local_notemyprogress/pagination",
  "local_notemyprogress/chartdynamic",
  "local_notemyprogress/pageheader",
], function (
  Vue,
  Vuetify,
  Axios,
  Moment,
  Pagination,
  ChartDynamic,
  Pageheader
) {
  "use strict";

  function init(content) {
    //console.log(content);
    Vue.use(Vuetify);
    Vue.component("pagination", Pagination);
    Vue.component("chart", ChartDynamic);
    Vue.component("pageheader", Pageheader);
    let vue = new Vue({
      delimiters: ["[[", "]]"],
      el: "#prueba",
      vuetify: new Vuetify(),
      data() {
        return {
          strings: content.strings,
          groups: content.groups,
          userid: content.userid,
          courseid: content.courseid,
          timezone: content.timezone,
          render_has: content.profile_render,
          loading: false,
          errors: [],
          pages: content.pages,
        };
      },
      mounted() {
        document.querySelector("#sessions-loader").style.display = "none";
        document.querySelector("#prueba").style.display = "block";
      },
      methods: {
        get_help_content() {
          let helpcontents = `Texto de Ayuda`;
          return helpcontents;
        },

        get_timezone() {
          let information = `${this.strings.ss_change_timezone} ${this.timezone}`;
          return information;
        },
      },
    });
  }

  return {
    init: init,
  };
});
